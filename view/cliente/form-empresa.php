<div class="col-md-6">
    <div class="panel">
        <div class="panel-body">
            <h4 class="m-t-0 m-b-30">Información de grupo</h4>

            <div class="btn-group btn-group-justified btn-group-control m-b-20" data-toggle="buttons">
                <label class="btn btn-default active">
                    <input type="radio" id="empresaInput" name="preciosOferta" value="empresa" checked>Empresa
                </label> 
                <label class="btn btn-default">
                    <input type="radio" id="grupoInput" name="preciosOferta" value="grupo">Grupo
                </label>
            </div>


            <div class="upload-img-wrapper">
                <input type="file" class="no-image" id="img-result">
            </div>
            
            <form action="">
                <div id="camposEmpresa">
                    <div class="form-group">
                        <label for="">Razón social</label>
                        <input type="text" class="form-control" id="empresaNombre" value="">
                    </div>

                    <div class="form-group">
                        <label for="">CUIT</label>
                        <input type="text" class="form-control" id="empresaCuit" value="">
                    </div>

                    <div class="form-group">
                        <label for="">Domicilio Fiscal</label>
                        <input type="text" class="form-control" id="empresaDomiciliio" value="">
                    </div>

                    <div class="form-group">
                        <label for="">Condicion frente al IVA</label>
                        <select class="form-control" id="empresaIva"></select>
                    </div>
                </div>

                <div id="camposGrupo">
                    <div class="form-group">
                        <label for="">Nombre de grupo</label>
                        <input type="text" class="form-control" id="grupoNombre" value="">
                    </div>

                    <div class="form-group">
                        <label for="">Empresa o grupo al que pertenecen</label>
                        <input type="text" class="form-control" id="grupoEmpresa" value="">
                    </div>
                </div>
                <input type="button" class="btn btn-default" onclick="insEmpresa()" value="Cambiar">
            </form>
        </div>
    </div>
</div>