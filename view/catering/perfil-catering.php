<?php 
  $titulo = 'Aldia | Perfil';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"><i class="icon" data-icon="&#xe056;"></i> Mi Perfíl</div>
              </div>  

              <div class="panel-body">
                <form id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">
                  <div class="row">

                    <div class="col-sm-4">
                      <div class="upload-img-wrapper">
                          <label class="center-block text-center" for="preImg">
                            <input type='file' id="preImg" name="preImg" />
                            <img id="imgCat" class="img-responsive" src="<?php echo $catering->imagen; ?>" alt="your image" />  
                          </label>
                        </div> <!-- Termina el uploader de img -->
                    </div>

                    <div class="col-sm-8">
                      <div class="form-group form-group-default m-t-15-p">
                        <label>Nombre del Catering</label>
                        <input type="text" class="form-control" name="nombreCatering" id="nombre" value="<?php echo $catering->nombre; ?>">
                      </div> 
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>CUENTALE A TUS CLIENTES SOBRE TU CATERING, COMO TRABAJAN, TIPOS DE MENÚES, ETC.</label>
                        <textarea class="form-control textarea-control" name="descripcion" id="descripcion"><?php echo $catering->descripcion; ?></textarea>
                      </div>
                    </div>
                  </div>
                    
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Cantidad mínima de pedidos diarios</label>
                        <input type="number" class="form-control" name="pedidosMinimos" value="<?php echo $catering->pedidos_minimos; ?>">
                      </div> 
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default form-group-default-select2 required">
                        <label class="">Zonas de cobertura</label>
                        <select id="zonaCobertura" name="zonaCobertura" class="full-width" data-placeholder="Busca un barrio" data-init-plugin="select2" multiple>
                          
                        </select>
                      </div>  
                    </div>
                  </div>
                  
                  <div class="clearfix"></div>
                  <a href="javascript:void(0)" onclick="editarPerfil()" class="btn btn-primary">Continuar</a>
                </form>
              </div>    
            </div> <!-- Termina .panel -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script src="assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/perfil-uc.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/perfil-catering.js" type="text/javascript"></script>
  </body>
</html>