

(function($) {
    $(document).ready(function() {

        // Agrega la clase .no-search al dropdown para deshabilitar el buscador

        $("#catering-type").select2({
            dropdownCssClass : 'no-search'
        }); 

        $("#multi-client").select2({
            dropdownCssClass : 'multi-client'
        });

        $('#datepicker-desde, #datepicker-hasta').datepicker();

    });

})(window.jQuery);