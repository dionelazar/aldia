$(document).ready(function(){
    var actualHref = window.location.href.split("/")[window.location.href.split("/").length - 1];
        actualHref = actualHref.split("#")[0];
    if(document.getElementById(actualHref) != null) {
      $('#'+actualHref).addClass('active');
    }
});

function logout()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'logout'
        },
        success : function(Data){
            console.log(Data);
            window.location.replace('index');
        }
    });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}