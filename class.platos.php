<?php 
/**
* 
*/
require_once 'class.conection.php';

class Platos extends Conection
{
	public $cod_mensaje;
	public $mensaje;

	public $id_plato;
	public $id_menu;
	public $id_catering;
	public $fecha;
	public $plato;
	public $txt_desc;
	public $calorias;
	public $precio;
	public $stock;

	public function getPlatos($post)
	{
		$id_menu = $post['id_menu'];

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = " EXEC sp_get_platos @id_menu = ? , @id_catering = ? ";
		$params = array( $id_menu, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6001', 'sp_get_platos', $params);
			Helpers::jsonEncodeError(0, 'Error 6001');
		}else{
			$platos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_plato = new Platos();

				$mi_plato->id_plato = $row['id_plato'];
				$mi_plato->id_menu = $row['id_menu'];
				$mi_plato->id_catering = $row['id_catering'];
				$mi_plato->fecha = $row['fecha']->format('Ymd');
				$mi_plato->plato = is_null($row['plato']) ? 'Nuevo plato' : utf8_encode($row['plato']);
				$mi_plato->txt_desc = utf8_encode($row['txt_desc']);
				$mi_plato->calorias = $row['calorias'];
				$mi_plato->precio = $row['precio'];

				$platos[$i] = $mi_plato;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			echo json_encode($platos);
		}
	}

	public function getPlatoEspecifico($post)
	{
		$id_menu = $post['id_menu'];
		$id_plato = $post['id_plato'];

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = " EXEC sp_get_plato_especifico @id_menu = ? , @id_plato = ?, @id_catering = ? ";
		$params = array( $id_menu, $id_plato, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6002', 'sp_get_plato_especifico', $params);
			Helpers::jsonEncodeError(0, 'Error 6002');
		}else{
			$platos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_plato = new Platos();

				$mi_plato->id_plato = $row['id_plato'];
				$mi_plato->id_menu = $row['id_menu'];
				$mi_plato->id_catering = $row['id_catering'];
				$mi_plato->fecha = $row['fecha']->format('Ymd');
				$mi_plato->plato = is_null($row['plato']) ? 'Nuevo plato' : utf8_encode($row['plato']);
				$mi_plato->txt_desc = utf8_encode($row['txt_desc']);
				$mi_plato->calorias = $row['calorias'];
				$mi_plato->precio = $row['precio'];
				$mi_plato->stock = $row['stock'];

				$platos[$i] = $mi_plato;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			echo json_encode($platos);
		}
	}

	public function getAllPlatosActivos()
	{
		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = " EXEC sp_get_all_platos_activos @id_catering = ? ";
		$params = array( $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6003', 'sp_get_all_platos_activos', $params);
			Helpers::returnError(0, 'Error 6003');
		}else{
			$platos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_plato = new Platos();

				$mi_plato->id_plato    = $row['id_plato'];
				$mi_plato->id_menu     = $row['id_menu'];
				$mi_plato->id_catering = $row['id_catering'];
				$mi_plato->fecha       = $row['fecha']->format('Ymd');
				$mi_plato->plato       = is_null($row['plato']) ? 'Nuevo plato' : utf8_encode($row['plato']);
				$mi_plato->txt_desc    = utf8_encode($row['txt_desc']);
				$mi_plato->calorias    = $row['calorias'];
				$mi_plato->precio      = $row['precio'];
				$mi_plato->dia         = $row['precio'];
				$mi_plato->numDia      = $row['precio'];

				$platos[$i] = $mi_plato;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			return $platos;
		}
	}

	public function insPlato($post)
	{
		$id_menu = $post['id_menu'];
		$fecha = $post['fecha'];

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_plato @id_menu = ? , @id_catering = ?, @fecha = ?";
		$params = array( $id_menu, $id_catering, $fecha );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6101', 'sp_ins_plato', $params);
			Helpers::jsonEncodeError(0, 'Error 6101');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$mi_plato = new Platos();

			$mi_plato->cod_mensaje = $row['cod_mensaje'];
			$mi_plato->mensaje = $row['mensaje'];

			if($mi_plato->cod_mensaje == -1)
			{
				$mi_plato->id_plato = $row['id_plato'];
				$mi_plato->id_menu = $row['id_menu'];
				$mi_plato->id_catering = $row['id_catering'];
				$mi_plato->fecha = $row['fecha']->format('d/m/y');
			}

			Conection::CerrarConexion($conn);	
			echo json_encode($mi_plato);
		}
	}

	public function updPlato($post)
	{
		$id_menu = $post['id_menu'];
		$id_plato = $post['id_plato'];
		$plato = utf8_decode($post['plato']);
		$kcal = $post['kcal'];
		$precio = $post['precio'];
		$stock = $post['stock'];
		$desc = utf8_decode($post['desc']);

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_plato @id_menu = ?, @id_plato = ? , @id_catering = ?, @plato = ?, @kcal = ?, @precio = ?, @stock = ?, @desc = ? ";
		$params = array( $id_menu, $id_plato, $id_catering, $plato, $kcal ,$precio ,$stock ,$desc );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6201', 'sp_upd_plato', $params);
			Helpers::jsonEncodeError(0, 'Error 6201');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$mi_plato = new Platos();

			$mi_plato->cod_mensaje = $row['cod_mensaje'];
			$mi_plato->mensaje = $row['mensaje'];

			Conection::CerrarConexion($conn);	
			echo json_encode($mi_plato);
		}
	}

	public function deletePlato($post)
	{
		$id_menu = $post['id_menu'];
		$id_plato = $post['id_plato'];

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_delete_plato @id_menu = ?, @id_plato = ? , @id_catering = ?";
		$params = array( $id_menu, $id_plato, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 6301', 'sp_delete_plato', $params);
			Helpers::jsonEncodeError(0, 'Error 6301');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$mi_plato = new Platos();

			$mi_plato->cod_mensaje = $row['cod_mensaje'];
			$mi_plato->mensaje = $row['mensaje'];

			Conection::CerrarConexion($conn);	
			echo json_encode($mi_plato);
		}
	}
}
?>
