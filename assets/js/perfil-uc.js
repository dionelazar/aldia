(function($) {

    'use strict';

    $(document).ready(function() {

        $("#zonaCobertura").select2({
            dropdownCssClass : 'multi-client'
        });

        // Upload img

        var uploader = document.createElement('input'),
        image = document.getElementById('img-result');

        uploader.type = 'file';
        uploader.accept = 'image/*';
        
        //image.onclick = function() {
        //    uploader.click();
        //}

        uploader.onchange = function() {
            var reader = new FileReader();
            reader.onload = function(evt) {
                image.classList.remove('no-image');
                image.style.backgroundImage = 'url(' + evt.target.result + ')';
                var request = {
                    itemtype: 'test 1',
                    brand: 'test 2',
                    images: [{
                        data: evt.target.result
                    }]
                };
            }
            reader.readAsDataURL(uploader.files[0]);
        }
    });

})(window.jQuery);