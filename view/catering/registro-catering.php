<?php 
  require_once '../../Controllers/RegistroController.php';
  $registro = new RegistroController();
  $registro->registroCatering();
  require_once '../header.login.php';
?>
  <body class="fixed-header">
    <div class="login-wrapper">
      <!-- Comienza Login Right Container-->
      <div class="login-container p-t-50">
        <div class="container login-subcontainer">
          <div class="row">
            <img class="center-block m-b-50" src="img/logo-white.svg" alt="logo" data-src="img/logo-white.svg" height="120">
            <h4 class="text-white text-center">Registro de <strong>Catering</strong></h4>
            <!-- Comienza Login Form -->
            <form id="form-login" class="p-t-15" role="form">
              <!-- Comienza Form Control-->
              <div class="form-group form-group-default form-group-login">
                <label>Nombre</label>
                <div class="controls">
                  <input type="text" id="nameLogin" name="" class="form-control" required>
                </div>
              </div>

              <div class="form-group form-group-default form-group-login">
                <label>Apellido</label>
                <div class="controls">
                  <input type="text" id="apellidoLogin" name="" class="form-control" required>
                </div>
              </div>

              <div class="form-group form-group-default form-group-login">
                <label>Nombre de Catering</label>
                <div class="controls">
                  <input type="text" id="cateringLogin" name="" class="form-control" required>
                </div>
              </div>

              <div class="form-group form-group-default form-group-login">
                <label>E-mail</label>
                <div class="controls">
                  <input type="email" id="mailLogin" name="" class="form-control" required>
                </div>
              </div>

              <div class="form-group form-group-default form-group-login input-group" style="overflow:visible">
                <label>Contraseña</label>
                <input id="passwordLogin" type="password" name="password" class="form-control" required>
                <a href="#0"  id="showPass" class="input-group-addon"><i data-toggle="tooltip" data-original-title="Mostrar Contraseña" class="icon" data-icon="&#xe037;"></i></a>
              </div>


              <input type="button" onclick="registrarCatering()" class="btn btn-lg btn-primary btn-cons btn-block m-t-10" value="Registrarme">
            </form>
            <!--Termina Login Form-->
            <div class="">
              <div class="m-b-30 sm-m-t-20clearfix">
                <div class="no-padding m-t-10">
                  <p class="text-white">
                    <small>Al efectuar el registro en aldia estaras aceptando los <a style="color: #790c1f;" href="">Términos y condiciones</a> del servicio</small>
                  </p>
                </div>
              </div>
            </div>  
          </div>
        </div>
      </div>
      <!-- Termina Login Right Container-->
    </div>
        
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="scripts/registro-catering.js"></script>
    <script>
    $(function()
    {
      $('#form-login').validate();

      $("#cuitLogin").mask("99-99999999-9");

      $("#passwordLogin").each(function (index, input) {
        var $input = $(input);

        $("#showPass").click(function () {
          var change = "";

          if ($('#showPass i').attr("data-original-title") === "Mostrar Contraseña") {
            $('#showPass i').attr("data-original-title", "Ocultar Contraseña")
            $('#showPass').addClass("active")
            change = "text";
          } else {
            $('#showPass i').attr("data-original-title", "Mostrar Contraseña")
            $('#showPass').removeClass("active")
            change = "password";
          }

          var rep = $("<input type='" + change + "' />")
          .attr("id", $input.attr("id"))
          .attr("name", $input.attr("name"))
          .attr('class', $input.attr('class'))
          .val($input.val())
          .insertBefore($input);
          $input.remove();
          $input = rep;
        }).insertAfter($input);
      });

      jQuery.extend(jQuery.validator.messages, {
        required: "¡Ups! No podemos enviar este campo vacio",
        email: "Mmmm ¿Estás seguro que ese es tu correo?",
      });
    })
    </script>
  </body>
</html>