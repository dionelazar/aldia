(function($) {

    'use strict';

    $(document).ready(function() {
        $.validator.addMethod("precio", function(value, element) {
            return this.optional(element) || /^(\$?)(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
        }, "Ingresa números sin comas ni puntos ej: 40");

        $.validator.addMethod("kcal", function(value, element) {
            return this.optional(element) || /^(\$?)(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
        }, "Ingresa números sin comas ni puntos ej: 500");

        $("#form-plato").validate();

        var platosList = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          // prefetch: '' aca va la url al json con la lista de platos que alguna vez ya ingreso
        });

        $('.sample-typehead').typeahead(null, {
          name: 'platosList',
          source: platosList
        });

        var addPlatoBtn = $('.add-plato');

        addPlatoBtn.each(function(){
            $(this).on('click', function(){
                var newPlato = '';
                newPlato = newPlato + '<div class="plato-creado">';
                newPlato = newPlato + ' <span>Nuevo plato</span>';
                newPlato = newPlato + '</div>';    
                $(this).hide().parent().append(newPlato);

                    $('#calendar-event').addClass('open');
                    $('html').css('cursor','initial');
            });
        });

        var editPlato = $('.plato-creado');

        editPlato.on('click', function(){
            $('#calendar-event').addClass('open');
            $('html').css('cursor','initial');
        });
    });
})(window.jQuery);