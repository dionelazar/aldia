function registrarCatering()
{
	if(!validarRegistro()) return false;

	var nameLogin = $('#nameLogin').val();
	var apellidoLogin = $('#apellidoLogin').val();
	var cateringLogin = $('#cateringLogin').val();
	var mailLogin = $('#mailLogin').val();
	var passwordLogin = $('#passwordLogin').val();
	var codigo_activacion = window.location.href.split('&')[1];

	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'registroCatering'
            ,codigo_activacion : codigo_activacion
            ,nombre : nameLogin
			,apellido : apellidoLogin
			,catering : cateringLogin
			,email : mailLogin
			,password : passwordLogin
        },
        success : function(Data){
            console.log(Data);
            var miData = JSON.parse(Data);
            if(miData[0].cod_mensaje == -1){
            	window.location.replace('index');
            }else{
            	alert(miData[0].mensaje);
            }
        }
    });
}

function validarRegistro()
{
	var nameLogin = $('#nameLogin').val();
	var apellidoLogin = $('#apellidoLogin').val();
	var cateringLogin = $('#cateringLogin').val();
	var mailLogin = $('#mailLogin').val();
	var passwordLogin = $('#passwordLogin').val();
	var codigo_activacion = window.location.href.split('&')[1];

	if( $('#nameLogin-error').text().length > 0 || nameLogin.length < 1)
		return false;

	if( $('#apellidoLogin-error').text().length > 0 || apellidoLogin.length < 1)
		return false;

	if( $('#cateringLogin-error').text().length > 0 || cateringLogin.length < 1)
		return false;

	if( $('#mailLogin-error').text().length > 0 || mailLogin.length < 1)
		return false;

	if( $('#passwordLogin-error').text().length > 0 || passwordLogin.length < 1)
		return false;

	if( codigo_activacion.length <= 0 )
		return false;

	return true;
}