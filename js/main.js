jQuery(document).ready(function($){
	var pedidoWrapper = $('#pedidoWrapper');
	var addToCartBtn = $('.add-plato');

	addToCartBtn.on('click', function(event){
		event.preventDefault();
		var idSemana = $('#ulSemanas .active')[0].id.split('_')[1];
		
        var obj = $($('#semana'+ (parseInt(idSemana)+1) +' .active')[0]).children()[0].id.split('_');
        var semana = obj[1];
        var dia = obj[2];
        addToCart($(this));

		if ($('.pedido').hasClass('hidden')) {
			$('.pedido').removeClass('hidden').slideDown()
		};

		
		$('#sinPedido').hide();

		$('#alertAddPlato').fadeIn(function(){
			setTimeout(function(){ 
				$('#alertAddPlato').fadeOut();
			}, 3000);
		});

		if ($(window).width() <= 770) { 
			$('.show-pedido-row').fadeIn();
		}



		if ($('#dia_'+ semana + '_' + dia).hasClass('hidden')) {
			$('#dia_'+ semana + '_' + dia).removeClass('hidden');
		};
	});


	function addToCart(trigger) {
		addPlato();
	}

	function addPlato() {
		var platoAdded = '';
        platoAdded = platoAdded + '<div class="plato-checkout">';
        platoAdded = platoAdded + '	<span>Nombre de plato x 1</span>'  ;
        platoAdded = platoAdded + '	<span class="plato-price-checkout pull-right">4000</span>';
        platoAdded = platoAdded + '	<a href="#0" class="delete-item pull-right"><i class="icon-cross"></i></a>';
        platoAdded = platoAdded + '</div>';
        
       	var idSemana = $('#ulSemanas .active')[0].id.split('_')[1];
        var obj = $($('#semana'+(parseInt(idSemana)+1) +' .active')[0]).children()[0].id.split('_');
        var semana = obj[1];
        var dia = obj[2];

        $('#dia_'+ semana + '_' + dia).append(platoAdded);

        deletePlato();
	}

	function deletePlato(){
		$(".delete-item").click(function(){
			$(this).parent().fadeOut(250, function(){
				$(this).remove();
			})
    	});	
	}

	$(".incrementer-plato button").on("click", function() {
	  var $button = $(this);
	  var oldValue = $button.parent().find("input").val();

	  if ($button.is(".sumar-plato")) {
		  var newVal = parseFloat(oldValue) + 1;
		} else {
	    if (oldValue > 1) {
	      var newVal = parseFloat(oldValue) - 1;
	    } else {
	      newVal = 1;
	    }
	  }
	  $button.parent().find("input").val(newVal);
	});


	if ($(window).width() <= 770) {

		$('.nav-semanas li').not('.nav-semanas li.active, .toggle-mobile-semanas').hide();
		
		$('#toggleSemanas').on('click', function(){
			$('#toggleSemanas i').toggleClass('icon-cross');
			$('.nav-semanas li').not(".toggle-mobile-semanas, .nav-semanas li.active").slideToggle();	
		});

		$('.nav-semanas li').not('.toggle-mobile-semanas').on('click', function(){
			$('#toggleSemanas i').toggleClass('icon-cross');
			$(this).siblings().not('.toggle-mobile-semanas').slideToggle();
		});

		$('#pedidoWrapper').parent().toggleClass('pedido-mobile col-md-3');

		$('#hidePedido').on('click', function(){
			$('.pedido-mobile').fadeOut();
			$('#showPedido').fadeIn();
		});

		$('#showPedido').on('click', function(){
			$(this).fadeOut();
			$('.pedido-mobile').fadeIn();
		});
	}		
});