<?php
	require_once '../class.conection.php';

	if(isset($_COOKIE['id_usuario']))
	{
		if( is_int(Cookies::getIdUsuario()) )
		{	
			$datos = Cookies::getDatosUser();
			if($datos->tipo_usuario == 1)
				header('location:welcomecatering');
			else
				header('location:home');
		}else{
			header('location:login');
		}
	}else{
		header('location:login');
	}
?>