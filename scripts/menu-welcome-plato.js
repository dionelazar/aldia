$(document).ready(function(){
    armarCalendario();
    getPlatos();
});

$('#eventSave').click(function(){
	editPlato();
})

$('#eventDelete').click(function(){
	deletePlato();
})

var dias = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
var id_plato_seleccionado = 0;

function armarCalendario()
{
	var fechas = getFechas();

	var html = '';

	html += '<thead>';
	html += '	<tr>';
		for (var i = 0; i < fechas.diffDays; i++) 
		{
			var nombreDia = dias[fechas.allDates[i].getDay()];
			var numberDia = fechas.allDates[i].getDate();
			html += '<th>'+nombreDia+' '+numberDia+'</th>';
		}
	html += '	</tr>';
	html += '</thead>';

	html += '<tbody>';
		for (var i = 0; i < 5; i++)
		{
			html += '<tr>';
			for (var j = 0; j < fechas.diffDays; j++) 
			{ 
				var dia = fechas.allDates[j].getDate();
				var mes = fechas.allDates[j].getMonth();
				var ano = fechas.allDates[j].getFullYear();

				if(dia < 10)
					dia = ('0' + dia).slice(-2)

				if(mes < 10)
					mes = ('0' + mes).slice(-2)

				var fecha = ano +''+mes+''+dia;
				html += '<td><div class="add-plato" id="date-'+fecha+'-'+i+'" onclick="nuevoPlato('+fecha+', this)">+ Agrega un plato</div></td>';
			}
			html += '</tr>';
		}
	html += '</tbody>';

	$('#calendarioPlatos').html(html);
}

function getFechas()
{
	var fechaDesde = new Date((miMenu.fecha_desde).split("/").reverse());
	var fechaHasta = new Date((miMenu.fecha_hasta).split("/").reverse());

	var timeDiff = Math.abs(fechaDesde.getTime() - fechaHasta.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

	var allDates = getDates(fechaDesde, fechaHasta);
	
	var misDatosFecha = {diffDays : diffDays, allDates : allDates};
	return misDatosFecha;
}

Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) 
{
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate < stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function nuevoPlato(fecha, dataButton)
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'platos'
            ,call  : 'insPlato'
            ,id_menu : miMenu.id_menu
            ,fecha : fecha
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var newPlato = '';
		    newPlato += '<div class="plato-creado" onclick="openEditPlato('+miData.id_plato+')">';
		    newPlato += ' <span>Nuevo plato</span>';
		    newPlato += '</div>';    
		    $(dataButton).hide().parent().append(newPlato);
		    id_plato_seleccionado = miData.id_plato;
	        $('#calendar-event').addClass('open');
	        $('html').css('cursor','initial');
	        
	        $('#nombrePlato').val('');
			$('#kcal').val('');
			$('#precio').val('');
			$('#stock').val('');
			$('#txtEventDesc').val('');
        }
    });
}

function openEditPlato(id_plato)
{
	id_plato_seleccionado = id_plato;
	$('#calendar-event').addClass('open');
	$('html').css('cursor','initial');
	getPlatoEspecifico();
}

function getPlatoEspecifico()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'platos'
            ,call  : 'getPlatoEspecifico'
            ,id_menu : miMenu.id_menu
            ,id_plato : id_plato_seleccionado
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            $('#nombrePlato').val(miData[0].plato);
			$('#kcal').val(miData[0].calorias);
			$('#precio').val(miData[0].precio);
			$('#stock').val(miData[0].stock);
			$('#txtEventDesc').val(miData[0].txt_desc);
        }
    });
}

function getPlatos()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'platos'
            ,call  : 'getPlatos'
            ,id_menu : miMenu.id_menu
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarPlatos(miData);
        }
    });
}

function armarPlatos(miData)
{
	if(miData.length > 0)
    {
        var anteriorFecha = miData[0].fecha;
        var j = 0;
        var fechas = [];

    	for (var i = 0; i < miData.length; i++) {
    		fechas.push( miData[i].fecha );
    	}
    	
	    for (var i = 0; i < miData.length; i++) 
	    {
	    	var newPlato = '';
		    newPlato += '<div class="plato-creado" onclick="openEditPlato('+miData[i].id_plato+')">';
		    newPlato += ' <span>'+miData[i].plato+'</span>';
		    newPlato += '</div>';

		    if($('#date-'+miData[i].fecha+'-'+j).parent().find('div.plato-creado').length > 0) //si tiene algo
		    {
		    	j++;
		    	$('#date-'+miData[i].fecha+'-'+j).hide().parent().append(newPlato);
		    }else{
		    	j = 0;
		    	$('#date-'+miData[i].fecha+'-'+j).hide().parent().append(newPlato);
		    }
	    }
	}
}

function editPlato()
{
	if(!validarDatosPlato()){
		alert('Completar todos los datos');
		return false;
	}

	var plato = $('#nombrePlato').val();
	var kcal = $('#kcal').val();
	var precio = $('#precio').val();
	var stock = $('#stock').val();
	var desc = $('#txtEventDesc').val();

	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'platos'
            ,call  : 'updPlato'
            ,id_menu : miMenu.id_menu
            ,id_plato : id_plato_seleccionado
            ,plato : plato
            ,kcal : kcal
            ,precio : precio
            ,stock : stock
            ,desc : desc
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            alert(miData.mensaje);
            location.reload();
        }
    });
}

function validarDatosPlato()
{
	var plato = $('#nombrePlato').val();
	var kcal = $('#kcal').val();
	var precio = $('#precio').val();
	var stock = $('#stock').val();
	var desc = $('#txtEventDesc').val();

	if(plato.length < 1)
		return false;

	if(kcal.length < 1)
		return false;

	if(precio.length < 1)
		return false;

	if(stock.length < 1)
		return false;

	if(desc.length < 1)
		return false;

	return true;
}

function deletePlato()
{
	if(confirm('Estás seguro que querés borrarlo?'))
	{
		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'platos'
	            ,call  : 'deletePlato'
	            ,id_menu : miMenu.id_menu
	            ,id_plato : id_plato_seleccionado
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);
	            console.log(miData.mensaje);
	            location.reload();
	        }
	    });
	}
}

function publicarMenu()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'menues'
            ,call  : 'publicarMenu'
            ,id_menu : miMenu.id_menu
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
            alert(miData.mensaje);
            if(miData.cod_mensaje == -1)
            	window.location.replace('menucatering');
        }
    });
}