$(document).ready(function(){
    armarSelectZonasCobertura();
});

var actualTab = 1;

function getPaises()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getPaises'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function getProvincias(cod_pais)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getProvincias'
            ,cod_pais : cod_pais
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function getMunicipios(cod_pais, cod_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getMunicipios'
            ,cod_pais : cod_pais
            ,cod_provincia : cod_provincia
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarBarrios(miData);
        }
    });
}

function armarSelectZonasCobertura()
{
    getMunicipios(1,1);
    getMunicipios(1,2);
    getMunicipiosPropios();
}

function armarBarrios(Data)
{
    html = '';
    html += '<optgroup label="'+Data[0].provincia+'">';
    for (var i = 0; i < Data.length; i++) 
    {
        html += '<option value="['+Data[i].id_pais+','+Data[i].id_provincia+','+Data[i].id_municipio+']">'+Data[i].municipio+'</option>';
    }
    html += '</optgroup>';

    $('#zonaCobertura').append(html);
}

function agregarPropios(Data)
{
    values = [];
    for (var i = 0; i < Data.length; i++) 
    {
        //if (i < (Data.length - 1) )
            values[i] = "["+Data[i].id_pais+","+Data[i].id_provincia+","+Data[i].id_municipio+"]";
            //values += "\'["+Data[i].id_pais+","+Data[i].id_provincia+","+Data[i].id_municipio+"]\',";
        //else
            //values = "\'["+Data[i].id_pais+","+Data[i].id_provincia+","+Data[i].id_municipio+"]\'";
    }

    $('#zonaCobertura').select2().val(values).trigger('change')
}

function getMunicipiosPropios()
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'catering'
            ,call  : 'getZonasCobertura'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
            agregarPropios(miData);
        }
    });
}

function editarPerfil()
{
    var formData = new FormData(document.getElementById("form-personal"));
        formData.append("action", "catering");
        formData.append("call", "updPerfil");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){
            var miData = JSON.parse(Data);
            alert(miData.mensaje);
        }
    });
}