$(document).ready(function(){
	getProvincias(1);
	getDatosUsuario();
	getIva();
	getDireccion();
});

$('#provincias').change(function(){
	var cod_provincia = $('#provincias').val();
	getMunicipios(1, cod_provincia);
})

function getPaises()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getPaises'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function getProvincias(cod_pais)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getProvincias'
            ,cod_pais : cod_pais
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            $('#provincias').html('');
            for (var i = 0; i < miData.length; i++) {
        		var option = '<option value="'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
        		$('#provincias').append(option);
        	}
    }
    });
}

function getMunicipios(cod_pais, cod_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        async: false,
        data: {
             action: 'localidades'
            ,call  : 'getMunicipios'
            ,cod_pais : cod_pais
            ,cod_provincia : cod_provincia
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            $('#municipios').html('');
            
            for (var i = 0; i < miData.length; i++) 
            {
            	if(parseInt(miData[i].id_municipio) != 0){
	            	var option = '<option value="'+miData[i].id_municipio+'">'+miData[i].municipio+'</option>';
	            	$('#municipios').append(option);
            	}
	        }
        }
    });
}

function getDireccion()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'direccion'
            ,call  : 'getDireccion'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            getMunicipios(1, miData.id_provincia);

            $('#calle').val(miData.calle);
			$('#entre_calles').val(miData.entre_calles);
			$('#altura').val(miData.altura);
			$('#piso').val(miData.piso);
			$('#oficina').val(miData.oficina);
			$('#cod_postal').val(miData.cod_postal);
			$('#provincias').val(miData.id_provincia);
			$('#municipios').val(miData.id_municipio);
        }
    });
}

function getDatosUsuario()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'getMiUsuario'
        },
        success : function(Data){
            var miData = JSON.parse(Data);

			$('#contactoNombre').val(miData.nombre);
			$('#contactoApellido').val(miData.apellido);
			$('#contactoEmail').val(miData.email);
			$('#contactoTelefono').val(parseInt(miData.telefono));
		}
    });
}

function getEmpresa()
{

}

function getIva()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'empresa'
            ,call  : 'getIva'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            for (var i = 0; i < miData.length; i++) {
            	var	option = '<option value="'+miData[i].id_iva+'">'+miData[i].iva+'</option>';
            	$('#empresaIva').append(option);
            }
		}
    });
}

function insDireccion()
{
	if(validarDireccion())
	{
		var calle = $('#calle').val();
		var entre_calles = $('#entre_calles').val();
		var altura = $('#altura').val();
		var piso = $('#piso').val();
		var oficina = $('#oficina').val();
		var cod_postal = $('#cod_postal').val();
		var provincias = $('#provincias').val();
		var municipios = $('#municipios').val();

		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'direccion'
	            ,call  : 'insDireccion'
	            ,calle : calle
				,entre_calles : entre_calles
				,altura : altura
				,piso : piso
				,oficina : oficina
				,cod_postal : cod_postal
				,provincias : provincias
				,municipios : municipios
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);
	            alert(miData.mensaje);
	        }
	    });
	}else{
		alert('Completar todos los datos');
	}
}

function updDatosUsuario()
{
	if(validarDatosUsuario())
	{
		var nombre = $('#contactoNombre').val();
		var apellido = $('#contactoApellido').val();
		var email = $('#contactoEmail').val();
		var telefono = $('#contactoTelefono').val();

		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'users'
	            ,call  : 'updDatosUsuario'
	            ,nombre : nombre
				,apellido : apellido
				,email : email
				,telefono : telefono
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);
	            alert(miData.mensaje);
	        }
	    });
	}else{
		alert('Completar todos los datos');
	}
}

function insEmpresa()
{
	if($('#empresaInput:checked').length != 1)
	{
		insGrupo();
		return false;
	}

	if(validarDatosEmpresa())
	{
		var nombre = $('#contactoNombre').val();
		var apellido = $('#contactoApellido').val();
		var email = $('#contactoEmail').val();
		var telefono = $('#contactoTelefono').val();

		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'users'
	            ,call  : 'insEmpresa'
	            ,nombre : nombre
				,apellido : apellido
				,email : email
				,telefono : telefono
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);
	            alert(miData.mensaje);
	        }
	    });
	}else{
		alert('Completar todos los datos');
	}
}

function insGrupo()
{
	if(validarDatosGrupo())
	{
		var nombre = $('#contactoNombre').val();
		var apellido = $('#contactoApellido').val();
		var email = $('#contactoEmail').val();
		var telefono = $('#contactoTelefono').val();

		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'users'
	            ,call  : 'insEmpresa'
	            ,nombre : nombre
				,apellido : apellido
				,email : email
				,telefono : telefono
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);
	            alert(miData.mensaje);
	        }
	    });
	}else{
		alert('Completar todos los datos');
	}
}

function validarDireccion()
{
	var calle = $('#calle').val();
	var entre_calles = $('#entre_calles').val();
	var altura = $('#altura').val();
	var piso = $('#piso').val();
	var oficina = $('#oficina').val();
	var cod_postal = $('#cod_postal').val();
	var provincias = $('#provincias').val();
	var municipios = $('#municipios').val();

	if(calle.length < 1)
		return false;

	if(entre_calles.length < 1)
		return false;

	if(altura.length < 1)
		return false;

	if(piso.length < 1)
		return false;

	if(oficina.length < 1)
		return false;

	if(cod_postal.length < 1)
		return false;

	if(provincias.length < 1)
		return false;

	if(municipios.length < 1)
		return false;

	return true;
}

function validarDatosUsuario()
{
	var nombre = $('#contactoNombre').val();
	var apellido = $('#contactoApellido').val();
	var email = $('#contactoEmail').val();
	var telefono = $('#contactoTelefono').val();

	if(nombre.length < 1)
		return false;

	if(apellido.length < 1)
		return false;

	if(!isEmail(email))
		return false;

	if(telefono.length < 1)
		return false;

	return true;
}

function validarDatosEmpresa()
{
	var nombre = $('#contactoNombre').val();
	var apellido = $('#contactoApellido').val();
	var email = $('#contactoEmail').val();
	var telefono = $('#contactoTelefono').val();

	if(nombre.length < 1)
		return false;

	if(apellido.length < 1)
		return false;

	if(!isEmail(email))
		return false;

	if(telefono.length < 1)
		return false;

	return true;
}


function validarDatosGrupo()
{
	var nombre = $('#contactoNombre').val();
	var apellido = $('#contactoApellido').val();
	var email = $('#contactoEmail').val();
	var telefono = $('#contactoTelefono').val();

	if(nombre.length < 1)
		return false;

	if(apellido.length < 1)
		return false;

	if(!isEmail(email))
		return false;

	if(telefono.length < 1)
		return false;

	return true;
}


function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}