<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';
require_once __DIR__.'/class.users.php';
require_once __DIR__.'/class.servicios.php';

class Catering extends Conection
{
	public $mensaje;
	public $cod_mensaje;
	public $nombre;
	public $id_catering;
	public $domicilio;
	public $razon_social;
	public $tipo_doc;
	public $nro_doc;
	public $id_iva;
	public $iva;
	public $imagen;
	public $descripcion;
	public $pedidos_minimos;
	public $menues_vigentes;

	public function getDatosCatering($id_catering)
	{
		$conn = Conection::Conexion();
		$sql = " EXEC sp_get_catering_by_id @id_catering = ?";
		$params = array( $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2001', $sql, $params);
			return Helpers::returnError(0, 'Error 2001');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_catering = new Catering();

			$mi_catering->mensaje      = utf8_encode($row['mensaje']);
			$mi_catering->cod_mensaje  = $row['cod_mensaje'];

			if($mi_catering->cod_mensaje == -1)
			{
				$mi_catering->nombre = $row['nombre'];
				$mi_catering->domicilio = $row['domicilio'];
				$mi_catering->razon_social = $row['razon_social'];
				$mi_catering->tipo_doc = $row['tipo_doc'];
				$mi_catering->nro_doc = $row['nro_doc'];
				$mi_catering->id_iva = $row['id_iva'];
				$mi_catering->iva = $row['iva'];
				$mi_catering->imagen = is_null($row['imagen']) ? 'http://placehold.it/350x350' : $row['imagen'];
				$mi_catering->descripcion = utf8_encode($row['descripcion']);
				$mi_catering->pedidos_minimos = $row['pedidos_minimos'];
				//$mi_catering->menues_vigentes = $row['menues_vigentes'];
			}

			Conection::CerrarConexion($conn);
			return $mi_catering;
		}
	}

	public function getClientes($post)
	{
		$clientes = Users::getClientes();
		echo json_encode($clientes);
	}

	public function getTipoServicio($post)
	{
		$servicios = Servicios::getTiposServicios();
		echo json_encode($servicios);
	}

	public function getZonasCobertura($post)
	{
		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_zonas_cobertura @id_catering = ?";
		$params = array( $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2002', $sql, $params);
			return Helpers::returnError(0, 'Error 2002');
		}else{
			require_once __DIR__.'/class.localidades.php';
			$zonas = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_zona = new Localidades();
				//$mi_zona->$cod_mensaje = $row['cod_mensaje'];
				$mi_zona->mensaje = $row['mensaje'];

				$mi_zona->id_pais = $row['id_pais'];				
				$mi_zona->id_provincia = $row['id_dpto'];				
				$mi_zona->id_municipio = $row['id_municipio'];
				$mi_zona->municipio = utf8_encode($row['municipio']);

				$zonas[$i] = $mi_zona;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $zonas );
		}
	}

	public function welcomeInsImagen($post, $files)
	{
		$imagen = $files['preImg'];
		$type = explode('/', $imagen['type']);
		$extension = $type[1];
		$typeArchivo = $type[0];
		$tmpFile = $imagen['tmp_name'];

		if($typeArchivo != 'image')
		{
			$mi_catering = new Catering();
			$mi_catering->cod_mensaje = 0;
			$mi_catering->mensaje = 'Tipo de archivo no permitido '.$extension;
			echo json_encode($mi_catering);
			return;
		}

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_imagen_catering @id_catering = ?, @extension = ?";
		$params = array( $id_catering, $extension );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2101', $sql, $params);
			return Helpers::returnError(0, 'Error 2101');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_catering = new Catering();

			$mi_catering->mensaje      = utf8_encode($row['mensaje']);
			$mi_catering->cod_mensaje  = $row['cod_mensaje'];
			$mi_catering->ruta  = $row['ruta'];

			$resultado = move_uploaded_file($tmpFile, $mi_catering->ruta);
            if (!$resultado) 
            {
                $mi_catering->cod_mensaje = 0;
                $mi_catering->cod_mensaje = 'Ocurrió un error al mover el archivo';
            }

			Conection::CerrarConexion($conn);
			echo json_encode($mi_catering);
		}
	}
	
	public function welcomeInsAbout($post)
	{
		$about = utf8_decode($post['descripcionCatering']);
		$pedidos_minimos = $post['cantidadMinima'];

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_about_catering @id_catering = ?, @about = ?, @pedidos_minimos = ?";
		$params = array( $id_catering, $about, $pedidos_minimos );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2102', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 2102');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_catering = new Catering();

			$mi_catering->mensaje      = utf8_encode($row['mensaje']);
			$mi_catering->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode($mi_catering);
		}
	}

	public function insZonasCobertura($post)
	{
		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$zonasCobertura = json_decode($post['zonasCobertura']);
		$result = array();
		$i = 0;

		foreach ($zonasCobertura as $key => $value) 
		{
			$value = str_replace("[", "", $value);
			$value = str_replace("]", "", $value);
			$v = explode(',', $value);
			$id_pais = $v[0];
			$id_dpto = $v[1];
			$id_municipio = $v[2];
			$result[$i]= Self::insZonaCobertura($id_pais, $id_dpto, $id_municipio, $id_catering);
			$i++;
		}
		echo json_encode($result);
	}

	public function insZonaCobertura($id_pais, $id_dpto, $id_municipio, $id_catering)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_zona_cobertura_catering @id_pais = ?, @id_dpto = ?, @id_municipio = ?, @id_catering = ?";
		$params = array( $id_pais, $id_dpto, $id_municipio, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2103', $sql, $params);
			return Helpers::returnError(0, 'Error 2103');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_catering = new Catering();

			$mi_catering->mensaje      = utf8_encode($row['mensaje']);
			$mi_catering->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $mi_catering;
		}
	}

	public function updPerfil($post, $files)
	{
		$img = $files['preImg'];
		$nombre = utf8_decode($post['nombreCatering']);
		$descripcion = utf8_decode($post['descripcion']);
		$pedidosMinimos = $post['pedidosMinimos'];

		if($img['name'] != '')
		 	Self::welcomeInsImagen($post, $files);

		$datosUser = Cookies::getDatosUser();
		$id_catering = $datosUser->id_catering;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_datos_catering @id_catering = ?, @nombre = ?, @descripcion = ?, @pedidosMinimos = ?";
		$params = array( $id_catering, $nombre, $descripcion, $pedidosMinimos );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 2201', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 2201');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_catering = new Catering();

			$mi_catering->mensaje      = utf8_encode($row['mensaje']);
			$mi_catering->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $mi_catering );
		}
	}
}
?>