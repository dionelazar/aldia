(function($) {

    'use strict';

    $(document).ready(function() {

        $('#perfilWizard').bootstrapWizard({
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $('#perfilWizard').find('.pager .next').hide();
                    $('#perfilWizard').find('.pager .finish').show().removeClass('disabled hidden');
                } else {
                    $('#perfilWizard').find('.pager .next').show();
                    $('#perfilWizard').find('.pager .finish').hide();
                }

                var li = navigation.find('li.active');

                var btnNext = $('#perfilWizard').find('.pager .next').find('button');
                var btnPrev = $('#perfilWizard').find('.pager .previous').find('button');


                // if ($current > 1 && $current < $total) {

                //     var nextIcon = li.next().find('.fa');
                //     var nextIconClass = nextIcon.attr('class').match(/fa-[\w-]*/).join();

                //     removeIcons(btnNext);
                //     btnNext.addClass(nextIconClass + ' btn-animated from-left fa');

                //     var prevIcon = li.prev().find('.fa');
                //     var prevIconClass = prevIcon.attr('class').match(/fa-[\w-]*/).join();

                //     removeIcons(btnPrev);
                //     btnPrev.addClass(prevIconClass + ' btn-animated from-left fa');
                // } else if ($current == 1) {
                //     // remove classes needed for button animations from previous button
                //     btnPrev.removeClass('btn-animated from-left fa');
                //     removeIcons(btnPrev);
                // } else {
                //     // remove classes needed for button animations from next button
                //     btnNext.removeClass('btn-animated from-left fa');
                //     removeIcons(btnNext);
                // }
            },
            onNext: function(tab, navigation, index) {
                
            },
            onPrevious: function(tab, navigation, index) {
                
            },
            onInit: function() {
                $('#perfilWizard ul').removeClass('nav-pills');
            }

        });

        $("#zonaCobertura").select2({
            dropdownCssClass : 'multi-client'
        });

        // Upload img

        function previewImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgCat').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#preImg").change(function(){
            previewImg(this);
        });
    });

})(window.jQuery);