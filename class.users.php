<?php 
/**
* 
*/
require_once 'class.conection.php';
require_once 'class.email.php';

class Users extends Conection
{
	public $id_usuario;
	public $tipo_usuario;
	public $id_tipo_usuario;
	public $id_catering;
	public $nombre;
	public $apellido;
	public $email;
	public $empresa;

	function login($post)
	{
		$email = $post['email'];
		$pass = $post['pass'];
		$pass = Encriptacion::encriptar($pass);

		$conn = Conection::Conexion();
		$sql = " EXEC sp_get_login @email = ? , @pass = ? ";
		$params = array( $email, $pass );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0001', 'sp_get_login', $params);
			Helpers::jsonEncodeError(0, 'Error 0001');
		}else{
			Cookies::destroyCookies();

			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_user = new Users();

			$mi_user->mensaje      = utf8_encode($row['mensaje']);
			$mi_user->cod_mensaje  = $row['cod_mensaje'];

			if($mi_user->cod_mensaje == -1)
			{
				$mi_user->id_usuario   = $row['id_usuario'];
				$mi_user->tipo_usuario = $row['tipo_usuario'];
				$mi_user->id_catering  = $row['id_catering'];
				$mi_user->nombre       = utf8_encode($row['nombre']);
				$mi_user->apellido     = utf8_encode($row['apellido']);

				Cookies::grabarCookiesLogin($mi_user->id_usuario, $mi_user->tipo_usuario, $mi_user->nombre, $mi_user->apellido, $mi_user->id_catering);
			}

			Conection::CerrarConexion($conn);	
			echo json_encode($mi_user);
		}
	}

	function registroLider($post)
	{
		$nombre = $post['nombre'];
		$apellido = $post['apellido'];
		$email = $post['email'];
		$password = $post['password'];
		$password = Encriptacion::encriptar($password);
		$id_catering = $post['id_catering'];

		$conn = Conection::Conexion();
		$sql = " EXEC sp_ins_user @txt_nombre = ? ,@txt_apellido = ? ,@txt_password = ?, @cod_tipo_usuario = ? ,@txt_email = ? ,@id_catering = ?";
		$params = array( $nombre ,$apellido, $password , 4, $email, $id_catering  );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0002', 'sp_ins_usuario', $params);
			Helpers::jsonEncodeError(0, 'Error 0002');
		}else{
			Cookies::destroyCookies();
			
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_user = new Users();

			$mi_user->mensaje      = utf8_encode($row['mensaje']);
			$mi_user->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);	
			echo json_encode($mi_user);
		}
	}

	function registroCatering($post)
	{
		$codigo_activacion = $post['codigo_activacion'];
		$nombre = $post['nombre'];
		$apellido = $post['apellido'];
		$catering = $post['catering'];
		$email = $post['email'];
		$password = $post['password'];
		$password = Encriptacion::encriptar($password);

		$conn = Conection::Conexion();
		$sql = " EXEC sp_ins_catering @codigo_activacion = ?, @nombre = ?, @apellido = ?, @catering = ?, @email = ?, @password = ? ";
		$params = array( $codigo_activacion ,$nombre ,$apellido, $catering ,$email ,$password );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0003', 'sp_ins_catering', $params);
			Helpers::jsonEncodeError(0, 'Error 0003');
		}else{
			$response = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_user = new Users();

				$mi_user->mensaje      = utf8_encode($row['mensaje']);
				$mi_user->cod_mensaje  = $row['cod_mensaje'];
				$response[$i] = $mi_user;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			echo json_encode($response);
		}
	}

	function comprobarCodigoActivacion($cod_activacion)
	{
		$conn = Conection::Conexion();
		$sql = " EXEC sp_verificar_codigo_activacion @txt_cod = ?";
		$params = array( $cod_activacion );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0004', 'sp_verificar_codigo_activacion', $params);
			return Helpers::returnError(0, 'Error 0004');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_user = new Users();

			$mi_user->mensaje      = utf8_encode($row['mensaje']);
			$mi_user->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);	
			return $mi_user;
		}
	}

	public function getClientes()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_clientes_by_usuario_catering @id_usuario = ?";
		$params = array( $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0005', $sql, $params);
			return Helpers::returnError(0, 'Error 0005');
		}else{
			$clientes = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_cliente = new Users();
				$mi_cliente->id_usuario = $row['id_usuario'];
				$mi_cliente->id_tipo_usuario = $row['id_tipo_usuario'];
				$mi_cliente->nombre = utf8_encode($row['nombre']);
				$mi_cliente->apellido = utf8_encode($row['apellido']);
				$mi_cliente->email = utf8_encode($row['email']);

				$clientes[$i] = $mi_cliente;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			return $clientes;
		}
	}

	public function getClientesAjax($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_clientes_by_usuario_catering @id_usuario = ?";
		$params = array( $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0005', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 0005');
		}else{
			$clientes = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_cliente = new Users();
				$mi_cliente->id_usuario = $row['id_usuario'];
				$mi_cliente->id_tipo_usuario = $row['id_tipo_usuario'];
				$mi_cliente->nombre = utf8_encode($row['nombre']);
				$mi_cliente->apellido = utf8_encode($row['apellido']);
				$mi_cliente->email = utf8_encode($row['email']);
				$mi_cliente->calle = utf8_encode($row['calle']);
				$mi_cliente->altura = utf8_encode($row['altura']);
				$mi_cliente->piso = utf8_encode($row['piso']);
				$mi_cliente->oficina = utf8_encode($row['oficina']);
				$mi_cliente->municipio = utf8_encode($row['municipio']);
				$mi_cliente->empresa = utf8_encode($row['empresa']);

				$clientes[$i] = $mi_cliente;
				$i++;
			}

			Conection::CerrarConexion($conn);	
			echo json_encode( $clientes );
		}
	}

	public function getMiUsuario($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuario @id_usuario = ?";
		$params = array( $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0007', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 0007');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$usuario = new Users();

			$usuario->id_tipo_usuario = $row['id_tipo_usuario'];
			$usuario->nombre = utf8_encode($row['nombre']);
			$usuario->apellido = utf8_encode($row['apellido']);
			$usuario->email = $row['email'];
			$usuario->telefono = $row['telefono'];

			Conection::CerrarConexion($conn);
			echo json_encode( $usuario );
		}
	}

	public function enviarInvitacionCliente($post)
	{
		$clientes = json_decode($post['emails']);

		$email = new Email();
		$email->invitarCliente($clientes);
	}

	public function updDatosUsuario($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$nombre = utf8_decode($post['nombre']);
		$apellido = utf8_decode($post['apellido']);
		$email = $post['email'];
		$telefono = $post['telefono'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_datos_usuario @id_usuario = ?, @nombre = ?, @apellido = ?, @email = ?, @telefono = ?";
		$params = array( $id_usuario, $nombre, $apellido, $email, $telefono );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 0102', $sql, $params);
			return Helpers::returnError(0, 'Error 0102');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$mi_cliente = new Users();
			$mi_cliente->cod_mensaje = $row['cod_mensaje'];
			$mi_cliente->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);	
			echo json_encode( $mi_cliente );
		}
	}

	function logout($post)
	{
		Cookies::destroyCookies();
		header('location:login');
	}
}
?>