$(document).ready(function(){
    armarSelectZonasCobertura();
});

var actualTab = 1;

function getPaises()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getPaises'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function getProvincias(cod_pais)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getProvincias'
            ,cod_pais : cod_pais
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function getMunicipios(cod_pais, cod_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'localidades'
            ,call  : 'getMunicipios'
            ,cod_pais : cod_pais
            ,cod_provincia : cod_provincia
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarBarrios(miData);
        }
    });
}

function armarSelectZonasCobertura()
{
    getMunicipios(1,1);
    getMunicipios(1,2);
}

function armarBarrios(Data)
{
    html = '';
    html += '<optgroup label="'+Data[0].provincia+'">';
    for (var i = 0; i < Data.length; i++) 
    {
        html += '<option value="['+Data[i].id_pais+','+Data[i].id_provincia+','+Data[i].id_municipio+']">'+Data[i].municipio+'</option>';
    }
    html += '</optgroup>';

    $('#zonaCobertura').append(html);
}

function finalizarPerfil()
{
    var zonasCobertura = $('#zonaCobertura').val();
}

function continuar()
{
    if(actualTab == 1)
        subirImagen();

    if(actualTab == 2)
        subirDescripcion();

    if(actualTab == 3)
        subirZonaCobertura();

    actualTab++;
}

function anterior()
{
    actualTab--;
}

function selectab(tab)
{
    actualTab = tab;
}

function subirImagen()
{
    var formData = new FormData(document.getElementById("formImg"));
        formData.append("action", "catering");
        formData.append("call", "welcomeInsImagen");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "direccionadorAjax.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){debugger
            miData = JSON.parse(Data);
        }
    });
}

function subirDescripcion()
{
    var descripcionCatering = $('#descripcionCatering').val();
    var cantidadMinima = $('#cantidadMinima').val();

    if(descripcionCatering.length < 2 && !parseInt(cantidadMinima).isInteger())
        return false;
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'catering'
            ,call  : 'welcomeInsAbout'
            ,descripcionCatering : descripcionCatering
            ,cantidadMinima : cantidadMinima
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
        }
    });
}

function subirZonaCobertura()
{
    var zonasCobertura = JSON.stringify($('#zonaCobertura').val());

    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'catering'
            ,call  : 'insZonasCobertura'
            ,zonasCobertura : zonasCobertura
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
        }
    });
}