$(document).ready(function(){
	getMenues();
});

function getMenues()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'menues'
            ,call  : 'getMenues'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.length == 0)
            	location.replace('menuwelcomecatering');
            else
            	armarMenues(miData);
        }
    });
}

function armarMenues(data)
{
	console.log(data);
	var col = 0;

	for (var i = 0; i < data.length; i++) 
	{		debugger
		if(col > 3)
			col = 0;

		var html = '';

		if(col == 0 || i%4 == 0)
			html += '<div class="row">';

		html += '<div class="col-md-3">';
	      html += '<a href="menu&'+data[i].id_menu +'&'+data[i].id_catering +'" class="menu-card">';
	        html += '<div class="menu-card-content">';
	          html += '<h4>Menú del '+ data[i].fecha_desde +' al '+ data[i].fecha_hasta +'</h4>';
	          html += '<strong>Descripción</strong>';
	          html += '<p>'+data[i].txt_desc+'</p>';
	          html += '<strong>Visible para:</strong>';
	          html += '<div class="menu-for">';
	          	$.each(data[i].clientes, function(key, value){
	          		html += '<span class="label label-info">'+value.nombre+' '+value.apellido+'</span>';
	          	})
	          if(data[i].id_estado == 3)
	          	html += '<button data-toggle="modal" data-target="#modalRepublicar" class="btn btn-default btn-block btn-republicar m-t-10"><i class="icon-retweet"></i> Republicar</button>'
	          html += '</div>';
	        html += '</div>';
	      html += '</a>';
	    html += '</div>';

	    if(col == 3 || i == data.length)
	    	html += '</div>';

	    if(data[i].id_estado != 3)
	    	$('#bodyVigentes').append(html);
	    else
	    	$('#bodyFinalizados').append(html);

	    col++;
	}
}