$(document).ready(function(){
	getClientes();
});

$('#sendInvitations').click(function(){
	enviarInvitacion();
});

function getClientes()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'getClientesAjax'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarBodyClients(miData);
        }
    });
}

function armarBodyClients(data)
{
	var html = '';

	for (var i = 0; i < data.length; i++) 
	{
		html +='<tr>';
		html +='	<td class="v-align-middle semi-bold">';
		html +='		<p><a href="empresa">'+data[i].empresa+'</a></p>';
		html +='	</td>';
		html +='	<td class="v-align-middle text-center">';
		html +='		<p>'+data[i].nombre+' '+data[i].apellido+'(<a href="#0">'+data[i].email+'</a>)</p>';
		html +='	</td>';
		html +='	<td class="v-align-middle text-right">';
		html +='		<p>'+data[i].calle+' '+data[i].altura+' '+data[i].piso+'°'+data[i].oficina+', '+data[i].municipio+'</p>';
		html +='	</td>';
    	html +='</tr>';
    }
    $('#bodyClients').html(html);
}

function enviarInvitacion()
{
	if(validarEmails())
	{
		var txt_emails = $('#emailTag').val();
		var emails = JSON.stringify(txt_emails.split(","));

		$.ajax({
	        type: 'POST',
	        url: 'direccionadorAjax.php',
	        datatype: 'html',
	        data: {
	             action: 'users'
	            ,call  : 'enviarInvitacionCliente'
	            ,emails : emails
	        },
	        success : function(Data){
	            var miData = JSON.parse(Data);	            
	            $('#formCont').slideUp();
	            $('.modal-header h5').hide();
	            $('#confirmCont').slideDown();
	        }
	    });
	}else{
		alert('Deben ser emails válidos');
	}
}

function validarEmails()
{
	var txt_emails = $('#emailTag').val();
	var emails = txt_emails.split(",");

	for (var i = 0; i < emails.length; i++) 
	{
		if(!isEmail(emails[i]))
			return false;
	}

	return true;
}