<?php 
/**
* 
*/
require_once 'class.conection.php';

class Localidades extends Conection
{
	public $cod_mensaje;
	public $mensaje;
	public $id_pais;
	public $pais;
	public $id_provincia;
	public $provincia;
	public $id_municipio;
	public $municipio;

	function getPaises()
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_paises";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 3001', 'sp_get_login', $params);
			Helpers::jsonEncodeError(0, 'Error 3001');
		}else{
			$paises = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_pais = new Localidades();

				$mi_pais->id_pais   = $row['id_pais'];
				$mi_pais->pais      = utf8_encode($row['pais']);

				$paises[$i] = $mi_pais;
				$i++;
			}
			Conection::CerrarConexion($conn);	
			echo json_encode($paises);
		}
	}

	function getProvincias($post)
	{
		$cod_pais = $post['cod_pais'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_provincias @cod_pais = ?";
		$params = array($cod_pais);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 3002', 'sp_get_provincias', $params);
			Helpers::jsonEncodeError(0, 'Error 3002');
		}else{
			$provincias = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_provincia = new Localidades();

				$mi_provincia->id_pais             = $row['id_pais'];
				$mi_provincia->id_provincia   = $row['id_provincia'];
				$mi_provincia->provincia      = utf8_encode($row['provincia']);

				$provincias[$i] = $mi_provincia;
				$i++;
			}
			Conection::CerrarConexion($conn);	
			echo json_encode($provincias);
		}
	}

	function getMunicipios($post)
	{
		$cod_pais = $post['cod_pais'];
		$cod_provincia = $post['cod_provincia'];
		//también salen los barrios en caso de capital federal
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_municipios @cod_pais = ?, @cod_provincia = ?";
		$params = array($cod_pais, $cod_provincia);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 3003', 'sp_get_municipios', $params);
			Helpers::jsonEncodeError(0, 'Error 3003');
		}else{
			$municipios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_municipio = new Localidades();

				$mi_municipio->id_pais        = $row['id_pais'];
				$mi_municipio->id_provincia   = $row['id_provincia'];
				$mi_municipio->id_municipio   = $row['id_municipio'];
				$mi_municipio->provincia      = utf8_encode($row['provincia']);
				$mi_municipio->municipio      = utf8_encode($row['municipio']);

				$municipios[$i] = $mi_municipio;
				$i++;
			}
			Conection::CerrarConexion($conn);	
			echo json_encode($municipios);
		}
	}
}
?>