function login()
{
	var mail = $('#mailLogin').val();
	var pass = $('#passwordLogin').val();

	if(!validarLogin()) return false;
	
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'login'
            ,email : mail
            ,pass : pass
        },
        success : function(Data){
            console.log(Data);
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
            {
            	window.location.replace('index');
            }else{
            	alert(miData.mensaje);
            }
        }
    });
}

function validarLogin()
{
	if( $('#mailLogin-error').text().length > 0 )
		return false;

	if( $('#passwordLogin-error').text().length > 0 )
		return false;

	return true;
}