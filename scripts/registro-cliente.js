function registrarCliente()
{
	if(!validarRegistro()) return false;

	var nombre = $('#nameLogin').val();
	var apellido = $('#apellidoLogin').val();
	var email = $('#mailLogin').val();
	var password = $('#passwordLogin').val();
	var id_catering = window.location.href.split('&')[1];

	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'users'
            ,call  : 'registroLider'
            ,nombre : nombre
			,apellido : apellido
			,email : email
			,password : password
			,id_catering : id_catering
        },
        success : function(Data){
            console.log(Data);
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1){
            	window.location.replace('index');
            }else{
            	alert(miData.mensaje);
            }
        }
    });
}

function validarRegistro()
{
	var nombre = $('#nameLogin').val();
	var apellido = $('#apellidoLogin').val();
	var email = $('#mailLogin').val();
	var password = $('#passwordLogin').val();

	if( $('#nameLogin-error').text().length > 0 || nombre.length < 1)
		return false;

	if( $('#apellidoLogin-error').text().length > 0 || apellido.length < 1)
		return false;

	if( $('#mailLogin-error').text().length > 0 || email.length < 1)
		return false;

	if( $('#passwordLogin-error').text().length > 0 || password.length < 1)
		return false;

	return true;
}