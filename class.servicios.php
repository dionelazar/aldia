<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';

class Servicios extends Conection
{
	public $id_tipo_servicio;
	public $servicio;
	public $cod_mensaje;
	public $mensaje;
	
	public function getTiposServicios()
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_tipo_servicio";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 4001', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 4001');
		}else{
			$servicios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_servicio = new Servicios();
				$mi_servicio->id_tipo_servicio = $row['id_tipo_servicio'];
				$mi_servicio->servicio = utf8_encode($row['servicio']);

				$servicios[$i] = $mi_servicio;
				$i++;
			}
			Conection::CerrarConexion($conn);	
			return $servicios;
		}
	}
}
?>