<?php 
	if(isset($_POST['action']))
	{
		switch ($_POST['action']) //action seria la class , despues iria el metodo (call)
		{
			case 'users':
				require_once 'class.users.php';
				$metodo = $_POST['call'];
				Users::$metodo($_POST);
				break;

			case 'menues':
				require_once 'class.menues.php';
				$metodo = $_POST['call'];
				Menues::$metodo($_POST);
				break;

			case 'platos':
				require_once 'class.platos.php';
				$metodo = $_POST['call'];
				Platos::$metodo($_POST);
				break;

			case 'catering':
				require_once 'class.catering.php';
				$metodo = $_POST['call'];
				if(isset($_FILES))
					Catering::$metodo($_POST, $_FILES);
				else
					Catering::$metodo($_POST);
				break;

			case 'localidades':
				require_once 'class.localidades.php';
				$metodo = $_POST['call'];
				Localidades::$metodo($_POST);
				break;

			case 'direccion':
				require_once 'class.direccion.php';
				$metodo = $_POST['call'];
				Direccion::$metodo($_POST);
				break;

			default:
				$class = ucwords($_POST['action']);
				require_once 'class.'.$class.'.php';
				$metodo = $_POST['call'];
				$class::$metodo($_POST);
				break;
		}
	}
?>