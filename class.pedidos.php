<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';

class Pedidos extends Conection
{
	public $mensaje;
	public $cod_mensaje;
	public $id_pedido;
	public $id_catering;
	public $fecha;
	public $comentario;
	public $precio;
	public $id_estado;
	public $fecha_desde; //fecha del menu
	public $fecha_hasta; //fecha del menu

	function getPedidosPendientes($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_pedidos_pendientes @id_usuario = ?";
		$params = array( $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8001', 'sp_get_pedidos_pendientes', $params);
			Helpers::jsonEncodeError(0, 'Error 8001');
		}else{
			$pedidos = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_pedido = new Pedidos();

				$mi_pedido->id_pedido   = $row['id_pedido'];
				$mi_pedido->id_catering = $row['id_catering'];
				$mi_pedido->fecha       = $row['fecha']->format('d/m/y');
				$mi_pedido->comentario  = utf8_encode($row['comentario']);
				$mi_pedido->precio      = $row['precio'];
				$mi_pedido->fecha_desde = $row['fecha_desde']->format('d/m/y');
				$mi_pedido->fecha_hasta = $row['fecha_hasta']->format('d/m/y');
				$mi_pedido->id_estado   = $row['id_estado'];
				$mi_pedido->estado      = utf8_encode($row['estado']);

				$pedidos[$i] = $mi_pedido;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $pedidos );
		}
	}

	function getPlatosPedido($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_pedido = $post['id_pedido'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_platos_from_pedido @id_usuario = ?, @id_pedido = ?";
		$params = array( $id_usuario, $id_pedido );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8002', 'sp_get_platos_from_pedido', $params);
			Helpers::jsonEncodeError(0, 'Error 8002');
		}else{
			$platos = array();
			$i = 0;
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_plato = new Pedidos();

				$mi_plato->id_pedido     = $row['id_pedido'];
				$mi_plato->id_plato      = $row['id_plato'];
				$mi_plato->id_menu       = $row['id_menu'];
				$mi_plato->id_catering   = $row['id_catering'];
				$mi_plato->fecha_pedido  = $row['fecha_pedido']->format('d/m/y');
				$mi_plato->txt_desc      = utf8_encode($row['txt_desc']);
				$mi_plato->cantidad      = $row['cantidad'];
				$mi_plato->plato         = utf8_encode($row['plato']);
				$mi_plato->precio        = $row['precio'];
				$mi_plato->fecha_plato   = $row['fecha_plato']->format('Ymd');

				$platos[$i] = $mi_plato;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $platos );
		}
	}

	function insPedido($post)
	{
		$miPedido = json_decode($post['pedido']);
		$comentario = $post['comentario'];
		$precio = $post['precio'];
		$id_usuario = Cookies::getIdUsuario();
		
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_pedido @id_usuario = ? , @comentario = ?, @precio = ?";
		$params = array( $id_usuario, utf8_decode($comentario), $precio );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8101', 'sp_ins_pedido', $params);
			Helpers::jsonEncodeError(0, 'Error 8101');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$pedido = new Pedidos();

			$pedido->cod_mensaje = $row['cod_mensaje'];
			$pedido->mensaje = $row['mensaje'];
			$pedido->id_pedido = $row['id_pedido'];

			Conection::CerrarConexion($conn);
			foreach ($miPedido as $key => $value) {
				Self::insPlatosPedido($pedido->id_pedido, $value->id_catering, $value->id_menu, $value->id_plato, $value->cantidad, null);
			}
			echo json_encode($pedido);
		}
	}

	function insPlatosPedido($id_pedido, $id_catering, $id_menu, $id_plato, $cantidad, $comentario)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_plato_in_pedido @id_pedido = ? , @id_catering = ?, @id_menu = ?, @id_plato = ?, @cantidad = ?, @comentario = ?";
		$params = array( $id_pedido, $id_catering, $id_menu, $id_plato, $cantidad, "NULL" );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8102', 'sp_ins_pedido', $params);
			Helpers::jsonEncodeError(0, 'Error 8102');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$pedido = new Pedidos();

			$pedido->cod_mensaje = $row['cod_mensaje'];
			$pedido->mensaje = $row['mensaje'];

			Conection::CerrarConexion($conn);
			return $pedido;
		}
	}

	public function confirmarPedido($post)
	{
		$id_pedido = $post['id_pedido'];
		$id_usuario = Cookies::getIdUsuario();
		
		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_confirmar_pedido @id_usuario = ? , @id_pedido = ?";
		$params = array( $id_usuario, $id_pedido );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8201', 'sp_upd_aceptar_pedido', $params);
			Helpers::jsonEncodeError(0, 'Error 8101');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$pedido = new Pedidos();

			$pedido->cod_mensaje = $row['cod_mensaje'];
			$pedido->mensaje = $row['mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode($pedido);
		}
	}

	public function rechazarPedido($post)
	{
		$id_pedido = $post['id_pedido'];
		$id_usuario = Cookies::getIdUsuario();
		
		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_rechazar_pedido @id_usuario = ? , @id_pedido = ?";
		$params = array( $id_usuario, $id_pedido );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 8202', 'sp_upd_aceptar_pedido', $params);
			Helpers::jsonEncodeError(0, 'Error 8102');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$pedido = new Pedidos();

			$pedido->cod_mensaje = $row['cod_mensaje'];
			$pedido->mensaje = $row['mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode($pedido);
		}
	}
}
 ?>