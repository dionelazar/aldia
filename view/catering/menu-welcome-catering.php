<?php 
  $titulo = 'Aldia | Menú';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <div class="panel-body">
                <h2 class="text-center font-montserrat menu-welcome-heading"><?php echo $catering->nombre; ?> aún no tiene ningún menú vigente</h2>

                <img class="hungry-icon center-block" src="img/hungry.svg" alt="icono hambriento">

                <p class="text-center menu-text">En Aldia hay muchos clientes hambrientos esperando que armes el menú de esta semana</p>

                <a href="nuevomenu" class="btn btn-primary btn-lg btn-menu-welcome center-block">
                  <span>Armar Menú</span>
                </a>
              </div>
            </div> <!-- Termina panel de clientes -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>

    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
  </body>
</html>