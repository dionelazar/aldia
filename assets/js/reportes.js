$(document).ready(function(){
var configDiario = {
    type: 'line',
    data: {
        labels: ["Lun", "Mar", "Mie", "Jue", "Vie"],
        datasets: [{
            data: [8, 4, 10, 5, 15, 10],
            borderColor:'rgba(255,99,132,1)',
            pointBackgroundColor:'rgba(255,99,132,1)',
            fill: false,
        }]
    },
    options: {
        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
                label: function(tooltipItems, data) { 
                    return tooltipItems.yLabel + ' Pedidos recibidos';
                }
            },
            custom: function(tooltip) {
                if (!tooltip) return;
                tooltip.displayColors = false;
            },
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Pedidos recibidos'
                },
                ticks : {
                    min : 0
                }
            }],
            xAxes: [{
                gridLines: { display: false } 
            }]
        }
    }
};

var configSemanal = {
    type: 'line',
    data: {
        labels: ["1-7 de julio", "8-14 de julio", "15-21 de julio", "22-31 de julio"],
        datasets: [{
            data: [55, 35, 88, 20],
            borderColor:'rgba(255,99,132,1)',
            pointBackgroundColor:'rgba(255,99,132,1)',
            fill: false,
        }]
    },
    options: {
        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
                label: function(tooltipItems, data) { 
                    return tooltipItems.yLabel + ' Pedidos recibidos';
                }
            },
            custom: function(tooltip) {
                if (!tooltip) return;
                tooltip.displayColors = false;
            },
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Pedidos recibidos'
                },
                ticks : {
                    min : 0
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                },
                gridLines: { display: false } 

            }]
        }
    }
};

var configMensual = {
    type: 'line',
    data: {
        labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        datasets: [{
            data: [350, 180, 100, 55, 150, 250, 400, 300, 350, 200, 170, 320],
            borderColor:'rgba(255,99,132,1)',
            pointBackgroundColor:'rgba(255,99,132,1)',
            fill: false,
        }]
    },
    options: {
        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
                label: function(tooltipItems, data) { 
                    return tooltipItems.yLabel + ' Pedidos recibidos';
                }
            },
            custom: function(tooltip) {
                if (!tooltip) return;
                tooltip.displayColors = false;
            },
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Pedidos recibidos'
                },
                ticks : {
                    min : 0
                }
            }],
            xAxes: [{
                gridLines: { display: false } 
            }]
        }
    }
};

var barChartDataPlatos = {
    labels: ["Milanesa c/ Mixta", "Ñoquis con Salsa", "Ensalada Cesar", "Morcilla Tucumana"],
    datasets: [{
        label: 'Dataset 1',
        backgroundColor: 'rgba(255,99,132,1)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        data: [350, 180, 100, 55]
    }]

};

var barChartDataClientes = {
    labels: ["Cliente 1", "Cliente 2", "Cliente 3", "Cliente 4", "Cliente 5", "Cliente 6", "Cliente 7"],
    datasets: [{
        label: 'Dataset 1',
        backgroundColor: 'rgba(255,99,132,1)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        data: [30, 50, 25, 45, 80, 55, 23]
    }]

};

window.onload = function() {
    var pedidosDiarios = document.getElementById("pedidosDiarios").getContext("2d");
    window.myLine = new Chart(pedidosDiarios, configDiario);

    var pedidosSemanales = document.getElementById("pedidosSemanales").getContext("2d");
    window.myLine = new Chart(pedidosSemanales, configSemanal);

    var pedidosMensuales = document.getElementById("pedidosMensuales").getContext("2d");
    window.myLine = new Chart(pedidosMensuales, configMensual);

    var rankingPlatos = document.getElementById("rankingPlatos").getContext("2d");
    window.myBar = new Chart(rankingPlatos, {
        type: 'bar',
        data: barChartDataPlatos,
        options: {
            legend: {
                position: false,
            },
            title: {
                display: false,
            },

            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function(tooltipItems, data) { 
                        return tooltipItems.yLabel + ' Platos pedidos';
                    }
                },
                custom: function(tooltip) {
                    if (!tooltip) return;
                    tooltip.displayColors = false;
                },
            },
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Cantidad de platos'
                    },
                    ticks : {
                        min : 0
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                    },
                    gridLines: { display: false } 

                }]
            }
        }
    });

    var pedidosClientes = document.getElementById("pedidosClientes").getContext("2d");
    window.myBar = new Chart(pedidosClientes, {
        type: 'bar',
        data: barChartDataClientes,
        options: {
            legend: {
                position: false,
            },
            title: {
                display: false,
            },

            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function(tooltipItems, data) { 
                        return tooltipItems.yLabel + ' Platos pedidos';
                    }
                },
                custom: function(tooltip) {
                    if (!tooltip) return;
                    tooltip.displayColors = false;
                },
            },
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Pedidos hechos'
                    },
                    ticks : {
                        min : 0
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                    },
                    gridLines: { display: false } 

                }]
            }
        }
    });
};





var mywindow = $(window);
var mypos = mywindow.scrollTop();
var up = false;
var newscroll;

mywindow.scroll(function () {
    newscroll = mywindow.scrollTop();
    if (newscroll > mypos && !up) {
        $('body').stop().removeClass('has-subnav-up');
        $('body').stop().addClass('has-subnav');
        up = !up;
        console.log(up);
    }if (newscroll == 0) {
        $('body').stop().removeClass('has-subnav-up');
    } else if(newscroll < mypos && up) {
        $('body').stop().removeClass('has-subnav');
        $('body').stop().addClass('has-subnav-up');
        up = !up;
    }
    mypos = newscroll;
});

});

