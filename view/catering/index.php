<?php
  require_once '../../Controllers/CateringController.php';
  $controller = new CateringController();
  $titulo = 'Aldia | Catering';
  require_once '../header.catering.php';
?>

      
      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
            <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <div class="row">
              <div class="col-md-6">
                <a href="nuevomenu" class="menu-card menu-add-plato menu-add-home m-t-0 text-center">
                  <div class="menu-card-content">
                    <i class="icon" data-icon="&#xe035;"></i>
                    <p>Agrega un nuevo menú</p>
                  </div>
                </a>

                <div class="panel">
                  <div class="panel-body">
                    <div class="menu-report-container">
                      <div class="icon-container pull-left">
                        <i class="icon-user-group"></i>
                      </div>
                      <div class="data-container pull-left">
                        <p class="title-report"><span class="bold">60</span> Clientes activos</p>
                      </div>
                      <a href="#0" class="btn btn-default pull-right m-t-15">Ver más</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="panel">
                  <div class="panel-body">
                    <div class="menu-report-container">
                      <div class="icon-container pull-left">
                        <img src="img/menu-icon-green.svg" alt="">
                      </div>
                      <div class="data-container pull-left">
                        <p class="title-report"><span class="bold">20</span> Menús vigentes</p>
                      </div>
                      <a href="#0" class="btn btn-default pull-right m-t-15">Ver más</a>
                    </div>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-body">
                    <div class="menu-report-container">
                      <div class="icon-container pull-left">
                        <img src="img/menu-icon-red.svg" alt="">
                      </div>
                      <div class="data-container pull-left">
                        <p class="title-report"><span class="bold">5</span> Menús finalizados</p>
                      </div>
                      <a href="#0" class="btn btn-default pull-right m-t-15">Ver más</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>  

            <div class="row">
              <div class="col-md-6">
                <div class="panel">
                  <div class="panel-heading">
                    <div class="panel-title"><i class="icon-pin"></i> Historial de pedidos</div>
                  </div>
                  <table class="table table-hover" id="tableHistorial">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>Menú semana</th>
                        <th>Monto</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="perfil">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="perfil">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="perfil">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>     
                </div>
              </div>

              <div class="col-md-6">
                <div class="panel">
                  <div class="panel-heading">
                    <div class="panel-title"><i class="icon-pin"></i> Pedidos recibidos</div>
                  </div>

                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <ul class="nav nav-pills pull-right xs-pull-reset">
                          <li class="active"><a data-toggle="tab" href="#diarios">Diarios</a></li>
                          <li><a data-toggle="tab" href="#semanales">Semanales</a></li>
                          <li><a data-toggle="tab" href="#mensuales">Mensuales</a></li>
                        </ul>    
                      </div>
                    </div>      

                    <div class="tab-content m-t-20">
                      <div id="diarios" class="tab-pane fade in active">
                        <canvas id="pedidosDiarios"></canvas>
                      </div>

                      <div id="semanales" class="tab-pane fade">
                        <canvas id="pedidosSemanales"></canvas>
                      </div>

                      <div id="mensuales" class="tab-pane fade">
                        <canvas id="pedidosMensuales"></canvas>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
            </div>    
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    <div class="modal fade slide-up disable-scroll" id="modalComentarios" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross fs-14"></i>
              </button>
              <h5>Menú del 2-5 al 5/5 - <span class="semi-bold">Arcor</span></h5>
            </div>

            <div class="modal-body">
              <p class="pedido-comentario m-t-20">"Este es un comentario que hicieron los usuarios clientes en el pedido Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"</p>

              <button class="btn btn-primary btn-lg btn-block m-t-30" data-dismiss="modal">Entendido</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/chart.min.js" type="text/javascript"></script>
    <script src="assets/js/index.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
  </body>
</html>