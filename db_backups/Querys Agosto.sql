CREATE PROC sp_upd_datos_catering
(
	 @id_catering INT
	,@nombre VARCHAR(200)
	,@descripcion VARCHAR(MAX)
	,@pedidosMinimos INT
)
AS BEGIN
SET NOCOUNT ON
	UPDATE catering SET
		 txt_desc = @nombre
		,txt_about = @descripcion
		,pedidos_minimos = @pedidosMinimos
	WHERE id_catering = @id_catering

	SELECT -1 'cod_mensaje'
	       ,'El perfil fue editado correctamente' 'mensaje'
END