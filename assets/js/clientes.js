(function($) {

    'use strict';
    $(document).ready(function() {

        // llama al plugin de tags
        $('#emailTag').tagsinput();

        
        $('#sendInvitations').on('click', function(){
            $('#formCont').slideUp();
            $('.modal-header h5').hide();
            $('#confirmCont').slideDown();
        });

        $('#okConfirm').on('click', function(){
            $('#formCont').slideDown();
            $('.modal-header h5').show();
            $('#confirmCont').slideUp();
        });
    });

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 720
    };

    var initTableWithSearch = function() {
        var table = $('#tableHistorial');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Est&aacute;s viendo del <b>_START_ al _END_</b> de un total de _TOTAL_ clientes"
            },
            "iDisplayLength": 10
        };

        table.dataTable(settings);
    }

    if ($(window).width() <= 768) {
        var initTableWithSearch = function() {
            var table = $('#tableClients');

            var settings = {
                "sDom": "<t><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "paginate": false,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Est&aacute;s viendo del <b>_START_ al _END_</b> de un total de _TOTAL_ clientes"
                },
                "iDisplayLength": 10
            };

            table.dataTable(settings);
        }    
    }

    initTableWithSearch();        

    $('#tablePedidos').stacktable();
})(window.jQuery);