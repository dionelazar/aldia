jQuery(document).ready(function($){ 
    $('.open-plato-details').each(function() {
        var menuDetails = $(this).prev('.timeline-pedidos-hidden');
        var btnText = $(this).children('.change-text');
        var angleDown = $(this).children('.icon-chevron-down');

        $(this).click(function(e) {
            menuDetails.slideToggle();
            
            btnText.text(function(i, text){
                return text === "Ver detalles del pedido" ? "Ocultar detalles" : "Ver detalles del pedido";
            });

            $('html, body').animate({
                scrollTop: menuDetails.offset().top - 160
            }, 1000);

            angleDown.toggleClass('icon-chevron-down icon-chevron-up');
        });
    });

    $('.btn-confirmar').each(function(){
        var confirmMsg = $(this).parents('.btn-group-pedidos').next('.pedido-confirmado');
        var thisParent = $(this).parents('.btn-group-pedidos');

        $(this).click(function(e) {
            thisParent.hide();
            confirmMsg.show();
        });
    });

    $('.btn-rechazar').each(function(){
        var deniedMsg = $(this).parents('.btn-group-pedidos').next('.pedido-confirmado').next('.pedido-rechazado');
        var thisParent = $(this).parents('.btn-group-pedidos');

        $(this).click(function(e) {
            thisParent.hide();
            deniedMsg.show();
        });
    });

    if ($(window).width() <= 770) { 
        $('.btn-group-pedidos').addClass('btn-group-justified');
        $('.pedido-confirmado').removeClass('pull-right')
        $('.pedido-rechazado').removeClass('pull-right')
    } else {
        $('.btn-group-pedidos').removeClass('btn-group-justified');
        $('.pedido-confirmado').addClass('pull-right')
        $('.pedido-rechazado').addClass('pull-right')
    }

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 720
    };

    var initTableWithSearch = function() {
        var table = $('#tableHistorial');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "paginate": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": ""
            },
            "iDisplayLength": 10
        };
        table.dataTable(settings);
    }
    initTableWithSearch();

    //$('#tableClients').stacktable();
});    