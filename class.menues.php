<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';
require_once __DIR__.'/class.users.php';

class Menues extends Conection
{
	public $cod_mensaje;
	public $mensaje;

	public $id_menu;
	public $id_tipo_servicio;
	public $tipo_servicio;
	public $fecha_desde;
	public $fecha_hasta;
	public $clientes;
	public $txt_desc;
	public $id_estado;


	public function getMenu($id_menu, $id_catering)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_menu @id_menu = ?, @id_catering = ?";
		$params = array( $id_menu, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5001', $sql, $params);
			return Helpers::returnError(0, 'Error 5001');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_menu = new Menues();

			$mi_menu->id_menu = $id_menu;
			$mi_menu->id_tipo_servicio = $row['id_tipo_servicio'];
			$mi_menu->tipo_servicio = utf8_encode($row['tipo_servicio']);
			$mi_menu->fecha_desde = $row['fecha_desde']->format('d/m/Y');
			$mi_menu->fecha_hasta = $row['fecha_hasta']->format('d/m/Y');
			$mi_menu->txt_desc = $row['txt_desc'];
			$mi_menu->id_estado = $row['id_estado'];
			$mi_menu->clientes = Self::getClientesMenu($id_menu, $id_catering);

			Conection::CerrarConexion($conn);
			return $mi_menu;
		}
	}

	public function getMenues($post)//$id_menu, $id_catering
	{
		$datosUser = Cookies::getDatosUser();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_all_menues @id_usuario = ?";
		$params = array( $datosUser->id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5002', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 5002');
		}else{			
			$i = 0;
			$menues = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_menu = new Menues();

				$mi_menu->id_menu = $row['id_menu'];
				$mi_menu->id_tipo_servicio = $row['id_tipo_servicio'];
				$mi_menu->tipo_servicio = utf8_encode($row['tipo_servicio']);
				$mi_menu->fecha_desde = $row['fecha_desde']->format('d-m-Y');
				$mi_menu->fecha_hasta = $row['fecha_hasta']->format('d-m-Y');
				$mi_menu->txt_desc = $row['txt_desc'];
				$mi_menu->id_estado = $row['id_estado'];
				$mi_menu->id_catering = $datosUser->id_catering;
				$mi_menu->clientes = Self::getClientesMenu($mi_menu->id_menu, $datosUser->id_catering);

				$menues[$i] = $mi_menu;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $menues );
		}
	}

	public function getClientesMenu($id_menu, $id_catering)//$id_menu, $id_catering
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_clientes_menu @id_menu = ?, @id_catering = ?";
		$params = array( $id_menu, $id_catering );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5003', $sql, $params);
			return Helpers::returnError(0, 'Error 5003');
		}else{
			$i = 0;
			$clientes = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_cliente = new Users();

				$mi_cliente->id_usuario = $row['id_usuario'];
				$mi_cliente->nombre = $row['nombre'];
				$mi_cliente->apellido = $row['apellido'];
				$mi_cliente->email = $row['email'];

				$clientes[$i] = $mi_cliente;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $clientes;
		}
	}

	public function getMenuesVigentes($post)
	{
		$datosUser = Cookies::getDatosUser();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_menues_vigentes @id_usuario = ?";
		$params = array( $datosUser->id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5002', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 5002');
		}else{			
			$i = 0;
			$menues = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_menu = new Menues();

				$mi_menu->id_menu = $row['id_menu'];
				$mi_menu->id_tipo_servicio = $row['id_tipo_servicio'];
				$mi_menu->tipo_servicio = utf8_encode($row['tipo_servicio']);
				$mi_menu->fecha_desde = $row['fecha_desde']->format('d-m-Y');
				$mi_menu->fecha_hasta = $row['fecha_hasta']->format('d-m-Y');
				$mi_menu->txt_desc = $row['txt_desc'];
				$mi_menu->id_estado = $row['id_estado'];
				$mi_menu->id_catering = $datosUser->id_catering;
				$mi_menu->clientes = Self::getClientesMenu($mi_menu->id_menu, $datosUser->id_catering);

				$menues[$i] = $mi_menu;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $menues );
		}
	}

	public function getMenuesVigentes2()
	{
		$datosUser = Cookies::getDatosUser();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_menues_vigentes @id_usuario = ?";
		$params = array( $datosUser->id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5003', $sql, $params);
			return Helpers::returnError(0, 'Error 5003');
		}else{			
			$i = 0;
			$menues = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_menu = new Menues();

				$mi_menu->id_menu = $row['id_menu'];
				$mi_menu->id_tipo_servicio = $row['id_tipo_servicio'];
				$mi_menu->tipo_servicio = utf8_encode($row['tipo_servicio']);
				$mi_menu->fecha_desde = $row['fecha_desde']->format('d/m');
				$mi_menu->fecha_hasta = $row['fecha_hasta']->format('d/m');
				$mi_menu->txt_desc = $row['txt_desc'];
				$mi_menu->id_estado = $row['id_estado'];
				$mi_menu->id_catering = $datosUser->id_catering;

				$menues[$i] = $mi_menu;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $menues;
		}
	}

	public function insMenu($post)
	{
		$tipo_servicio = $_POST['tipo_servicio'];
		$fecha_desde = $_POST['fecha_desde'];
		$fecha_hasta = $_POST['fecha_hasta'];
		$clientes = json_decode($_POST['clientes']);
		$txt_desc = $_POST['txt_desc'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_menu @id_tipo_servicio = ?, @id_usuario = ?, @txt_desc = ?, @fecha_desde = ?, @fecha_hasta = ?";
		$params = array( $tipo_servicio, $id_usuario, $txt_desc ,$fecha_desde ,$fecha_hasta );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5001', $sql, $params);
			return Helpers::returnError(0, 'Error 5101');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_menu = new Menues();

			$mi_menu->mensaje      = utf8_encode($row['mensaje']);
			$mi_menu->cod_mensaje  = $row['cod_mensaje'];

			if($mi_menu->cod_mensaje == -1)
			{
				$mi_menu->id_menu = $row['id_menu'];
				$mi_menu->id_catering = $row['id_catering'];
				foreach ($clientes as $key => $value) {
					Self::insClientesMenu($mi_menu->id_menu, $mi_menu->id_catering, $value);
				}
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $mi_menu );
		}
	}

	function insClientesMenu($id_menu, $id_catering, $cliente)
	{

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_cliente_a_menu @id_menu = ?, @id_catering = ?, @id_cliente = ?";
		$params = array( $id_menu, $id_catering, $cliente );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5102', $sql, $params);
			return Helpers::returnError(0, 'Error 5102');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_menu = new Menues();

			$mi_menu->mensaje      = utf8_encode($row['mensaje']);
			$mi_menu->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $mi_menu;
		}
	}

	function publicarMenu($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_menu = $post['id_menu'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_publicar_menu @id_menu = ?, @id_usuario = ?";
		$params = array( $id_menu, $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 5103', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 5103');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$mi_menu = new Menues();

			$mi_menu->mensaje      = utf8_encode($row['mensaje']);
			$mi_menu->cod_mensaje  = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $mi_menu );
		}
	}
}
?>