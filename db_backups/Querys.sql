CREATE TABLE [dbo].[visibilidad_menu_por_usuario](
	[id_usuario] [int] NOT NULL,
	[id_menu] [int] NOT NULL,
	[id_catering] [int] NOT NULL,
	[sn_activo] [int] NOT NULL,
 CONSTRAINT [PK_visibilidad_menu_por_usuario] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC,
	[id_menu] ASC,
	[id_catering] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[visibilidad_menu_por_usuario]  WITH CHECK ADD  CONSTRAINT [FK_visibilidad_menu_por_usuario_menu] FOREIGN KEY([id_menu], [id_catering])
REFERENCES [dbo].[menu] ([id_menu], [id_catering])
GO

ALTER TABLE [dbo].[visibilidad_menu_por_usuario] CHECK CONSTRAINT [FK_visibilidad_menu_por_usuario_menu]
GO

ALTER TABLE [dbo].[visibilidad_menu_por_usuario]  WITH CHECK ADD  CONSTRAINT [FK_visibilidad_menu_por_usuario_users] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[users] ([cod_usuario])
GO

ALTER TABLE [dbo].[visibilidad_menu_por_usuario] CHECK CONSTRAINT [FK_visibilidad_menu_por_usuario_users]
GO


ALTER PROC [dbo].[sp_ins_menu]
(
	 @id_tipo_servicio INT
	,@id_usuario INT
	,@txt_desc VARCHAR(MAX)
	,@fecha_desde SMALLDATETIME = null
	,@fecha_hasta SMALLDATETIME = null
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_menu INT
	       ,@id_catering INT
		   ,@cod_tipo_usuario INT

	SELECT @id_catering = id_catering
		 , @cod_tipo_usuario = cod_tipo_usuario 
		FROM users WHERE cod_usuario = @id_usuario

	IF @cod_tipo_usuario = 1 BEGIN
		SELECT @id_menu = ISNULL(MAX(id_menu), 0) + 1 FROM menu WHERE id_catering = @id_catering

		INSERT INTO  menu (id_menu, id_catering, id_tipo_servicio, id_usuario, txt_desc, fecha_desde, fecha_hasta)
					VALUES(@id_menu, @id_catering, @id_tipo_servicio, @id_usuario, @txt_desc, @fecha_desde, @fecha_hasta)
		
		SELECT -1 'cod_mensaje'
		      ,'Menú creado correctamente' 'mensaje'
			  ,@id_menu 'id_menu'
			  ,@id_catering 'id_catering'
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
		      ,'El usuario no tiene permisos para crear un nuevo menú' 'mensaje'
	END
END

GO

CREATE PROC sp_ins_cliente_a_menu
(
	@id_menu INT
	,@id_catering INT
	,@id_cliente INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM visibilidad_menu_por_usuario WHERE id_usuario = @id_cliente AND id_menu = @id_menu AND id_catering = @id_catering)  BEGIN
		SELECT 0 'cod_mensaje'
			  ,'Repetido' 'mensaje'
	END ELSE BEGIN
		IF @id_cliente = 0 BEGIN
			DECLARE @mi_cliente INT
			DECLARE @max INT

			SELECT @max = MAX(cod_usuario) FROM users WHERE id_catering = @id_catering AND cod_tipo_usuario = 2
			SELECT @mi_cliente = MIN(cod_usuario) FROM users WHERE id_catering = @id_catering AND cod_tipo_usuario = 2

			WHILE (@mi_cliente <= @max AND @mi_cliente IS NOT NULL)
			BEGIN
				INSERT INTO visibilidad_menu_por_usuario (id_usuario, id_menu, id_catering, sn_activo)
										           VALUES(@mi_cliente, @id_menu, @id_catering, -1)
				
				IF EXISTS(SELECT MIN(cod_usuario) FROM users WHERE id_catering = @id_catering AND cod_tipo_usuario = 2 AND cod_usuario > @mi_cliente) BEGIN
					SELECT @mi_cliente = MIN(cod_usuario) FROM users WHERE id_catering = @id_catering AND cod_tipo_usuario = 2 AND cod_usuario > @mi_cliente
				END ELSE BEGIN
					SELECT @mi_cliente = null
				END
				
			END

			SELECT -1 'cod_mensaje'
				  ,'Todos' 'mensaje'
		END ELSE BEGIN
			INSERT INTO visibilidad_menu_por_usuario (id_usuario, id_menu, id_catering, sn_activo)
										   VALUES(@id_cliente, @id_menu, @id_catering, -1)
			SELECT -1 'cod_mensaje'
				  ,'ok' 'mensaje'
		END
	END
END

GO
ALTER PROC [dbo].[sp_get_clientes_by_usuario_catering]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_catering INT

	SELECT @id_catering = id_catering
		FROM users WHERE cod_usuario = @id_usuario

	SELECT cod_usuario 'id_usuario'
	      ,cod_tipo_usuario 'id_tipo_usuario'
		  ,txt_user 'usuario'
		  ,txt_nombre 'nombre'
		  ,txt_apellido 'apellido'
		  ,txt_email 'email'
		  ,txt_direccion 'direccion'
	FROM users
	WHERE id_catering = @id_catering
	  AND cod_tipo_usuario = 2
END

GO
ALTER PROC [dbo].[sp_get_login]
(
	@email VARCHAR(1000)
	,@pass VARCHAR(1000)
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @cod_usuario INT
	SELECT @cod_usuario = cod_usuario FROM users WHERE (txt_email = @email OR txt_user = @email) AND txt_password = @pass

	IF @cod_usuario IS NOT NULL BEGIN
		SELECT   'Login correcto' 'mensaje'
				,-1 'cod_mensaje'
				,cod_usuario 'id_usuario'
				,cod_tipo_usuario 'tipo_usuario'
				,txt_user 'usuario'
				,txt_nombre 'nombre'
				,txt_apellido 'apellido'
				,id_catering 'id_catering'
			FROM users
			WHERE cod_usuario = @cod_usuario
	END ELSE BEGIN
		IF EXISTS(SELECT 1 FROM users WHERE txt_email = @email OR txt_user = @email) BEGIN
			SELECT 0 'cod_mensaje'
				  ,'Contraseña incorrecta' 'mensaje'
		END ELSE BEGIN
			SELECT 0 'cod_mensaje'
				  ,'Usuario/email incorrecto' 'mensaje'
		END
	END
END

GO

ALTER table menu ADD id_estado INT FOREIGN KEY (id_estado) REFERENCES estado_menu(id_estado)

GO

GO
ALTER PROC [dbo].[sp_ins_menu]
(
	 @id_tipo_servicio INT
	,@id_usuario INT
	,@txt_desc VARCHAR(MAX)
	,@fecha_desde SMALLDATETIME = null
	,@fecha_hasta SMALLDATETIME = null
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_menu INT
	       ,@id_catering INT
		   ,@cod_tipo_usuario INT

	SELECT @id_catering = id_catering
		 , @cod_tipo_usuario = cod_tipo_usuario 
		FROM users WHERE cod_usuario = @id_usuario

	IF @cod_tipo_usuario = 1 BEGIN
		SELECT @id_menu = ISNULL(MAX(id_menu), 0) + 1 FROM menu WHERE id_catering = @id_catering

		INSERT INTO  menu (id_menu, id_catering, id_tipo_servicio, id_usuario, txt_desc, fecha_desde, fecha_hasta, id_estado)
					VALUES(@id_menu, @id_catering, @id_tipo_servicio, @id_usuario, @txt_desc, @fecha_desde, @fecha_hasta, 2)
		
		SELECT -1 'cod_mensaje'
		      ,'Menú creado correctamente' 'mensaje'
			  ,@id_menu 'id_menu'
			  ,@id_catering 'id_catering'
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
		      ,'El usuario no tiene permisos para crear un nuevo menú' 'mensaje'
	END
END

GO

CREATE PROC sp_get_all_menues
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_catering INT
	
	SELECT @id_catering = id_catering FROM users WHERE cod_usuario = @id_usuario

	SELECT m.id_menu 'id_menu'
	      ,m.id_tipo_servicio 'id_tipo_servicio'
		  ,s.txt_desc 'tipo_servicio'
		  ,m.txt_desc 'txt_desc'
		  ,m.fecha_desde 'fecha_desde'
		  ,m.fecha_hasta 'fecha_hasta'
		  ,m.id_estado 'id_estado'		  
	FROM menu m
	JOIN tipo_servicio s ON s.id_tipo_servicio = m.id_tipo_servicio
	WHERE id_catering = @id_catering
END

GO

CREATE PROC sp_get_clientes_menu
(
	 @id_menu INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT v.id_usuario 'id_usuario'
	      ,u.txt_user 'usuario'
		  ,u.txt_nombre 'nombre'
		  ,u.txt_apellido 'apellido'
		  ,u.txt_email 'email'
	FROM visibilidad_menu_por_usuario v
	JOIN users u ON u.cod_usuario = v.id_usuario
	WHERE   v.id_menu = @id_menu
		AND v.id_catering = @id_catering
		AND v.sn_activo = -1
END

GO

CREATE TABLE [dbo].[tdias](
	[id_dia] [int] NOT NULL,
	[txt_desc] [varchar](200) NOT NULL,
 CONSTRAINT [PK_tdias] PRIMARY KEY CLUSTERED 
(
	[id_dia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

insert into tdias values(1, 'Lunes')
insert into tdias values(2, 'Martes')
insert into tdias values(3, 'Miércoles')
insert into tdias values(4, 'Jueves')
insert into tdias values(5, 'Viernes')
insert into tdias values(6, 'Sábado')
insert into tdias values(7, 'Domingo')

GO

CREATE PROC sp_get_platos
(
	 @id_menu INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT id_plato 'id_plato'
		  ,id_menu 'id_menu'
	      ,id_catering 'id_catering'
		  ,fecha 'fecha'
		  ,txt_plato 'plato'
		  ,txt_desc 'txt_desc'
		  ,calorias 'calorias'
		  ,precio 'precio'
		  ,stock 'stock'
	FROM platos
	WHERE id_menu = @id_menu
	  AND id_catering = @id_catering
END

GO

ALTER TABLE platos ADD stock int

GO

ALTER TABLE catering ADD url_imagen VARCHAR(1000)
ALTER TABLE catering ADD txt_about VARCHAR(MAX)
ALTER TABLE catering ADD pedidos_minimos INT

GO

