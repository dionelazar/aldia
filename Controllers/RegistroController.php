<?php 
/**
* 
*/
require_once 'Controller.php';
require_once __DIR__.'/../class.users.php';
require_once __DIR__.'/../class.catering.php';

class RegistroController extends Controllers
{
	public function registroCatering()
	{
		if (isset($_GET['hash'])) {
			$cod_activacion = $_GET['hash'];
			$verificacion = Users::comprobarCodigoActivacion($cod_activacion);
			if($verificacion->cod_mensaje != -1)
				header('location:login');
		}else{
		    header('location:login');
		    die();
		}
	}

	public function registroCliente()
	{
		if (isset($_GET['id_catering'])) {
			$id_catering = $_GET['id_catering'];
			$datosCatering = Catering::getDatosCatering($id_catering);
			if($datosCatering->cod_mensaje != -1)
			{
				echo $datosCatering->mensaje;
				die();
			}else{
				return $datosCatering;
			}	
		}else{
		    header('location:login');
		    die();
		}
	}
}
?>