<?php
  require_once '../../Controllers/CateringController.php';
  $controller = new CateringController();
  $titulo = 'Aldia | Menú';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <li>
                <a href="menucatering">Menú</a>
              </li>
              <li><a href="#0" class="active">Nuevo Menú</a>
              </li>
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <div class="panel-body">
                <form id="form-personal" role="form" autocomplete="off">
                  <div class="row">
                   <div class="col-sm-12">
                     <div class="form-group form-group-default form-group-default-select2 required">
                        <label class="">Tipo de servicio</label>
                        <select id="catering-type" class="full-width" data-placeholder="Selecciona un tipo de catering" data-init-plugin="select2">
                        </select>
                      </div>
                   </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Menú disponible desde</label>
                        <input type="text" class="form-control" placeholder="Elije una fecha" id="datepicker-desde">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Menú disponible hasta</label>
                        <input type="text" class="form-control" placeholder="Elije una fecha" id="datepicker-hasta">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                   <div class="col-sm-12">
                     <div class="form-group form-group-default form-group-default-select2 required">
                        <label class="">Visible para</label>
                        <select id="multi-client" class="full-width" data-placeholder="Elige quienes verán tu menú" data-init-plugin="select2" multiple>
                          
                        </select>
                      </div>
                   </div>
                  </div>

                  <div class="form-group form-group-default">
                    <label>Descripción</label>
                    <textarea class="form-control textarea-control" id="descripcion"></textarea>
                  </div>
                  
                  <div class="clearfix"></div>
                  <a href="javascript:void(0)" onclick="crearMenu()" class="btn btn-primary">Continuar</a>
                </form>
              </div>
            </div> <!-- Termina panel de clientes -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>

    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/menu-welcome-formulario.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/menu-formulario.js" type="text/javascript"></script>
  </body>
</html>