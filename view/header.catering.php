<?php 
  require_once __DIR__.'/../Controllers/CateringController.php';
  $controller = new CateringController();
  $catering = $controller->getDatosCatering();
  Cookies::bloquearPaginaCatering();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $titulo; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#c10a28">
    <meta name="theme-color" content="#c10a28" />
    <meta content="" name="description" />
    <meta content="Amedia" name="author" />
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="pages/css/style.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
	   <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	  <![endif]-->
  </head>
  <body class="fixed-header menu-pin">

    <nav class="page-sidebar" data-pages="sidebar">
      <div class="sidebar-header">
        <!-- <img src="assets/img/logo_white.png" alt="logo" class="brand" data-src="assets/img/logo_white.png" data-src-retina="assets/img/logo_white_2x.png" width="78" height="22"> -->
      </div>
      
      <div class="profile-container">
        <div class="profile-wrapper center-block">
          <div class="img-wrapper">
            <img class="img-responsive" src="<?php echo $catering->imagen; ?>" alt="<?php echo $catering->nombre; ?>">
          </div>  
          <a class="btn-profile" data-toggle="tooltip" data-original-title="Mi perfíl" href="perfil"><i class="icon" data-icon="&#xe056;"></i></a>
        </div>
        
        <h4 class="text-white font-montserrat text-center"><?php echo $catering->nombre; ?></h4>
      </div>

      <div class="sidebar-menu">
        <ul class="menu-items">
          <li class="m-t-10" id="catering">
            <a href="catering">
              <span class="title"><i class="icon" data-icon="&#xe006;"></i> Inicio</span>
            </a>
          </li>
          <li class="m-t-10" id="menucatering">
            <a href="menucatering">
              <span class="title"><i class="icon" data-icon='"'></i> Menú</span>
            </a>
          </li>
          <li class="m-t-10" id="pedidos">
            <a href="pedidos">
              <span class="title"><i class="icon" data-icon="&#xe034;"></i> Pedidos</span>
            </a>
          </li>
          <li class="m-t-10" id="misclientes"> <!-- la clase active indica que esta en esa seccion de la app -->
            <a href="misclientes">
              <span class="title"><i class="icon" data-icon="&#xe057;"></i> Clientes</span>
            </a>
          </li>
          <li class="m-t-10" id="reportes">
            <a href="reportes">
              <span class="title"><i class="icon" data-icon="&#xe001;"></i> Reportes</span>
            </a>
          </li>
          <li class="m-t-10">
            <a href="micuenta">
              <span class="title"><i class="icon icon-user"></i> Mi perfil</span>
            </a>
          </li>
          
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="more-nav">
        <div class="col-md-12 text-center">
            <i class="icon icon-exit"></i>
            <a href="javascript:void(0)" onclick="logout()">Cerrar Sesión</a>
        </div>
      </div>
    </nav>

    <div class="page-container">
      <div class="header">
        <div class="container-fluid relative">
          <div class="pull-left full-height visible-sm visible-xs">
            <div class="header-inner">
            </div>
            <!-- Termina ACTION BAR -->
          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline">
               <h4 class="text-white">Clientes</h4>
              </div>
            </div>
          </div>
          <div class="pull-right full-height visible-sm visible-xs">
            <div class="header-inner dropdown">
              <button class="mobile-dropdown-toggle rotate-90" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="icon menu-icon" data-icon="/"></i>  
              </button>
              <ul class="dropdown-menu mobile-dropdown" role="menu">
                <li><a href="perfil">Perfil</a>
                </li>
                <li><a href="#">Cuenta</a>
                </li>
                <li><a href="#">Ayuda</a>
                </li>
              </ul>
            </div>
            <!-- Termina ACTION BAR -->
          </div>
        </div>

        <div class="container-mobile-nav container-fluid relative">
          <div class="hidden-md hidden-lg">
              <ul class="mobile-nav">
                <li>
                  <a href="catering">
                    <i class="icon" data-icon="&#xe006;"></i>
                  </a>
                </li>
                <li>
                  <a href="menucatering">
                    <i class="icon" data-icon='"'></i>
                  </a>
                </li>
                <li>
                  <a href="pedidos">
                    <i class="icon" data-icon="&#xe034;"></i>
                  </a>
                </li>
                <li> <!-- la clase active indica que esta en esa seccion de la app -->
                  <a href="misclientes">
                    <i class="icon" data-icon="&#xe057;"></i>
                  </a>
                </li>
                <li>
                  <a href="reportes">
                    <i class="icon" data-icon="&#xe001;"></i>
                  </a>
                </li>
              </ul>
          </div>
        </div>
        <!-- Termina MOBILE CONTROLS -->

        <?php require_once __DIR__.'/notificaciones.php'; ?>