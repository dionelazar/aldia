<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';

class Direccion extends Conection
{
	public $calle;
	public $entre_calles;
	public $altura;
	public $piso;
	public $oficina;
	public $cod_postal;
	public $id_provincia;
	public $id_municipio;
	public $cod_mensaje;
	public $mensaje;
	
	public function getDireccion($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_direccion_usuario @id_usuario = ?";
		$params = array( $id_usuario );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 20001', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 20001');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$direccion = new Direccion();

			$direccion->calle = utf8_encode($row['calle']);
			$direccion->entre_calles = utf8_encode($row['entre_calles']);
			$direccion->altura = $row['altura'];
			$direccion->piso = $row['piso'];
			$direccion->oficina = utf8_encode($row['oficina']);
			$direccion->cod_postal = $row['cod_postal'];
			$direccion->id_provincia = $row['id_provincia'];
			$direccion->id_municipio = $row['id_municipio'];

			Conection::CerrarConexion($conn);
			echo json_encode( $direccion );
		}
	}

	public function insDireccion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$calle = utf8_decode($post['calle']);
		$entre_calles = utf8_decode($post['entre_calles']);
		$altura = $post['altura'];
		$piso = $post['piso'];
		$oficina = $post['oficina'];
		$cod_postal = $post['cod_postal'];
		$provincias = $post['provincias'];
		$municipios = $post['municipios'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_detalle_direccion @id_usuario = ?, @txt_calle = ?, @txt_entre_calles = ?, @altura = ?, @piso = ?, @oficina = ?, @cod_postal = ?, @id_dpto = ?, @id_municipio = ?";
		$params = array( $id_usuario, $calle, $entre_calles, $altura, $piso, $oficina, $cod_postal, $provincias, $municipios );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 21001', $sql, $params);
			return Helpers::returnError(0, 'Error 21001');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

			$mi_direccion = new Direccion();
			$mi_direccion->cod_mensaje = $row['cod_mensaje'];
			$mi_direccion->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $mi_direccion );
		}
	}
}
 ?>