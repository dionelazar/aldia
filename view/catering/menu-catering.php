<?php 
  $titulo = 'Aldia | Menú';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <ul class="nav nav-tabs nav-tabs-simple nav-tabs-menu" role="tablist">
                <li class="active">
                  <a href="#menuesVigentes" data-toggle="tab" role="tab">Vigentes</a>
                </li>
                <li>
                  <a href="#menuesGuardados" data-toggle="tab" role="tab">Guardados</a>
                </li>
                <li>
                  <a href="#menuesFinalizados" data-toggle="tab" role="tab">Finalizados</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="menuesVigentes">
                  <div class="row">
                    <div class="col-md-3">
                      <a href="nuevomenu" class="menu-card menu-add-plato text-center">
                        <div class="menu-card-content">
                          <i class="icon" data-icon="&#xe035;"></i>
                          <p>Agrega un nuevo menú</p>
                        </div>
                      </a>
                    </div>
                    <div id="bodyVigentes">
                      
                    </div>
                    
                </div> <!-- Finaliza pestaña #menuesVigentes -->

                <div class="tab-pane" id="menuesFinalizados">
                  <div class="row">
                  <div id="bodyFinalizados"></div>
                    
                  </div>
                </div><!-- Finaliza pestaña #menuesVigentes -->
              </div><!-- Finaliza pestaña tab.content -->
            </div> <!-- Termina panel de MENUES -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-collapse/bootstrap-tabcollapse.js" type="text/javascript"></script>

    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/menu-catering.js" type="text/javascript"></script>
  </body>
</html>