$(document).ready(function() {
    $("#condicionIva").select2({
        minimumResultsForSearch: -1
    });

    var msgContact = "¡Genial! Se modificó tu información de contacto con exito";
    var msgPass = "No logramos cambiar tu contraseña. Intenta nuevamente";
    var msgFacturacion = "¡Genial! Se modificó tu información de facturación con exito";
    var position = "top";
    var styleBar = "bar";
    var typeBar = "success";

    $('#infoContact').click(function(){
    	$('body').pgNotification({
    		style: styleBar,
    		message: msgContact,
    		position: position,
    		timeout: 5000,
    		type: typeBar
    	}).show();	
    })

    $('#newPass').click(function(){
    	$('body').pgNotification({
    		style: styleBar,
    		message: msgPass,
    		position: position,
    		timeout: 5000,
    		type: "warning"
    	}).show();	
    })

    $('#infoFacturacion').click(function(){
    	$('body').pgNotification({
    		style: styleBar,
    		message: msgFacturacion,
    		position: position,
    		timeout: 5000,
    		type: typeBar
    	}).show();	
    })
});