$(document).ready(function(){
    getClientes();
    getTipoServicio();
});

function getClientes()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'catering'
            ,call  : 'getClientes'
        },
        success : function(Data){
            var miData = JSON.parse(Data);            
            armarClientes(miData);
        }
    });
}

function getTipoServicio()
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'catering'
            ,call  : 'getTipoServicio'
        },
        success : function(Data){
            var miData = JSON.parse(Data);            
            armarTipoServicio(miData);
        }
    });
}

function armarClientes(Data)
{
    html = '';
    html += '<optgroup label="Clientes">';
        html += '<option value="0">Todos</option>';
    for (var i = 0; i < Data.length; i++) 
    {
        html += '<option value="'+Data[i].id_usuario+'">'+Data[i].nombre+' '+Data[i].apellido+'</option>';
    }
    html += '</optgroup>';

    $('#multi-client').append(html);
}

function armarTipoServicio(Data)
{
    html = '';
    for (var i = 0; i < Data.length; i++) 
    {
        html += '<option value="'+Data[i].id_tipo_servicio+'">'+Data[i].servicio+'</option>';
    }

    $('#catering-type').append(html);
}

function crearMenu()
{
    if(!validarForm()) return false;

    var tipo_servicio = $('#catering-type').val();
    var fecha_desde = formatearFecha( $('#datepicker-desde').val() );
    var fecha_hasta = formatearFecha( $('#datepicker-hasta').val() );
    var clientes = JSON.stringify($('#multi-client').val());
    var txt_desc = $('#descripcion').val();

    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'menues'
            ,call  : 'insMenu'
            ,tipo_servicio :tipo_servicio
            ,fecha_desde : fecha_desde
            ,fecha_hasta : fecha_hasta
            ,clientes : clientes
            ,txt_desc : txt_desc
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
                window.location.replace('menu&'+miData.id_menu+'&'+miData.id_catering);
            else
                alert(miData.mensaje);
        }
    });
}

function validarForm()
{
    var tipo_servicio = $('#catering-type').val();
    var fecha_desde = formatearFecha( $('#datepicker-desde').val() );
    var fecha_hasta = formatearFecha( $('#datepicker-hasta').val() );
    var clientes = JSON.stringify($('#multi-client').val());
    var txt_desc = $('#descripcion').val();

    if(tipo_servicio < 1)
        return false;

    if(fecha_desde < 1)
        return false;

    if(fecha_hasta < 1)
        return false;

    if(clientes < 1)
        return false;

    if(txt_desc < 1)
        return false;

    return true;
}

function formatearFecha(fecha)
{
    if(fecha.length > 1)
    {
        var fechaSplit = fecha.split("/");//dia, mes, año
        
        var fechaFormateada = fechaSplit[2] + fechaSplit[1] + fechaSplit[0];//año, mes, dia
        return fechaFormateada;
    }else{
        return null;
    }
}