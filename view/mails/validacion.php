<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Aldia mail</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			* {
				-ms-text-size-adjust:100%;
				-webkit-text-size-adjust:none;
				-webkit-text-resize:100%;
				text-resize:100%;
			}
			a{
				outline:none;
				color:#fda50c;
				text-decoration:none;
			}
			a:hover{text-decoration:underline !important;}
			a[x-apple-data-detectors]{
				color:inherit !important;
				text-decoration:none !important;
			}
			.link a{
				color:#fff;
				text-decoration: underline;
			}
			.link a:hover{text-decoration:none !important;}
			.active:hover{opacity:0.8;}
			.active{
				-webkit-transition:all 0.3s ease;
				   -moz-transition:all 0.3s ease;
					-ms-transition:all 0.3s ease;
						transition:all 0.3s ease;
			}
			.footer a{color: #fff;}
			table td{border-collapse:collapse !important; mso-line-height-rule:exactly;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
			@media only screen and (max-width:500px) {
				table[class="flexible"]{width:100% !important;}
				*[class="hide"]{
					display:none !important;
					width:0 !important;
					height:0 !important;
					padding:0 !important;
					font-size:0 !important;
					line-height:0 !important;
				}
				td[class="img-flex"] img{width:100% !important; height:auto !important;}
				td[class="aligncenter"]{text-align:center !important;}
				th[class="flex"]{display:block !important; width:100% !important;}
				tr[class="table-holder"]{display:table !important; width:100% !important;}
				th[class="thead"]{display:table-header-group !important; width:100% !important;}
				th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
				td[class="wrapper"]{padding:5px !important;}
				td[class="header"]{padding:30px 10px !important;}
				td[class="header"] *{text-align:center !important;}
				td[class="title"]{
					padding:0 0 30px !important;
					font-size:30px !important;
					line-height:36px !important;
				}
				td[class="frame"]{padding:20px 10px !important;}
				td[class="holder"]{padding:20px 20px 30px !important;}
				td[class="holder"] *{text-align:center !important;}
				td[class="block"]{padding:0 10px 10px !important;}
				td[class="box"]{padding:0 20px 20px !important;}
				td[class="nav"]{line-height:30px !important;}
				td[class="p-30"]{padding:0 0 30px !important;}
				td[class="p-20"]{padding:0 0 20px !important;}
				td[class="p-10"]{padding:0 0 10px !important;}
				td[class="block-10"]{padding:0 10px 20px !important;}
				td[class="indent-null"]{padding:0 !important;}
				td[class="indent"]{padding:0 10px !important;}
				td[class="w-20"]{width:20px !important;}
				td[class="h-auto"]{height:auto !important;}
				td[class="footer"]{padding:20px 10px !important;}
			}
		</style>
	</head>
	<body style="margin:0; padding:0;">
		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
			<tr>
				<td class="hide">
					<table width="700" cellpadding="0" cellspacing="0" style="width:700px !important;">
						<tr>
							<td style="min-width:700px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table data-module="pre-header" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#CF080F" style="padding:0 10px;">
								<table class="flexible" width="700" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td data-color="text-pre-header" data-size="size text-pre-header" data-min="10" data-max="24" data-link-color="link text-pre-header color" data-link-style="text-decoration:none; color:#fff;" align="center" style="padding:9px 10px 11px; font:14px/19px Arial, Helvetica, sans-serif; color:#6b0004;">¿No podés visualizar correctamente el email? <a href="sr_view_online" style="text-decoration:underline; color:#fff;">Click aquí</a></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table data-module="module-2" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#e8e8e8" style="padding: 50px 10px 0px;">
								<table class="flexible" width="700" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="frame" bgcolor="#ffffff" style="padding:80px 40px 20px;">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:underline; color:#000;" align="center" style="padding:0 0 15px; font:bold 24px/26px Arial, Helvetica, sans-serif; color:#565656;">
														<img src="http://aldia.com.ar/views/mails/images/icon-invitation.png" alt="" />
													</td>
												</tr>
												<tr>
													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:underline; color:#000;" align="center" style="padding:0 0 15px; font:bold 24px/26px Arial, Helvetica, sans-serif; color:#565656;">{NombreCatering} te invita a unirte a Aldia</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table data-module="module-3" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" class="block-10" bgcolor="#e8e8e8" style="padding:0 10px 20px;">
								<table class="flexible" width="700" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td data-bgcolor="bg-block" class="frame" bgcolor="#ffffff" style="border-radius:0 0 3px 3px; padding:0px 40px 40px;">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																<td data-bgcolor="bg-content" class="holder" style="padding:0px 39px 40px; border-radius:3px;">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr class="link" align="center	">
																			<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="text-decoration:underline; color:#fff;" style="padding:0 0 23px; font:14px/25px Arial, Helvetica, sans-serif; color:#565656;">
																				Hola! <b style="color:#CF080F;">{NombreCatering}</b> te ha invitado a registrarte en <b>Aldía</b>, la plataforma para hacer tus pedidos de viandas online. <br /> ¡Ingresá y empezá a simplificar tu trabajo!
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
																					<tr>
																						<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="26" class="active" bgcolor="#CF080F" align="center" style="mso-padding-alt:13px 20px 11px; border-radius:3px; font:bold 15px/17px Arial, Helvetica, sans-serif; color:#ffffff; text-transform:uppercase;"><a href="{link}" style="text-decoration:none !important; color:#fff !important; display:block; padding:13px 20px 11px;">Registrarme en Aldia</a></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#e8e8e8" style="padding:0 10px;">
								<table class="flexible" width="700" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="header" style="padding:0px 0 37px;">
											<table class="flexible" width="700" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
												<tr>
													<th class="flex" align="center" style="vertical-align:top; padding:0;">
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																<td align="right"><img src="http://aldia.com.ar/views/mails/images/logo.png" height="29" width="70" border="0" style="vertical-align:top;" alt="Amedia" /></td>
															</tr>
														</table>
													</th>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>