<?php 
    $title = 'Aldia | Mis Pedidos';
    require_once '../header.cliente.php';
    require_once __DIR__.'/../../Controllers/ClienteController.php';
    $clienteController = new ClienteController();
    $usuario = $clienteController->getDatosUser();
    $catering = $clienteController->getDatosCatering();
?>

    <section id="videoWelcome" class="">
        <h1 class="text-center">Estos son tus pedidos<br>hechos a <?php echo $catering->nombre; ?></h1>
    </section>

    <section id="pedidos" class="aldia-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <ul class="nav nav-tabs nav-day">
                          <li class="active"><a data-toggle="tab" href="#pedidosPendientes">Pendientes</a></li>
                          <li><a data-toggle="tab" href="#calendario">Calendario de entregas</a></li>
                          <li><a data-toggle="tab" href="#historial">Historial</a></li>
                        </ul>

                        <div class="tab-content pad-15">
                            <div id="pedidosPendientes" class="tab-pane fade in active">
                                <div class="col-md-12" id="listaPedidosPendientes">
                                    <div class="pedidos-card">
                                        <ul class="pedidos-list">
                                            <li><i class="icon-calendar"></i> 1/5</li>
                                            <li><i class="icon-cutlery"></i> Menú del 2-5 al 5/5</li>
                                            <li><i class="icon-wallet"></i> <span class="pedido-price">250</span></li>
                                        </ul>

                                        <div class="pendiente-label pull-right">
                                            <i class="icon-time-reverse"></i> <span>Pendiente de confirmación</span>
                                        </div>

                                        <div class="timeline-pedidos-hidden">
                                            <h4>Detalles del pedido</h4>
                                            <ul class="timeline-checkout">
                                              <li class="day-plato">
                                                <h4>lunes 2</h4>
                                                <p class="plato-checkout">Nombre de plato x 1</p>
                                              </li>

                                              <li class="day-plato">
                                                <h4>martes 3</h4>
                                                <p class="plato-checkout">Nombre de plato x 1</p>
                                              </li>

                                              <li class="day-plato">
                                                <h4>miércoles 4</h4>
                                                <p class="plato-checkout">Nombre de plato x 1</p>
                                              </li>

                                              <li class="day-plato">
                                                <h4>jueves 5</h4>
                                                <p class="plato-checkout">Nombre de plato x 1</p>
                                              </li>

                                              <li class="day-plato">
                                                <h4>viernes 6</h4>
                                                <p class="plato-checkout">Nombre de plato x 1</p>
                                              </li>
                                            </ul>
                                            
                                            <h4 class="comentarios-title">Comentarios enviados</h4>
                                            <p>La quiero con mayonesa, sin ketchup y con mostaza, dejale el tomate, pero sacale la lechuga y la carne, dejale el orégano pero reemplazame la cebolla por la gaseosa y agrandame las papas y cambiámela por un cono de vainilla y si no tenés de vainilla ponele ketchup a la comida y agrandame el menú por 50 Centavos pero no le pongas condimentos al helado porque vengo del cine y ella se llama Valentina</p>
                                        </div>

                                        <a href="#0" class="open-plato-details text-center">
                                            <p class="change-text">Ver detalles del pedido</p>
                                            <i class="icon-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="calendario" class="tab-pane fade in">
                                <ul class="nav nav-pills nav-weeks-pedidos m-b-20">
                                    <li class="active"><a data-toggle="pill" href="#semanaActual">Semana actual</a></li>
                                    <li><a data-toggle="pill" href="#semana2">Semana desde el 2/5 al 5/5</a></li>
                                    <li><a data-toggle="pill" href="#semana3">Semana desde el 2/5 al 5/5</a></li>
                                    <li><a data-toggle="pill" href="#semana4">Semana desde el 2/5 al 5/5</a></li>
                                </ul>

                                <div class="tab-content">

                                    <div id="semanaActual" class="tab-pane fade in active">
                                        <table class="table table-hover" id="tableCalendario">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Lunes 2-5</th>
                                                    <th>Martes 2-5</th>
                                                    <th>Miércoles 2-5</th>
                                                    <th>Jueves 2-5</th>
                                                    <th>Viernes 2-5</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                          <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                              <tr>
                                                <td class="v-align-middle">
                                                    <p>nombre de plato</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                    </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p>15</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p class="price-pedidos">2345</p>
                                                </td>
                                                <tr>
                                                    <td class="v-align-middle">
                                                    <p class="bold font-montserrat">TOTAL</p>
                                                </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 

                                    <div id="semana2" class="tab-pane fade in">
                                        <table class="table table-hover" id="tableCalendario">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Lunes 2-5</th>
                                                    <th>Martes 2-5</th>
                                                    <th>Miércoles 2-5</th>
                                                    <th>Jueves 2-5</th>
                                                    <th>Viernes 2-5</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                          <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                              <tr>
                                                <td class="v-align-middle">
                                                    <p>nombre de plato</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                    </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p>15</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p class="price-pedidos">2345</p>
                                                </td>
                                                <tr>
                                                    <td class="v-align-middle">
                                                    <p class="bold font-montserrat">TOTAL</p>
                                                </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 

                                    <div id="semana3" class="tab-pane fade in">
                                        <table class="table table-hover" id="tableCalendario">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Lunes 2-5</th>
                                                    <th>Martes 2-5</th>
                                                    <th>Miércoles 2-5</th>
                                                    <th>Jueves 2-5</th>
                                                    <th>Viernes 2-5</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                          <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                              <tr>
                                                <td class="v-align-middle">
                                                    <p>nombre de plato</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                    </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p>15</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p class="price-pedidos">2345</p>
                                                </td>
                                                <tr>
                                                    <td class="v-align-middle">
                                                    <p class="bold font-montserrat">TOTAL</p>
                                                </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 

                                    <div id="semana4" class="tab-pane fade in">
                                        <table class="table table-hover" id="tableCalendario">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Lunes 2-5</th>
                                                    <th>Martes 2-5</th>
                                                    <th>Miércoles 2-5</th>
                                                    <th>Jueves 2-5</th>
                                                    <th>Viernes 2-5</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                          <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                        <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="v-align-middle">
                                                      <p>nombre de plato</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                      <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p>15</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p></p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                              <tr>
                                                <td class="v-align-middle">
                                                    <p>nombre de plato</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                    </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p></p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p>15</p>
                                                </td>
                                                <td class="v-align-middle">
                                                    <p class="price-pedidos">2345</p>
                                                </td>
                                                <tr>
                                                    <td class="v-align-middle">
                                                    <p class="bold font-montserrat">TOTAL</p>
                                                </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                    <td class="v-align-middle">
                                                        <p class="price-pedidos">2345</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>    
                                </div>        
                            </div>

                            <div id="historial" class="tab-pane fade in">
                                <table class="table table-hover" id="tableHistorial">
                                    <thead>
                                      <tr>
                                        <th>Fecha</th>
                                        <th>Menú semana</th>
                                        <th>Detalle del pedido</th>
                                        <th>Monto</th>
                                        <th></th>
                                      </tr>
                                    </thead>

                                    <tbody>
                                      <tr>
                                        <td class="v-align-middle">
                                          <p>1/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>2-5 al 6/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>Aca va un boton re laca</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p class="price-pedidos">2345</p>
                                        </td>
                                        <td class="v-align-middle text-right">
                                            <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>  
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="v-align-middle">
                                          <p>1/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>2-5 al 6/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>Aca va un boton re laca</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p class="price-pedidos">2345</p>
                                        </td>
                                        <td class="v-align-middle text-right">
                                            <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>  
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="v-align-middle">
                                          <p>1/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>2-5 al 6/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>Aca va un boton re laca</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p class="price-pedidos">2345</p>
                                        </td>
                                        <td class="v-align-middle text-right">
                                            <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>  
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="v-align-middle">
                                          <p>1/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>2-5 al 6/5</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p>Aca va un boton re laca</p>
                                        </td>
                                        <td class="v-align-middle">
                                          <p class="price-pedidos">2345</p>
                                        </td>
                                        <td class="v-align-middle text-right">
                                            <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>  
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade slide-up disable-scroll" id="modalDetalles" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross fs-14"></i>
              </button>
              <h3>Menú del 2-5 al 5/5</h3>
            </div>
            <div class="modal-body">

              <h4>Detalles del pedido</h4>
              <ul class="timeline-checkout">
                <li class="day-plato">
                  <h4>lunes 2</h4>
                  <p class="plato-checkout">Nombre de plato x 1</p>
                </li>

                <li class="day-plato">
                  <h4>martes 3</h4>
                  <p class="plato-checkout">Nombre de plato x 1</p>
                </li>

                <li class="day-plato">
                  <h4>miércoles 4</h4>
                  <p class="plato-checkout">Nombre de plato x 1</p>
                </li>

                <li class="day-plato">
                  <h4>jueves 5</h4>
                  <p class="plato-checkout">Nombre de plato x 1</p>
                </li>

                <li class="day-plato">
                  <h4>viernes 6</h4>
                  <p class="plato-checkout">Nombre de plato x 1</p>
                </li>
              </ul>

              <p class="pedido-comentario m-t-20">"Este es un comentario que hicieron los usuarios clientes en el pedido Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"</p>

              <button class="btn btn-primary btn-lg btn-block m-t-30" data-dismiss="modal">Entendido</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <a href="index"><img class="center-block" src="img/logo-white.svg" alt="Aldia"></a>
                    </div>

                    <!-- <ul class="social-contact">
                        <li>
                            <img src="img/facebook.svg" alt="facebook">
                        </li>
                        <li>
                            <img src="img/twitter.svg" alt="twitter">
                        </li>
                    </ul> -->

                    <ul class="footer-links">
                        <li>
                            <a href="mispedidos">Mis pedidos</a>
                        </li>
                        <!-- <li>
                            <a href="#0">Invitados</a>
                        </li> -->
                        <li>
                            <a href="configuracion">Configuración</a>
                        </li>
                        <li>
                            <a href="#0">Ayuda</a>
                        </li>
                        <li>
                            <a href="#0">Términos y condiciones</a>
                        </li>
                    </ul>
                </div>    
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mis-pedidos.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/mis-pedidos.js"></script>
</body>
</html>
