<?php /**
* 
*/
require_once 'class.conection.php';
require_once __DIR__.'/assets/plugins/PHPMailer/PHPMailerAutoload.php';
require_once __DIR__.'/class.catering.php';

class Email extends Conection
{
	public $cod_mensaje;
	public $mensaje;
	static $env = 'localhost/aldia/';
	//private $env = 'http://aldia.com.ar/';

	public function invitarCliente($emails)
	{
		$titulo = 'Invitación aldia';
		$datosUser = Cookies::getDatosUser();
		$catering = Catering::getDatosCatering($datosUser->id_catering);
		$link = Self::$env.'registro-cliente&'.$datosUser->id_catering;
		$mensaje = Self::setearTemplateRegistro($catering->nombre, $link);
		$msj = Self::mandar_mail($emails, $titulo, $mensaje);

		$miEmail = new Email();
		$miEmail->mensaje = $msj;
		
		echo json_encode($miEmail);
	}

	public function mandar_mail($para, $titulo, $mensaje)
	{
		//$para = 'dionel.azar@amedia.com.ar';
		if(!is_array($para))
			$destinatarios = explode(',', $para);
		else
			$destinatarios = $para;

		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host = "smtp.gmail.com";
		//$mail->Host = gethostbyname("smtp.gmail.com");
		$mail->Port = 465;//465
		$mail->Username = "no-reply@amedia.com.ar";
		$mail->Password = "sotelo21";
		$mail->SetFrom('no-reply@amedia.com.ar', 'Aldia');
		$mail->AddReplyTo("no-reply@amedia.com.ar","Aldia");
		$mail->Subject = utf8_decode($titulo);

		$mail->MsgHTML($mensaje);//$mail->MsgHTML(utf8_encode($mensaje));

		for ($i=0; $i < count($destinatarios); $i++) 
		{
			$mail->addAddress($destinatarios[$i], $destinatarios[$i]);
			if(!$mail->Send()) {			
				//echo "Error al enviar email: " . $mail->ErrorInfo. "<br>";
				return 'Error al enviar el email';
			} else {
				//echo "Se envió un email!<br>";
				return 'Se envió un email. ';
			}
		}
	}

	public function setearTemplateRegistro( $nombreCatering, $link )
	{
		$message = file_get_contents( __DIR__.'/view/mails/validacion.php' );

		$replacements = array(		    
		    '({NombreCatering})' => $nombreCatering,
		    '({link})' => $link
		    //'({message_body})' =&gt; nl2br( stripslashes( $message_content ) )
		);
		$message = preg_replace( array_keys( $replacements ), array_values( $replacements ), $message );

		return $message;
	}
}

?>