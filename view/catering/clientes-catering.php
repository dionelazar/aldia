<?php 
  require_once '../../Controllers/CateringController.php';
  $controller = new CateringController();
  $titulo = 'Aldia | Clientes';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de clientes -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"><i class="icon" data-icon="&#xe057;"></i> Clientes</div>
                <div class="pull-right xs-pull-reset">
                    <form class="buscar-cliente" id="buscarCliente" action="">
                      <input type="text" id="search-client-table" class="form-control pull-right xs-pull-reset" placeholder="Buscar">  
                      <button><i class="icon" data-icon="&#xe041;"></i></button>
                    </form>
                </div>
              </div>

              <div class="panel-body">
                <button type="button" data-toggle="modal" data-target="#modalAddClient" class="btn btn-block-mobile btn-primary btn-lg pull-right m-b-20">
                  <i class="icon" data-icon="&#xe035;"></i>
                  <span>Invitar clientes</span>
                </button>

                <table class="table table-hover" id="tableClients">
                  <thead>
                    <tr>
                      <th>Empresa</th>
                      <th class="text-center">Contacto</th>
                      <th class="text-right">Dirección de entrega</th>
                    </tr>
                  </thead>
                  <tbody id="bodyClients">
                    
                  </tbody>
                </table>

                
              </div>
            </div> <!-- Termina panel de clientes -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="modalAddClient" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon" data-icon="9"></i>
              </button>
              <h5>Invitar <span class="semi-bold">clientes</span></h5>
            </div>
            <div class="modal-body">
              <div id="formCont" class="form-container">
                <form class="m-t-10" role="form">
                  <div class="form-group form-group-default">
                    <label>Ingresa los correos electrónicos</label>
                    <input id="emailTag" class="tagsinput custom-tag-input" type="text" />
                  </div>
                  <i>Puedes agregar cuantos e-mails quieras. Presiona enter cuando estés preparado para agregar otro correo a la lista</i>
                </form>
                <div class="row">
                  <div class="col-md-4 m-t-10 sm-m-t-10 pull-right">
                    <button id="sendInvitations" type="button" class="btn btn-primary btn-block m-t-5">Enviar invitaciones</button>
                  </div>    
                </div>  
              </div>
              <div id="confirmCont" class="confirmation-container">
                <i class="icon text-center center-block" data-icon="&#xe04e;"></i>
                <h3 class="text-center">Se han enviado las invitaciones</h3>
                <a id="okConfirm" class="btn btn-primary btn-block m-t-30" data-dismiss="modal" href="#0">Continuar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- / Modal -->
    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    
    <script src="assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script src="assets/plugins/stacktable.js"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/clientes.js" type="text/javascript"></script>

    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/clientes-catering.js" type="text/javascript"></script>
  </body>
</html>