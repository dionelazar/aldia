<?php 
/**
* 
*/
require_once 'Controller.php';
require_once __DIR__.'/../class.catering.php';

class CateringController extends Controllers
{
	
	function getDatosCatering()
	{
		$catering = new Catering();
		$datosUser = Cookies::getDatosUser();
		$miCatering = $catering->getDatosCatering($datosUser->id_catering);
		
		return $miCatering;
	}

	public function primeraVez()
	{
		$miCatering = Self::getDatosCatering();
		if(strlen ($miCatering->descripcion) > 0)
			//return $miCatering->descripcion;
			header('location:catering');
		else
			return $miCatering;
	}
}
?>