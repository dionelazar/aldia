<?php 
  $titulo = 'Aldia | Menú';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <ul class="nav nav-tabs nav-tabs-simple" role="tablist">
                <li class="active">
                  <a href="#pedidosPendientes" data-toggle="tab" role="tab">Pendientes</a>
                </li>
                <li>
                  <a href="#calendarioEntregas" data-toggle="tab" role="tab">Calendario de entregas</a>
                </li>
                <li>
                  <a href="#historialPedidos" data-toggle="tab" role="tab">Historial de pedidos</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="pedidosPendientes">                       
                </div> <!-- Finaliza pestaña #pedidosPendientes -->

                <div class="tab-pane" id="calendarioEntregas">

                  <ul class="nav nav-pills nav-weeks-pedidos m-b-20">
                    <li class="active"><a data-toggle="pill" href="#semanaActual">Semana actual</a></li>
                    <li><a data-toggle="pill" href="#semana2">Semana desde el 2/5 al 5/5</a></li>
                    <li><a data-toggle="pill" href="#semana3">Semana desde el 2/5 al 5/5</a></li>
                    <li><a data-toggle="pill" href="#semana4">Semana desde el 2/5 al 5/5</a></li>
                  </ul>

                  <div class="btn-group dropdown-default"> <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> Todos los clientes <span class="caret"></span> </a>
                    <ul class="dropdown-menu ">
                      <li>
                        <a href="#">Cliente 1</a>
                      </li>
                      <li>
                        <a href="#">Cliente 2</a>
                      </li>
                      <li>
                        <a href="#">Cliente 3</a>
                      </li>
                    </ul>
                  </div>
                  
                  <!-- Se visualiza solamente cuando se elije un Cliente -->
                  <p class="m-l-5 m-t-10">Dirección: Olleros 1811</p>
                  <p class="m-l-5">Horario de entrega: 16:20</p>
                  <!-- / Se visualiza solamente cuando se elije un Cliente -->

                  <a href="#0" class="btn btn-default pull-right" >Exportar a Excel</a>

                  <div class="tab-content">
                    <div id="semanaActual" class="tab-pane fade in active">
                      <table class="table table-hover" id="tableCalendario">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Lunes 2-5</th>
                            <th>Martes 2-5</th>
                            <th>Miércoles 2-5</th>
                            <th>Jueves 2-5</th>
                            <th>Viernes 2-5</th>
                            <th>Total</th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <tr>
                            <td class="v-align-middle">
                              <p class="bold font-montserrat">TOTAL</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                    <div id="semana2" class="tab-pane fade">
                      <table class="table table-hover" id="tableCalendario">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Lunes 2-5</th>
                            <th>Martes 2-5</th>
                            <th>Miércoles 2-5</th>
                            <th>Jueves 2-5</th>
                            <th>Viernes 2-5</th>
                            <th>Total</th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <tr>
                            <td class="v-align-middle">
                              <p>Total</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div id="semana3" class="tab-pane fade">
                      <table class="table table-hover" id="tableCalendario">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Lunes 2-5</th>
                            <th>Martes 2-5</th>
                            <th>Miércoles 2-5</th>
                            <th>Jueves 2-5</th>
                            <th>Viernes 2-5</th>
                            <th>Total</th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <tr>
                            <td class="v-align-middle">
                              <p>Total</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div id="semana4" class="tab-pane fade">
                      <table class="table table-hover" id="tableCalendario">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Lunes 2-5</th>
                            <th>Martes 2-5</th>
                            <th>Miércoles 2-5</th>
                            <th>Jueves 2-5</th>
                            <th>Viernes 2-5</th>
                            <th>Total</th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="v-align-middle">
                              <p>nombre de plato</p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p></p>
                            </td>
                            <td class="v-align-middle">
                              <p>15</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <tr>
                            <td class="v-align-middle">
                              <p>Total</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                            <td class="v-align-middle">
                              <p class="price-pedidos">2345</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div><!-- Finaliza pestaña #calendarioEntregas -->

                <div class="tab-pane" id="historialPedidos">
                  <table class="table table-hover" id="tableHistorial">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>Menú semana</th>
                        <th>Monto</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td class="v-align-middle">
                          <p>1/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p><a href="empresa">Nombre de cliente A</a></p>
                        </td>
                        <td class="v-align-middle">
                          <p>2-5 al 6/5</p>
                        </td>
                        <td class="v-align-middle">
                          <p class="price-pedidos">2345</p>
                        </td>
                        <td class="v-align-middle text-right">
                          <button data-toggle="modal" data-target="#modalDetalles" class="btn btn-primary btn-details" ><i data-toggle="tooltip" data-original-title="Detalles del pedido" class="icon-dots-3"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- Finaliza pestaña #historialPedidos -->
              </div><!-- Finaliza pestaña tab.content -->
            </div> <!-- Termina panel de MENUES -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->
    

    <div class="modal fade slide-up disable-scroll" id="modalComentarios" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross fs-14"></i>
              </button>
              <h5><div id="modalMenuFechas"></div><span class="semi-bold">Arcor</span></h5>
            </div>
            <div class="modal-body">
              <p class="pedido-comentario m-t-20" id="modalComentario"></p>

              <button class="btn btn-primary btn-lg btn-block m-t-30" data-dismiss="modal">Entendido</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-collapse/bootstrap-tabcollapse.js" type="text/javascript"></script>

    <script src="assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <!--script src="https://files-stackablejs.netdna-ssl.com/stacktable.min.js"></script-->

    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/pedidos-catering.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/pedidos-catering.js" type="text/javascript"></script>
  </body>
</html>