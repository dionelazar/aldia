<?php 
  require_once '../../Controllers/MenuController.php';
  $controller = new MenuController();
  $menu = $controller->getMenu();
  
  $titulo = 'Aldia | Menú';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <li>
                <a href="menucatering">Menú</a>
              </li>
              <li>
                <a href="#0" class="active"><?php echo $menu->tipo_servicio; ?></a>
              </li>
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <div class="panel-body">
                <h3 class="menu-welcome-heading text-center">Menú desde <?php echo $menu->fecha_desde; ?> al <?php echo $menu->fecha_hasta; ?></h3>
                <p><strong>Menú visible para:</strong>
                  <?php foreach ($menu->clientes as $key => $value): ?>
                  <span class="label label-clients"><?php echo $value->nombre.' '.$value->apellido; ?></span>   
                  <?php endforeach ?>
                </p>
                <p><strong>Descripción:</strong> <?php echo $menu->txt_desc; ?></p>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-plato" id="calendarioPlatos">
                      
                    </table>
                  </div>
                </div>
                <div class="row m-t-20">
                  <div class="col-md-12">
                    <a href="javascript:void(0)" onclick="publicarMenu()" class="btn btn-primary pull-right">Continuar</a>  
                  </div>
                </div>
              </div>
            </div> <!-- Termina panel de clientes -->
          </div> <!-- Termina CONTAINER FLUID -->


          <!-- Comienza Form -->
          <div class="quickview-wrapper calendar-event" id="calendar-event">
            <div class="view-port clearfix" id="eventFormController">
              <form id="form-plato" class="view bg-white">
                <div class="scrollable">
                  <a class="pg-close text-master link pull-right p-r-10 p-t-10" data-toggle="quickview" data-toggle-element="#calendar-event" href="#"><i class="icon" data-icon='9'></i></a>
                  <div class="p-l-30 p-r-30 p-t-20 header-edit-plato text-center">
                    <i class="icon" data-icon='"'></i>
                    <h3>Editar plato</h3>
                  </div>

                  <div class="p-t-15 p-b-15">
                    <div class="form-group-attached">
                      <div class="form-group form-group-default required typehead" id="sample-three">
                        <label>Platos</label>
                        <input class="typeahead form-control sample-typehead" type="text" placeholder="Busca o crea uno nuevo" id="nombrePlato">
                      </div>
                      <div class="form-group form-group-default input-group">
                        <span class="input-group-addon">
                          Kcal
                        </span>
                        <label>Calorias</label>
                        <input type="text" id="kcal" class="form-control kcal">
                      </div>
                      <div class="form-group form-group-default input-group">
                        <span class="input-group-addon">
                          $
                        </span>
                        <label>precio</label>
                        <input type="number" id="precio" class="form-control precio">
                      </div>
                      <div class="form-group form-group-default input-group">
                        <span class="input-group-addon">
                          n°
                        </span>
                        <label>Stock</label>
                        <input type="number" id="stock" class="form-control">
                      </div>
                      <div class="row clearfix">
                        <div class="form-group form-group-default">
                          <label>Descripción</label>
                          <textarea class="form-control" id="txtEventDesc"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <input type="button" id="eventDelete" class="btn btn-block btn-white" value="Eliminar plato">
                  </div>

                  <div class="col-md-6">
                    <input type="button" id="eventSave" class="btn btn-block btn-primary btn-cons" value="Guardar">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

  <script>
    var miMenu = <?php echo json_encode($menu); ?>
  </script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/interactjs/interact.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment-with-locales.min.js"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-typehead/typeahead.bundle.min.js"></script>
    <script src="assets/plugins/bootstrap-typehead/typeahead.jquery.min.js"></script>

    <script src="pages/js/pages.min.js"></script>
    <script src="pages/js/pages.calendar.js"></script>
    <script src="assets/js/menu-welcome-plato.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/menu-welcome-plato.js" type="text/javascript"></script>
  </body>
</html>