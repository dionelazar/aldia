<?php 
    require_once __DIR__.'/../../Controllers/CateringController.php';
    $cateringController = new CateringController();
    $catering = $cateringController->getDatosCatering();

    $title = 'Aldia | Mis Pedidos';
    require_once '../header.cliente.php';
?>

    <section id="videoWelcome" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-img-container center-block">
                        <img src="<?php echo $catering->imagen; ?>">
                    </div>   
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center catering-name"><?php echo $catering->nombre; ?></h2>
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-12">
                    <div class="data-catering">
                        <ul>
                            <li>
                                <span><?php echo $catering->menues_vigentes; ?></span>
                                <p>Menús vigentes</p>
                            </li>
                            <li>
                                <span><?php echo $catering->pedidos_minimos; ?></span>
                                <p>Pedidos minimos</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        
    </section>

    <section class="aldia-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            Descripcion
                        </div>
                        <div class="panel-body">
                            <?php echo $catering->descripcion; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <a href="index.html"><img class="center-block" src="img/logo-white.svg" alt="Aldia"></a>
                    </div>

                    <!-- <ul class="social-contact">
                        <li>
                            <img src="img/facebook.svg" alt="facebook">
                        </li>
                        <li>
                            <img src="img/twitter.svg" alt="twitter">
                        </li>
                    </ul> -->

                    <ul class="footer-links">
                        <li>
                            <a href="mis-pedidos.html">Mis pedidos</a>
                        </li><!-- 
                        <li>
                            <a href="#0">Invitados</a>
                        </li> -->
                        <li>
                            <a href="#0">Configuración</a>
                        </li>
                        <li>
                            <a href="#0">Ayuda</a>
                        </li>
                        <li>
                            <a href="#0">Términos y condiciones</a>
                        </li>
                    </ul>
                </div>    
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.dotdotdot.min.js"></script>

    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
</body>
</html>
