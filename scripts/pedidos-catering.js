$(document).ready(function(){
	getPedidosPendientes()
});

var dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];

Date.prototype.addDays = function() {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate());
    return dat;
}

function getDates(startDate, stopDate) 
{
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate < stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function getDate(miDate)
{
    var dateString  = miDate;
    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);

    var date        = new Date(year, month-1, day);

    currentDate = date.addDays();
    return currentDate;
}

function getPedidosPendientes()
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'pedidos'
            ,call  : 'getPedidosPendientes'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarPedidosPendientes(miData);
        }
    });
}

function getPlatosPedido(id_pedido)
{
	$.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'pedidos'
            ,call  : 'getPlatosPedido'
            ,id_pedido : id_pedido
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarPlatos(miData, id_pedido);
        }
    });
}

function confirmarPedido(myButton, id_pedido)
{
    var confirmMsg = $(myButton).parents('.btn-group-pedidos').next('.pedido-confirmado');
    var thisParent = $(myButton).parents('.btn-group-pedidos');

    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'pedidos'
            ,call  : 'confirmarPedido'
            ,id_pedido : id_pedido
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            thisParent.hide();
            confirmMsg.show();
        }
    });
}

function rechazarPedido(myButton, id_pedido)
{
    var deniedMsg = $(myButton).parents('.btn-group-pedidos').next('.pedido-confirmado').next('.pedido-rechazado');
    var thisParent = $(myButton).parents('.btn-group-pedidos');

    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'pedidos'
            ,call  : 'rechazarPedido'
            ,id_pedido : id_pedido
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            thisParent.hide();
            deniedMsg.show();
        }
    });
}

function armarPedidosPendientes(data)
{
	html = '';
	
	for (var i = 0; i < data.length; i++) 
	{
        html += '<div class="col-md-12">'
        html +=     '<div class="pedidos-card">'
        html +=         '<ul class="pedidos-list">'
        html +=             '<li><i class="icon-calendar"></i> '+data[i].fecha+'</li>'
        html +=             '<li><i class="icon-user"></i> Arcor</li>'
        html +=             '<li><i class="icon-cutlery"></i> Menú del '+data[i].fecha_desde+' al '+data[i].fecha_hasta+'</li>'
        html +=             '<li><i class="icon-wallet"></i> <span class="pedido-price">'+data[i].precio+'</span></li>'
        html +=         '</ul>'

        html +=         '<div class="btn-group btn-group-pedidos">'
        html +=            '<div class="btn-group">'
        html +=                '<button type="button" class="btn btn-rechazar" onclick="rechazarPedido(this, '+data[i].id_pedido+')" id="rechazarBtn_'+data[i].id_pedido+'">'
        html +=                    '<div>'
        html +=                        '<i class="icon-trash"></i>'
        html +=                    '</div>'
        html +=                    '<span class="fs-11 text-uppercase">Rechazar</span>'
        html +=                '</button>'
        html +=            '</div>'
        html +=            '<div class="btn-group">'
        html +=                '<button type="button" class="btn btn-confirmar" onclick="confirmarPedido(this, '+data[i].id_pedido+')" id="confirmarBtn_'+data[i].id_pedido+'">'
        html +=                '<div>'
        html +=                  '<i class="icon-checkmark"></i>'
        html +=                '</div>'
        html +=                '<span class="fs-11 text-uppercase">Confirmar</span>'
        html +=              '</button>'
        html +=            '</div>'
        html +=        '</div>'

        html +=        '<div class="pedido-confirmado pull-right">'
        html +=            '<i class="icon-checkmark"></i> <span>Confirmaste el pedido</span>'
        html +=        '</div>'
        html +=        '<div class="pedido-rechazado pull-right">'
        html +=            '<i class="icon-cross"></i> <span>Rechazaste el pedido</span>'
        html +=        '</div>'

        html +=      '<div class="timeline-pedidos-hidden" id="detallesPedidoPendiente_'+data[i].id_pedido+'">'
        html +=        '<h4>Detalles del pedido</h4>'
        html +=        '<div id="listaPlatosPedidoPendiente_'+data[i].id_pedido+'"></div>'
        html +=            '<a href="#0" class="btn btn-default m-t-30 m-r-10"  data-toggle="modal" data-target="#modalComentarios" onclick="armarComentario(\'' + data[i].comentario + '\' , \'Menú del '+data[i].fecha_desde+' al '+data[i].fecha_hasta+'\')">Ver comentarios</a>'
        html +=            '<a href="#0" class="btn btn-default m-t-30" >Ir al calendario de entregas</a>'
        html +=        '</div>'
        html +=        '<a href="javascript:void(0)" onclick="getPlatosPedido('+data[i].id_pedido+')" class="open-plato-details text-center">'
        html +=            '<p class="change-text" id="btnVerDetalles_'+data[i].id_pedido+'">Ver detalles del pedido</p>'
        html +=            '<i class="icon-chevron-down" id="icoVerDetalles_'+data[i].id_pedido+'"></i>'
        html +=        '</a>'
        html +=    '</div>'
        html += '</div>'
	}

    $('#pedidosPendientes').html(html);

    for (var i = 0; i < data.length; i++) 
    {
        if(data[i].id_estado != 1)
        {
            if(data[i].id_estado == 2)
            {
                var confirmMsg = $('#confirmarBtn_'+data[i].id_pedido).parents('.btn-group-pedidos').next('.pedido-confirmado');
                var thisParent = $('#confirmarBtn_'+data[i].id_pedido).parents('.btn-group-pedidos');
                thisParent.hide();
                confirmMsg.show();
            }else if(data[i].id_estado == 3){
                var deniedMsg = $('#rechazarBtn_'+data[i].id_pedido).parents('.btn-group-pedidos').next('.pedido-confirmado').next('.pedido-rechazado');
                var thisParent = $('#rechazarBtn_'+data[i].id_pedido).parents('.btn-group-pedidos');
                thisParent.hide();
                deniedMsg.show();
            }
        }
    }
}

function armarPlatos(data, id_pedido)
{
	var menuDetails = $('#detallesPedidoPendiente_'+id_pedido);
    var btnText = $('#btnVerDetalles_'+id_pedido);
    var angleDown = $('#icoVerDetalles_'+id_pedido);

    menuDetails.slideToggle();
    
    btnText.text(function(i, text){
        return text === "Ver detalles del pedido" ? "Ocultar detalles" : "Ver detalles del pedido";
    });

    $('html, body').animate({
        //scrollTop: menuDetails.offset().top - 160
    }, 1000);

    angleDown.toggleClass('icon-chevron-down icon-chevron-up');

    var html = '';

    for (var i = 0; i < data.length; i++) 
    {
    	var fec = getDate(data[i].fecha_plato)
	    var dia = dias[fec.getDay()];
	    var numDia = fec.getDate();

	    var myElem = document.getElementById('ulTimelinePlatos_'+dia+numDia);
		//if (myElem === null)
			html += '<ul class="timeline-checkout" id="ulTimelinePlatos_'+dia+numDia+'">';
		    html += '  <li class="day-plato">';
		    html += '    <h4>'+dia+' '+numDia+'</h4>';
		    html += '    <p class="plato-checkout">'+data[i].plato+' x '+data[i].cantidad+'</p>';
		    html += '  </li>';
		    html += '</ul>';
    }

    $('#listaPlatosPedidoPendiente_'+id_pedido).html(html);
}

function armarComentario(comentario, menu)
{
    $('#modalComentario').html(comentario);
    $('#modalMenuFechas').html(menu);
}