<?php
  require_once '../../Controllers/CateringController.php';
  $controller = new CateringController();
  $controller->primeraVez();
  $titulo = 'Aldia | Perfil catering';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de menu welcome -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"><i class="icon" data-icon="&#xe056;"></i> Mi Perfíl</div>
              </div>  
                <div id="perfilWizard" class="m-t-20">
                  <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
                    <li class="active">
                      <a data-toggle="tab" href="#tab1" onclick="selectab(1)"><span>Bienvenido</span></a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab2" onclick="selectab(2)"><span>Sobre tu catering</span></a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab3" onclick="selectab(3)"><span>Zona de cobertura</span></a>
                    </li>
                    <li class="hidden">
                      <a data-toggle="tab" href="#tab4" onclick="selectab(4)"><span>Fin</span></a>
                    </li>
                  </ul>

                  <div class="tab-content">
                    <div class="tab-pane active slide-left" id="tab1">
                      <div class="row row-same-height">
                        <h2 class="text-center font-montserrat menu-welcome-heading">¡Comencemos!</h2>
                        <h4 class="text-center">Estamos listos para ayudarte a completar tu perfíl y que más clientes te conozcan</h4>

                        <div class="upload-img-wrapper">
                          <p class="text-center cta-upload-profile">Subí tu imagen</p>

                          <form id="formImg" enctype="multipart/form-data">
                            <label class="center-block text-center" for="preImg">
                              <input type='file' id="preImg" name="preImg" />
                              <img id="imgCat" class="img-responsive" src="http://via.placeholder.com/150x150" alt="your image" />  
                            </label>
                          </form>

                          <p class="text-center tip-upload">Recuerda subir una imagen que represente tu catering, que tenga 150px de altura y 150px de ancho</p>
                        </div> <!-- Termina el uploader de img -->
                        
                      </div>
                    </div>

                    <div class="tab-pane slide-left padding-20" id="tab2">
                      <div class="row row-same-height">
                        <h2 class="text-center font-montserrat menu-welcome-heading">Contanos un poco más</h2>
                        <h4 class="text-center">Escribe una breve descripción de tu catering para que tus clientes te conozcan aún más</h4>
                        
                        <br>

                        <form id="about">
                          <div class="form-group form-group-default">
                            <label>CUENTALE A TUS CLIENTES SOBRE TU CATERING, COMO TRABAJAN, TIPOS DE MENÚES, ETC.</label>
                            <textarea class="form-control textarea-control" id="descripcionCatering"></textarea>
                          </div>

                          <div class="form-group form-group-default required ">
                            <label>CANTIDAD MÍNIMA DE PEDIDOS DIARIOS</label>
                            <input type="number" class="form-control" id="cantidadMinima" required>
                          </div>  
                        </form>
                      </div>
                    </div>

                    <div class="tab-pane slide-left padding-20" id="tab3">
                      <div class="row row-same-height">
                        <h2 class="text-center font-montserrat menu-welcome-heading">¿Cuál es tu cobertura?</h2>
                        <h4 class="text-center">Elije en que barrios estará disponible tu catering. Recuerda que esta información se ve reflejada en el directorio de búsqueda</h4>

                        <br>
                        
                        <div class="form-group form-group-default form-group-default-select2 required">
                          <label class="">Zonas de cobertura</label>
                          <select id="zonaCobertura" class="full-width" data-placeholder="Busca un barrio" data-init-plugin="select2" multiple>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="tab-pane slide-left padding-20" id="tab4">
                      <p class="text-center congrats-icon m-b-30"><i class="icon" data-icon="&#xe04e;"></i></p>
                      
                      <h2 class="text-center font-montserrat menu-welcome-heading">¡Felicitaciones!</h2>
                      <h4 class="text-center">Haz completado tu información para que tus clientes puedan encontrarte y saber sobre tu catering. Recuerda que puedes cambiar esta información cuando quieras</h4>
                    </div>

                    <div class="padding-20 bg-white">
                      <ul class="pager wizard">
                        <li class="next">
                          <button class="btn btn-primary btn-cons pull-right" onclick="continuar()" type="button">
                            <span>Continuar</span>
                          </button>
                        </li>
                        <li class="next finish hidden">
                          <a href="catering" class="btn btn-primary btn-cons  pull-right" type="button">Finalizar</a>
                        </li>
                        <li class="previous first hidden">
                          <button class="btn btn-default btn-cons  pull-right" type="button">
                            <span>Rehacer</span>
                          </button>
                        </li>
                        <li class="previous">
                          <button class="btn btn-default btn-cons pull-right" onclick="anterior()" type="button">
                            <span>Anterior</span>
                          </button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div> <!-- Termina .panel -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script src="assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/perfil-welcome.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
    <script src="scripts/perfil-welcome-catering.js" type="text/javascript"></script>
  </body>
</html>