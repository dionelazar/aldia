GO

ALTER TABLE platos ADD sn_activo INT

GO

UPDATE platos set sn_activo = -1

GO

ALTER PROC [dbo].[sp_get_catering_by_id]
(
	@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM catering WHERE id_catering = @id_catering) BEGIN
		SELECT 
				  '' 'mensaje'
				, -1 'cod_mensaje'
				, c.txt_desc 'nombre'
				, c.txt_domicilio 'domicilio'
				, c.txt_razon_social 'razon_social'
				, c.id_tipo_doc 'tipo_doc'
				, c.nro_doc 'nro_doc'
				, c.id_iva 'id_iva'
				, i.txt_desc 'iva'
				, c.url_imagen 'imagen'
				, c.txt_about 'descripcion'
				, c.pedidos_minimos 'pedidos_minimos'
		FROM catering c
		LEFT JOIN iva_condiciones i ON i.id_iva = c.id_iva
		WHERE id_catering = @id_catering
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
		      ,'No existe el catering. ' 'mensaje'
	END
END

GO

CREATE TABLE [dbo].[rutas](
	[id_ruta] [int] NOT NULL,
	[txt_desc] [varchar](200) NOT NULL,
 CONSTRAINT [PK_rutas] PRIMARY KEY CLUSTERED 
(
	[id_ruta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

insert into rutas values(1, 'subidas/images/')
insert into rutas values(2, 'subidas/imgCatering/')
insert into rutas values(3, 'subidas/imgPerfil/')
insert into rutas values(4, 'subidas/videos/')

GO

CREATE PROC sp_ins_imagen_catering
(
	@id_catering INT
	,@extension VARCHAR(20)
)	
AS BEGIN
SET NOCOUNT ON
	DECLARE @carpeta VARCHAR(100)
	DECLARE @ruta VARCHAR(500)

	SELECT @carpeta = txt_desc FROM rutas WHERE id_ruta = 2
	SET @ruta = @carpeta+CONVERT(VARCHAR(10), @id_catering)+'.'+@extension

	UPDATE catering SET url_imagen = @ruta WHERE id_catering = @id_catering

	SELECT -1 'cod_mensaje'
	      ,'Imagen subida correctamente' 'mensaje'
		  ,@ruta 'ruta'
END

GO

CREATE PROC sp_ins_about_catering
(
	 @id_catering INT
	,@about VARCHAR(MAX)
	,@pedidos_minimos INT
)
AS BEGIN
SET NOCOUNT ON
	UPDATE catering SET txt_about = @about, pedidos_minimos = @pedidos_minimos WHERE id_catering = @id_catering
	SELECT -1 'cod_mensaje'
	      ,'Descripción subida correctamente' 'mensaje'
END

GO

insert into tmunicipio values(	1,	1,	0, 'Todos', null)
insert into tmunicipio values(	1,	2,	0, 'Todos', null)
insert into tmunicipio values(	1,	3,	0, 'Todos', null)
insert into tmunicipio values(	1,	4,	0, 'Todos', null)
insert into tmunicipio values(	1,	5,	0, 'Todos', null)
insert into tmunicipio values(	1,	6,	0, 'Todos', null)
insert into tmunicipio values(	1,	7,	0, 'Todos', null)
insert into tmunicipio values(	1,	8,	0, 'Todos', null)
insert into tmunicipio values(	1,	9,	0, 'Todos', null)
insert into tmunicipio values(	1,	10,	0, 'Todos', null)
insert into tmunicipio values(	1,	11,	0, 'Todos', null)
insert into tmunicipio values(	1,	12,	0, 'Todos', null)
insert into tmunicipio values(	1,	13,	0, 'Todos', null)
insert into tmunicipio values(	1,	14,	0, 'Todos', null)
insert into tmunicipio values(	1,	15,	0, 'Todos', null)
insert into tmunicipio values(	1,	16,	0, 'Todos', null)
insert into tmunicipio values(	1,	17,	0, 'Todos', null)
insert into tmunicipio values(	1,	18,	0, 'Todos', null)
insert into tmunicipio values(	1,	19,	0, 'Todos', null)
insert into tmunicipio values(	1,	20,	0, 'Todos', null)
insert into tmunicipio values(	1,	21,	0, 'Todos', null)
insert into tmunicipio values(	1,	22,	0, 'Todos', null)
insert into tmunicipio values(	1,	23,	0, 'Todos', null)
insert into tmunicipio values(	1,	24,	0, 'Todos', null)

	GO

GO

CREATE TABLE [dbo].[zonas_cobertura_de_catering](
	[id_zona] [int] NOT NULL,
	[id_pais] [int] NOT NULL,
	[id_dpto] [int] NOT NULL,
	[id_municipio] [int] NOT NULL,
	[id_catering] [int] NOT NULL,
 CONSTRAINT [PK_zonas_cobertura_de_catering] PRIMARY KEY CLUSTERED 
(
	[id_zona] ASC,
	[id_pais] ASC,
	[id_dpto] ASC,
	[id_municipio] ASC,
	[id_catering] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[zonas_cobertura_de_catering]  WITH CHECK ADD  CONSTRAINT [FK_zonas_cobertura_de_catering_catering] FOREIGN KEY([id_catering])
REFERENCES [dbo].[catering] ([id_catering])
GO

ALTER TABLE [dbo].[zonas_cobertura_de_catering] CHECK CONSTRAINT [FK_zonas_cobertura_de_catering_catering]
GO

ALTER TABLE [dbo].[zonas_cobertura_de_catering]  WITH CHECK ADD  CONSTRAINT [FK_zonas_cobertura_de_catering_tmunicipio] FOREIGN KEY([id_pais], [id_dpto], [id_municipio])
REFERENCES [dbo].[tmunicipio] ([cod_pais], [cod_dpto], [cod_municipio])
GO

ALTER TABLE [dbo].[zonas_cobertura_de_catering] CHECK CONSTRAINT [FK_zonas_cobertura_de_catering_tmunicipio]
GO


go
CREATE PROC sp_get_zonas_cobertura
(
	@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM zonas_cobertura_de_catering WHERE id_catering = @id_catering) BEGIN
		SELECT  -1 'cod_mensaje'
			   ,'ok' 'mensaje'
			   ,z.id_pais 'id_pais'
			   ,z.id_dpto 'id_dpto'
			   ,z.id_municipio 'id_municipio'
			   ,m.txt_desc 'municipio'
		FROM zonas_cobertura_de_catering z
		 JOIN tmunicipio m ON m.cod_pais = z.id_pais AND m.cod_dpto = z.id_dpto AND m.cod_municipio = z.id_municipio
		WHERE id_catering = @id_catering
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
			 ,'No hay zonas registradas' 'mensaje'
	END
END

GO

CREATE PROC sp_ins_plato
(
	 @id_menu INT
	,@id_catering INT
	,@fecha SMALLDATETIME
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_plato INT

	SELECT @id_plato = ISNULL(MAX(id_plato), 0) + 1 FROM platos WHERE id_menu = @id_menu

	INSERT INTO platos ( id_plato,  id_menu,  id_catering, fecha, txt_plato, txt_desc, calorias, precio, stock ,sn_activo)
				VALUES (@id_plato, @id_menu, @id_catering, @fecha , null     , null    , null    , null  , null, -1 )
	SELECT  @id_plato 'id_plato'
			,@id_menu 'id_menu'
			,@id_catering 'id_catering'
			,@fecha 'fecha'
			,-1 'cod_mensaje'
			,'Nuevo plato ingresado correctamente' 'mensaje'
END


GO

ALTER PROC [dbo].[sp_get_platos]
(
	 @id_menu INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT id_plato 'id_plato'
		  ,id_menu 'id_menu'
	      ,id_catering 'id_catering'
		  ,fecha 'fecha'
		  ,txt_plato 'plato'
		  ,txt_desc 'txt_desc'
		  ,calorias 'calorias'
		  ,precio 'precio'
		  ,stock 'stock'
	FROM platos
	WHERE id_menu = @id_menu
	  AND id_catering = @id_catering
	  AND sn_activo = -1
END

GO

CREATE PROC sp_delete_plato
(
	 @id_menu INT
	,@id_plato INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	UPDATE platos SET sn_activo = 0
	WHERE id_catering = @id_catering
	  AND id_menu = @id_menu
	  AND id_plato = @id_plato

	SELECT -1 'cod_mensaje'
	      ,'plato eliminado correctamente' 'mensaje'
END

GO


CREATE PROC sp_get_plato_especifico
(
	 @id_menu INT
	,@id_plato INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT id_plato 'id_plato'
		  ,id_menu 'id_menu'
	      ,id_catering 'id_catering'
		  ,fecha 'fecha'
		  ,txt_plato 'plato'
		  ,txt_desc 'txt_desc'
		  ,calorias 'calorias'
		  ,precio 'precio'
		  ,stock 'stock'
	FROM platos
	WHERE id_menu = @id_menu
	  AND id_plato = @id_plato
	  AND id_catering = @id_catering
	  AND sn_activo = -1
END

GO

CREATE PROC sp_ins_detalle_direccion
(
	 @id_usuario INT
	,@txt_calle VARCHAR(1000)
	,@txt_entre_calles VARCHAR(2000)
	,@altura INT
	,@piso INT
	,@oficina VARCHAR(50)
	,@cod_postal VARCHAR(50)
	,@id_dpto INT
	,@id_municipio INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM direccion_detalle_usuario WHERE id_usuario = @id_usuario) BEGIN
		UPDATE direccion_detalle_usuario SET
			txt_calle = @txt_calle
			,txt_entre_calles = @txt_entre_calles
			,altura = @altura
			,piso = @piso
			,oficina = @oficina
			,cod_postal = @cod_postal
			,id_pais = 1
			,id_dpto = @id_dpto
			,id_municipio = @id_municipio
		WHERE id_usuario = @id_usuario

		SELECT -1 'cod_mensaje'
			  ,'Actualizado correctamente' 'mensaje'
	END ELSE BEGIN
		INSERT INTO direccion_detalle_usuario ( id_usuario,  txt_calle,  txt_entre_calles,  altura,  piso,  oficina,  cod_postal,  id_pais,  id_dpto,  id_municipio)
										VALUES(@id_usuario, @txt_calle, @txt_entre_calles, @altura, @piso, @oficina, @cod_postal,  1 ,      @id_dpto, @id_municipio)
		SELECT -1 'cod_mensaje'
			  ,'Ingresado correctamente' 'mensaje'
	END
END

GO

ALTER TABLE users DROP COLUMN txt_direccion
ALTER TABLE users DROP COLUMN cod_tipo_doc
ALTER TABLE users DROP COLUMN nro_doc

GO

ALTER PROC [dbo].[sp_ins_user]
(
	 @txt_nombre VARCHAR(1000)
	,@txt_apellido VARCHAR(1000)
	,@txt_password VARCHAR(1000)
	,@cod_tipo_usuario INT
	,@txt_email VARCHAR(1000)
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @cod_usuario INT

	IF EXISTS(SELECT 1 FROM users WHERE txt_email = @txt_email) BEGIN
		SELECT 'El email ya está en uso' 'mensaje'
				,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT @cod_usuario = ISNULL(MAX(cod_usuario), 0) + 1 FROM users

		INSERT INTO users (cod_usuario
						 , cod_tipo_usuario
						 , txt_nombre
						 , txt_apellido
						 , txt_password
						 , url_imagen
						 , txt_email
						 , id_catering)
					values(@cod_usuario
						 , @cod_tipo_usuario
						 , @txt_nombre
						 , @txt_apellido
						 , @txt_password
						 , ''
						 , @txt_email
						 , @id_catering)
		SELECT 'Usuario creado correctamente' 'mensaje'
			  ,-1 'cod_mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_ins_catering]
(
	 @codigo_activacion VARCHAR(100)
	,@nombre VARCHAR(200)
	,@apellido VARCHAR(200)
	,@catering VARCHAR(200)
	,@email VARCHAR(200)
	,@password VARCHAR(200)
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_catering INT
	       ,@id_cod_activacion INT
	
	SELECT @id_cod_activacion = id_cod FROM codigos_activacion WHERE txt_cod = @codigo_activacion
	SELECT @id_catering = ISNULL(MAX(id_catering), 0) + 1  FROM catering

	IF EXISTS(SELECT 1 FROM codigos_activacion WHERE id_cod = @id_cod_activacion AND sn_activo = -1) BEGIN
		IF EXISTS(SELECT 1 FROM catering WHERE txt_desc = @catering) BEGIN
			SELECT 0 'cod_mensaje'
				  ,'Ya existe un catering con ese nombre.' 'mensaje'
		END ELSE BEGIN
			INSERT INTO catering (id_catering, txt_desc, txt_domicilio, txt_razon_social, id_tipo_doc, nro_doc, id_iva, sn_activa)
							VALUES(@id_catering, @catering, null, null, null, null, null, -1)
			EXEC sp_ins_user @nombre, @apellido, @password, 1, @email, @id_catering
			UPDATE codigos_activacion SET sn_activo = 0 WHERE id_cod = @id_cod_activacion
			SELECT -1 'cod_mensaje'
				  ,'Catering registrado correctamente.' 'mensaje'
		END
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
				  ,'El código de activación no existe o ya fue usado.' 'mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_get_login]
(
	@email VARCHAR(1000)
	,@pass VARCHAR(1000)
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @cod_usuario INT
	SELECT @cod_usuario = cod_usuario FROM users WHERE txt_email = @email AND txt_password = @pass

	IF @cod_usuario IS NOT NULL BEGIN
		SELECT   'Login correcto' 'mensaje'
				,-1 'cod_mensaje'
				,cod_usuario 'id_usuario'
				,cod_tipo_usuario 'tipo_usuario'
				,txt_nombre 'nombre'
				,txt_apellido 'apellido'
				,id_catering 'id_catering'
			FROM users
			WHERE cod_usuario = @cod_usuario
	END ELSE BEGIN
		IF EXISTS(SELECT 1 FROM users WHERE txt_email = @email) BEGIN
			SELECT 0 'cod_mensaje'
				  ,'Contraseña incorrecta' 'mensaje'
		END ELSE BEGIN
			SELECT 0 'cod_mensaje'
				  ,'Email incorrecto' 'mensaje'
		END
	END
END

GO

ALTER PROC [dbo].[sp_get_clientes_menu]
(
	 @id_menu INT
	,@id_catering INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT v.id_usuario 'id_usuario'
		  ,u.txt_nombre 'nombre'
		  ,u.txt_apellido 'apellido'
		  ,u.txt_email 'email'
		  --direccion
	FROM visibilidad_menu_por_usuario v
	JOIN users u ON u.cod_usuario = v.id_usuario
	WHERE   v.id_menu = @id_menu
		AND v.id_catering = @id_catering
		AND v.sn_activo = -1
END

GO

CREATE PROC sp_ins_detalle_direccion
(
	 @id_usuario INT
	,@txt_calle VARCHAR(1000)
	,@txt_entre_calles VARCHAR(2000)
	,@altura INT
	,@piso INT
	,@oficina VARCHAR(50)
	,@cod_postal VARCHAR(50)
	,@id_dpto INT
	,@id_municipio INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM direccion_detalle_usuario WHERE id_usuario = @id_usuario) BEGIN
		UPDATE direccion_detalle_usuario SET
			txt_calle = @txt_calle
			,txt_entre_calles = @txt_entre_calles
			,altura = @altura
			,piso = @piso
			,oficina = @oficina
			,cod_postal = @cod_postal
			,id_pais = 1
			,id_dpto = @id_dpto
			,id_municipio = @id_municipio
		WHERE id_usuario = @id_usuario

		SELECT -1 'cod_mensaje'
			  ,'Actualizado correctamente' 'mensaje'
	END ELSE BEGIN
		INSERT INTO direccion_detalle_usuario ( id_usuario,  txt_calle,  txt_entre_calles,  altura,  piso,  oficina,  cod_postal,  id_pais,  id_dpto,  id_municipio)
										VALUES(@id_usuario, @txt_calle, @txt_entre_calles, @altura, @piso, @oficina, @cod_postal,  1 ,      @id_dpto, @id_municipio)
		SELECT -1 'cod_mensaje'
			  ,'Ingresado correctamente' 'mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_get_clientes_by_usuario_catering]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_catering INT

	SELECT @id_catering = id_catering
		FROM users WHERE cod_usuario = @id_usuario

	SELECT cod_usuario 'id_usuario'
	      ,cod_tipo_usuario 'id_tipo_usuario'
		  ,txt_nombre 'nombre'
		  ,txt_apellido 'apellido'
		  ,txt_email 'email'
	FROM users
	WHERE id_catering = @id_catering
	  AND cod_tipo_usuario = 2
END

GO

CREATE PROC sp_get_direccion_usuario
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT txt_calle 'calle'
		  ,txt_entre_calles 'entre_calles'
		  ,altura 'altura'
		  ,piso 'piso'
		  ,oficina 'oficina'
		  ,cod_postal 'cod_postal'
		  ,id_dpto 'id_provincia'
		  ,id_municipio 'id_municipio'
	FROM direccion_detalle_usuario
	WHERE id_usuario = @id_usuario
END

GO


GO
ALTER TABLE users ADD txt_telefono VARCHAR(100)
GO
ALTER PROC sp_get_usuario
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT cod_tipo_usuario 'id_tipo_usuario'
		  ,txt_nombre 'nombre'
		  ,txt_apellido 'apellido'
		  ,txt_email 'email'
		  ,txt_telefono 'telefono'
	FROM users
	WHERE cod_usuario = @id_usuario
END

GO 

CREATE PROC sp_upd_datos_usuario
(
	  @id_usuario INT
	, @nombre VARCHAR(500)
	, @apellido VARCHAR(500)
	, @email VARCHAR(500)
	, @telefono VARCHAR(100)
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM users WHERE txt_email = @email AND cod_usuario != @id_usuario) BEGIN
		SELECT 0 'cod_mensaje'
			  ,'El email ya está en uso' 'mensaje'
	END ELSE BEGIN
		UPDATE users SET 
			 txt_nombre = @nombre
			,txt_apellido = @apellido
			,txt_email = @email
			,txt_telefono = @telefono
		WHERE cod_usuario = @id_usuario

		SELECT -1 'cod_mensaje'
			  ,'Editado correctamente' 'mensaje'
	END
END

GO

