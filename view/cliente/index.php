<?php 
    require_once __DIR__.'/../../Controllers/ClienteController.php';
    $clienteController = new ClienteController();
    $usuario = $clienteController->getDatosUser();
    $catering = $clienteController->getDatosCatering();
    $title = 'Aldia | Home';
    require_once '../header.cliente.php';

    //echo '<pre>';print_r($platos);echo '</pre>';
?>

    <section id="videoWelcome" class="">
        <h1 class="text-center">Hola <?php echo $usuario->nombre; ?> <br>Hace tu pedido a <?php echo $catering->nombre; ?></h1>
    </section>

    <section id="menu" class="aldia-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center main-color-text">Menú</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-pills nav-menu text-center nav-semanas" id='ulSemanas'>
                      <!-- Este toggle es para la version mobile -->
                      <li class="toggle-mobile-semanas"><button id="toggleSemanas"><i class="icon-chevron-down"></i></button></li>
                    </ul>
                </div>
            </div>

            <div class="row menu-timeline-section">
                <div class="col-md-9">
                    <div class="panel">
                        <div id="semanasTab" class="tab-content">                        

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div id="pedidoWrapper" class="panel">
                        <header class="panel-heading">
                            Tu pedido
                        </header>
                        <div class="panel-body">
                            <div id="sinPedido">
                                <img src="img/hungry.svg" alt="" class="img-responsive">

                                <h4 class="text-center">Aún no agregaste platos a tu pedido.</h4>
                                <p class="text-center">Aqui podrás ver como se arma tu pedido</p>    
                            </div>

                            <div class="pedido hidden">
                                <div class="alert alert-info" role="alert">
                                    El pedido mínimo es de <strong id="pedidosMinimos"><?php echo $catering->pedidos_minimos ?></strong> platos diarios
                                </div>

                                <div class="timeline-row" id="miCarrito">

                                </div>

                                <hr>

                                <div class="totals-row">
                                    <ul class="checkout-totals">
                                        <li><strong><span>Total</span> <span class="check-prices pull-right" id="totalCarrito">0</span></strong></li>
                                    </ul>

                                    <textarea class="pedido-comment" placeholder="Puedes agregar un comentario" name="" id="carritoComentario"></textarea>

                                    <div class="row">
                                        <div class="col-xs-6 hidden-sm hidden-md hidden-lg">
                                            <button id="hidePedido" class="btn btn-back btn-block">Volver</button>
                                        </div>
                                        <div class="col-md-12 col-xs-6">
                                            <input type="button" class="btn btn-checkout btn-block" onclick="realizarPedido()" value="Realizar pedido">
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="alertAddPlato">
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="icon-cross"></i></a>
            Se agrego el plato a tu pedido
        </div>    
    </div>

    <div class="show-pedido-row">
        <button id="showPedido" class="btn btn-checkout btn-block"><i class="icon-cart"></i> Ver pedido</button>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <a href="index"><img class="center-block" src="img/logo-white.svg" alt="Aldia"></a>
                    </div>

                    <!-- <ul class="social-contact">
                        <li>
                            <img src="img/facebook.svg" alt="facebook">
                        </li>
                        <li>
                            <img src="img/twitter.svg" alt="twitter">
                        </li>
                    </ul> -->

                    <ul class="footer-links">
                        <li>
                            <a href="mispedidos">Mis pedidos</a>
                        </li><!-- 
                        <li>
                            <a href="#0">Invitados</a>
                        </li> -->
                        <li>
                            <a href="configuracion">Configuración</a>
                        </li>
                        <li>
                            <a href="#0">Ayuda</a>
                        </li>
                        <li>
                            <a href="#0">Términos y condiciones</a>
                        </li>
                    </ul>
                </div>    
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.dotdotdot.min.js"></script>

    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/index.js"></script>
</body>
</html>
