<div class="pull-right">
          <div class="header-inner">
            <!-- Comienza NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l no-style p-l-30 p-r-20">
              <li class="p-t-5 inline">
                <div class="dropdown">
                  <a href="javascript:;" id="notification-center" class="icon notification-icon" data-icon="x" data-toggle="dropdown">
                    <span class="bubble"></span>
                  </a>
                  <!-- Comienza Notification Dropdown -->
                  <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                    <!-- Comienza Notification -->
                    <div class="notification-panel">
                      <!-- Comienza Notification Body-->
                      <div class="notification-body scrollable">
                        <!-- Comienza Notification Item-->
                        <div class="notification-item unread clearfix"> <!-- ¡IMPORTANTE! La clase .unread corresponde si la notificación es nueva -->
                          <div class="heading">
                            <a href="#0" class="pull-left">
                              <span class="bold">Te ha llegado una notificacion re larga</span>
                            </a>
                            <span class="pull-right time">Hoy 11:00am</span>
                          </div>
                        </div><!-- Termina Notification Item-->
                        <!-- Comienza Notification Item-->
                        <div class="notification-item clearfix">
                          <div class="heading">
                            <a href="#" class="pull-left">
                              <span class="bold">Esta es una notificación muy fancy</span>
                            </a>
                            <span class="pull-right time">Ayer 11:00am</span>
                          </div>
                        </div><!-- Termina Notification Item-->
                        <!-- Comienza Notification Item-->
                        <div class="notification-item clearfix"> 
                          <div class="heading">
                            <a href="#" class="pull-left">
                              <span class="bold">¡Hola mundo!</span>
                            </a>
                            <span class="pull-right time">31/8/2016 11:00pm</span>
                          </div>
                        </div><!-- Termina Notification Item-->
                      </div><!-- Termina Notification Body-->
                      <!-- Comienza Notification Footer-->
                      <div class="notification-footer text-center">
                        <a href="#" class="">Leer todas las notificaciones</a>
                        <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                        </a>
                      </div><!-- Termina Notification Footer-->
                    </div><!-- Termina Notification -->
                  </div><!-- Termina Notification Dropdown -->
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div><!-- Termina HEADER -->