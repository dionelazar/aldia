<?php 
/**
* 
*/
require_once 'class.conection.php';

class Admin extends Conection
{
	
	public function generateLinkRegistroCatering()
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_codigo_activacion";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 1001', $sql, $params);
			return Helpers::returnError(0, 'Error 1001');
		}else{
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
			$data = new Admin();

			$data->mensaje      = utf8_encode($row['mensaje']);
			$data->cod_mensaje  = $row['cod_mensaje'];
			$data->codigo       = $row['codigo'];

			Conection::CerrarConexion($conn);	
			return $data;
		}
	}
}

?>