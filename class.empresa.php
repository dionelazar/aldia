<?php 
/**
* 
*/
require_once __DIR__.'/class.conection.php';

class Empresa extends Conection
{
	public $id_empresa;
	public $id_tipo_empresa;
	public $id_iva;
	public $id_tipo_doc;
	public $iva;
	public $tipo_doc;
	public $tipo_empresa;
	public $nro_doc;
	public $nombre;
	public $url_imagen;

	public function getEmpresa($post)
	{

	}

	public function getIva($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_iva";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Helpers::insertarError('Error 7002', $sql, $params);
			return Helpers::jsonEncodeError(0, 'Error 7002');
		}else{
			$ivas = array();
			$i = 0;

			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
			{
				$mi_iva = new Empresa();

				$mi_iva->id_iva = $row['id_iva'];
				$mi_iva->iva = utf8_encode($row['txt_desc']);

				$ivas[$i] = $mi_iva;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $ivas );
		}
	}

	public function insEmpresa($post)
	{

	}
} 
?>