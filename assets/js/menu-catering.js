$(document).ready(function() {
    var menuMsg = "¡Genial! Se republicó el menú con exito";
    var position = "top";
    var styleBar = "bar";
    var typeBar = "success";
    
    $('#menuMsg').click(function(){
    	$('body').pgNotification({
    		style: styleBar,
    		message: menuMsg,
    		position: position,
    		timeout: 5000,
    		type: typeBar
    	}).show();	
    })
});