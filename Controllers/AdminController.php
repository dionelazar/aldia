<?php 
/**
* 
*/
require_once 'Controller.php';
require_once __DIR__.'/../class.admin.php';

class AdminController extends Controllers
{
	function generateLinkRegistroCatering()
	{
		$env = 'localhost/aldia/';
		//$env = 'http://aldia.com/';
		$dataCod = Admin::generateLinkRegistroCatering();
		if($dataCod->cod_mensaje == -1)
			$link = $env.'registro-catering&'.$dataCod->codigo;
		else
			return $dataCod->mensaje;

		return $link;
	}
}
 ?>