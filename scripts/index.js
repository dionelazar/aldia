var dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];
var miPedido = [];

$(document).ready(function(){
    getMenues();
});

function getFechas(fecha_desde, fecha_hasta)
{
	var fechaDesde = new Date((fecha_desde).split("-").reverse());
	var fechaHasta = new Date((fecha_hasta).split("-").reverse());

	var timeDiff = Math.abs(fechaDesde.getTime() - fechaHasta.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

	var allDates = getDates(fechaDesde, fechaHasta);
	
	var misDatosFecha = {diffDays : diffDays, allDates : allDates};
	return misDatosFecha;
}

Date.prototype.addDays = function() {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate());
    return dat;
}

function getDates(startDate, stopDate) 
{
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate < stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function getDate(miDate)
{
    var dateString  = miDate;
    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);

    var date        = new Date(year, month-1, day);

    currentDate = date.addDays();
    return currentDate;
}

function formatDate(date)
{
    var dateSplit = date.split("-");
    return dateSplit[0]+'/'+dateSplit[1];
}

function getMenues()
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'menues'
            ,call  : 'getMenuesVigentes'
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarMenues(miData);
        }
    });
}

function getPlatos(id_menu)
{
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'platos'
            ,call  : 'getPlatos'
            ,id_menu : id_menu
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
            armarPlatos(miData);
        }
    });
}

function armarMenues(data)
{
    var ulSemanas = '';
    var id_primer_menu = parseInt( data[0].id_menu );
    var tabSemanas = '';
    for (var i = 0; i < data.length; i++) 
    {
        if( i == 0 )
            ulSemanas += '<li id="liSemana_'+i+'" class="active">';
        else
            ulSemanas += '<li id="liSemana_'+i+'">';

        ulSemanas += '<a data-toggle="pill" href="#semana'+i+'" onclick="getPlatos('+data[i].id_menu+')">Desde el '+formatDate(data[i].fecha_desde)+' al '+formatDate(data[i].fecha_hasta)+'</a>';
        ulSemanas += '</li>';

        if( i == 0 )
            tabSemanas += '<div id="semana'+i+'" class="tab-pane fade in active">';
        else
            tabSemanas += '<div id="semana'+i+'" class="tab-pane fade">';

        tabSemanas += ' <ul class="nav nav-tabs nav-day" id="navSemana_'+data[i].id_menu+'">';
        tabSemanas += ' </ul>';
        tabSemanas += '<div class="tab-content pad-15" id="platosSemana_'+data[i].id_menu+'">';
        tabSemanas += '</div>';
        tabSemanas += '</div>';
    }
    $('#semanasTab').html(tabSemanas);
    $('#ulSemanas').html(ulSemanas);
    getPlatos(id_primer_menu);
}

function armarPlatos(data)
{
    var html = '';

    var misDias = []; //aca van los nombres de los tabs, no entran las fechas que no tienen platos
    var fechas = [];

    for (var i = 0; i < data.length; i++) 
    {
        var miFecha = getDate(data[i].fecha);
        fechas.push( miFecha );
    }

    fechas.sortBy(function(o){ return o.date }); //ordeno las fechas

    var liFechas = '';
    for (var i = 0; i < data.length; i++) {
        var dia = dias[fechas[i].getDay()];
        var numDia = fechas[i].getDate();
        var liFecha = '';        
        if(misDias.indexOf(dia+' '+numDia) == -1)
        {
            if( i == 0)
                liFecha = '<li class="active"><a data-toggle="tab" href="#'+dia+numDia+'" id="liDia_'+data[0].id_menu+'_'+numDia+'">'+dia+' '+numDia+'</a></li>';
            else
                liFecha = '<li><a data-toggle="tab" href="#'+dia+numDia+'" id="liDia_'+data[0].id_menu+'_'+numDia+'">'+dia+' '+numDia+'</a></li>';
            
            liFechas += liFecha;
        }
        misDias.push(dia+' '+numDia);        
    }
    $('#navSemana_'+data[0].id_menu).html(liFechas);

    var diaAnterior = '';
    var misPlatos = '';
    for (var i = 0; i < data.length; i++) 
    {
        var dia = dias[fechas[i].getDay()];
        var numDia = fechas[i].getDate();
        var liFecha = '';
        var miPlato = '';

        if(diaAnterior != dia){
            miPlato += '<div id="'+dia+numDia+'" class="tab-pane fade in active">';
            miPlato += '<div class="row">';
        } 

        miPlato += platoPorDia(data[i]);

        var diaSiguiente = fechas[i+1] == undefined ? 0 : dias[fechas[i+1].getDay()];

        if(dia != diaSiguiente){
            miPlato += '</div>';
            miPlato += '</div>';
        }
        diaAnterior = dia;
        misPlatos += miPlato;
    }
    $('#platosSemana_'+data[0].id_menu).html(misPlatos);
    //para arreglar que se ven todos juntos, clickeo los botones para que se arregle
    $('#liDia_'+data[0].id_menu+'_'+fechas[fechas.length-1].getDate()).click();
    $('#liDia_'+data[0].id_menu+'_'+fechas[0].getDate()).click();
}

function platoPorDia(data)
{
    var fec = getDate(data.fecha)
    var dia = dias[fec.getDay()];
    var numDia = fec.getDate();
    var datosId = data.id_catering+'_'+data.id_menu+'_'+data.id_plato;
    var datosOnClick = data.id_catering+', '+data.id_menu+', '+data.id_plato;

    html = '';
    html += '<div class="col-md-3">';
        html += '<div class="plato">';
            html += '<h3 id="nombrePlato_'+datosId+'">'+data.plato+'</h3>';
            html += '<p class="descripcion-plato">'+data.txt_desc+'</p>';
            html += '<ul>';
                html += '<li>Kcal: '+data.calorias+'</li>';
                html += '<hr>';
                html += '<li class="plato-price">';
                    html += '<div class="incrementer-plato">';
                        html += '<button class="restar-plato" onclick="restarPlato('+datosOnClick+')"><i class="icon-minus"></i></button>';
                        html += '<input type="text" name="" id="cantidad_'+datosId+'" value="1">';
                        html += '<button class="sumar-plato" onclick="sumarPlato('+datosOnClick+')"><i class="icon-plus"></i></button>    ';
                    html += '</div>';
                    html += '<span class="pull-right" id="precio_'+datosId+'">'+data.precio+'</span>';
                html += '</li>';
            html += '</ul>';
            html += '<a class="add-plato" href="javascript:void(0)" onclick="addCarritoPlato('+datosOnClick+', \''+dia+'\', '+numDia+')">Agregar plato</a>';
        html += '</div>';
    html += '</div>';
    return html;
}

(function(){
  if (typeof Object.defineProperty === 'function'){
    try{Object.defineProperty(Array.prototype,'sortBy',{value:sb}); }catch(e){}
  }
  if (!Array.prototype.sortBy) Array.prototype.sortBy = sb;

  function sb(f){
    for (var i=this.length;i;){
      var o = this[--i];
      this[i] = [].concat(f.call(o,o,i),o);
    }
    this.sort(function(a,b){
      for (var i=0,len=a.length;i<len;++i){
        if (a[i]!=b[i]) return a[i]<b[i]?-1:1;
      }
      return 0;
    });
    for (var i=this.length;i;){
      this[--i]=this[i][this[i].length-1];
    }
    return this;
  }
})();

function sumarPlato(id_catering, id_menu, id_plato)
{
    var total = parseInt( $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val() ) + 1;
    $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val(total);
}

function restarPlato(id_catering, id_menu, id_plato)
{
    var total = parseInt( $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val() ) - 1;
    if(total > 0)
        $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val(total);
}

function addCarritoPlato(id_catering, id_menu, id_plato, dia, numDia)
{
    var cantidad = parseInt( $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val() );
    var precio = parseInt( $('#precio_'+id_catering+'_'+id_menu+'_'+id_plato).text() );

    if ($('.pedido').hasClass('hidden')) {
        $('.pedido').removeClass('hidden').slideDown()
    };

    
    $('#sinPedido').hide();

    $('#alertAddPlato').fadeIn(function(){
        setTimeout(function(){ 
            $('#alertAddPlato').fadeOut();
        }, 3000);
    });

    if ($(window).width() <= 770) { 
        $('.show-pedido-row').fadeIn();
    }

    addUlCarrito(id_catering, id_menu, id_plato, dia, numDia);

    var valorCompra = parseInt( $('#totalCarrito').text() );
    var valorAgregado = cantidad * precio;
    $('#totalCarrito').text(valorCompra + valorAgregado);
}

function addUlCarrito(id_catering, id_menu, id_plato, dia, numDia)
{
    var cantidad = parseInt( $('#cantidad_'+id_catering+'_'+id_menu+'_'+id_plato).val() );
    var precio = parseInt( $('#precio_'+id_catering+'_'+id_menu+'_'+id_plato).text() );
    var nombrePlato = $('#nombrePlato_'+id_catering+'_'+id_menu+'_'+id_plato).text();

    var myElem = document.getElementById('carritoDia_'+dia+'_'+numDia+'_'+id_plato);
    if (myElem === null){
        myUl  = '<ul class="timeline-checkout" id="carritoDia_'+dia+'_'+numDia+'_'+id_plato+'">';
        myUl += '    <li class="day-plato" id="carrito_orden_'+id_catering+'_'+id_menu+'_'+id_plato+'">';
        myUl += '        <h4>'+dia+' '+numDia+'</h4>';
        myUl += '    </li>';
        myUl += '</ul>';
        $('#miCarrito').append(myUl);
    }
    platoAdded  = '<div class="plato-checkout">';
    platoAdded += ' <span>'+nombrePlato+' x '+cantidad+'</span>'  ;
    platoAdded += ' <span class="plato-price-checkout pull-right">'+precio+'</span>';
    platoAdded += ' <a href="javascript:void(0)" onclick="eliminarDelCarrito(this, '+precio * cantidad+', '+id_catering+', '+id_menu+', '+id_plato+')" class="delete-item pull-right"><i class="icon-cross"></i></a>';
    platoAdded += '</div>';
    $('#carrito_orden_'+id_catering+'_'+id_menu+'_'+id_plato).append(platoAdded);
    miPedido.push({id_catering:id_catering, id_menu:id_menu, id_plato:id_plato, cantidad:cantidad});
}

function eliminarDelCarrito(item, monto, id_catering, id_menu, id_plato)
{
    $(item).parent().fadeOut(250, function(){
        $(item).remove();
    })
    var valorCompra = parseInt( $('#totalCarrito').text() );
    $('#totalCarrito').text(valorCompra - monto )

    for(var i = 0; i < miPedido.length; i++) {
        if(miPedido[i].id_catering == id_catering && miPedido[i].id_menu == id_menu && miPedido[i].id_plato == id_plato) {
            miPedido.splice(i, 1);
            break;
        }
    }
}

function realizarPedido()
{
    var pedidosMinimos = parseInt($('#pedidosMinimos').text());
    var cantPlatos = 0;

    for (var i = 0; i < miPedido.length; i++) {
        cantPlatos += parseInt(miPedido[i].cantidad);
    }

    if(cantPlatos < pedidosMinimos){
        alert('No llega al cupo mínimo de pedidos');
        return false;
    }
    var comentario = $('#carritoComentario').val();

    if(comentario.length == 0) comentario = "null";

    var precio = parseInt($('#totalCarrito').text());
    $.ajax({
        type: 'POST',
        url: 'direccionadorAjax.php',
        datatype: 'html',
        data: {
             action: 'pedidos'
            ,call  : 'insPedido'
            ,pedido : JSON.stringify(miPedido)
            ,comentario : comentario
            ,precio : precio
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
            alert(miData.mensaje);
            location.reload();
        }
    });
}