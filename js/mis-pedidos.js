jQuery(document).ready(function($){
	$('.open-plato-details').each(function() {
        var menuDetails = $(this).prev('.timeline-pedidos-hidden');
        var btnText = $(this).children('.change-text');
        var angleDown = $(this).children('.icon-chevron-down');

        $(this).click(function(e) {
            menuDetails.slideToggle();
            
            btnText.text(function(i, text){
                return text === "Ver detalles del pedido" ? "Ocultar detalles" : "Ver detalles del pedido";
            });

            $('html, body').animate({
                scrollTop: menuDetails.offset().top - 160
            }, 1000);

            angleDown.toggleClass('icon-chevron-down icon-chevron-up');
        });
    });

});