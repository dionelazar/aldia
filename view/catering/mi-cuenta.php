<?php 
  $titulo = 'Aldia | Mi cuenta';
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <div class="panel panel-transparent">
                      <!-- Nav tabs -->
                      <div class="nav-tabs-header nav-tabs-linetriangle">
                        <ul class="nav nav-tabs nav-tabs-linetriangle">
                          <li class="active">
                            <a data-toggle="tab" href="#fade1"><span>Hello World</span></a>
                          </li>
                          <li>
                            <a data-toggle="tab" href="#fade2"><span>Hello Two</span></a>
                          </li>
                          <li>
                            <a data-toggle="tab" href="#fade3"><span>Hello Three</span></a>
                          </li>
                        </ul>
                      </div>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane fade in active" id="fade1">
                          <div class="row column-seperation">
                            <div class="col-md-6">
                              <h3>
                                                <span class="semi-bold">Sometimes</span> Small things in life means the most
                                            </h3>
                            </div>
                            <div class="col-md-6">
                              <h3 class="semi-bold">great tabs</h3>
                              <p>Native boostrap tabs customized to Pages look and feel, simply changing class name you can change color as well as its animations</p>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="fade2">
                          <div class="row">
                            <div class="col-md-12">
                              <h3>“ Nothing is
                                                <span class="semi-bold">impossible</span>, the word itself says 'I'm
                                                <span class="semi-bold">possible</span>'! ”</h3>
                              <p>A style represents visual customizations on top of a layout. By editing a style, you can use Squarespace's visual interface to customize your...</p>
                              <br>
                              <p class="pull-right">
                                <button type="button" class="btn btn-default btn-cons">White</button>
                                <button type="button" class="btn btn-success btn-cons">Success</button>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="fade3">
                          <div class="row">
                            <div class="col-md-12">
                              <h3>Follow us &amp; get updated!</h3>
                              <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite celebrities, and breaking news.</p>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <!-- Este .breadcrumb debe estar aunque este vacio para conservar el margen que hay entre .page-content-wrapper y .header -->
            </ul> <!-- finaliza .breadcrumb -->

            <div class="row">
              
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title"><i class="icon-user"></i> Información de contacto</div>
                  </div>  

                  <div class="panel-body">
                    <form id="form-personal" role="form" autocomplete="off">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Nombre de contacto</label>
                            <input type="text" class="form-control" value="Isabel Cortez">
                          </div> 
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>E-mail</label>
                            <input type="email" class="form-control" value="isabel.ita@yimeil.com">
                          </div> 
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                      <a id="infoContact" href="#0" class="btn btn-primary">Cambiar información</a>
                    </form>
                  </div>    
                </div> 
              </div>

              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title"><i class="icon-lock-open"></i> Cambiar contraseña</div>
                  </div>  

                  <div class="panel-body">
                    <form id="form-personal" role="form" autocomplete="off">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Nueva contraseña</label>
                            <input type="password" class="form-control">
                          </div> 
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                      <a id="newPass" href="#0" class="btn btn-primary">Cambiar contraseña</a>
                    </form>
                  </div>    
                </div>   
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title"><i class="icon-blog"></i> Información de facturación</div>
                  </div>  

                  <div class="panel-body">
                    <form id="form-personal" role="form" autocomplete="off">

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Razón social</label>
                            <input type="text" class="form-control">
                          </div> 
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Condición frente al IVA</label>
                            <select id="condicionIva" class="full-width" data-placeholder="Busca un barrio" data-init-plugin="select2">
                                <option value="Responsable Inscripto">Responsable Inscripto</option>
                                <option value="Monotributista">Monotributista</option>
                                <option value="Exento">Exento</option>
                                <option value="No Responsable">No Responsable</option>
                                <option value="Consumidor final">Consumidor final</option>
                            </select>
                          </div> 
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>CUIT <small>(Soló numeros)</small></label>
                            <input type="number" class="form-control">
                          </div> 
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default">
                            <label>Domicilio Fiscal</label>
                            <input type="text" class="form-control">
                          </div> 
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                      <a id="infoFacturacion" href="#0" class="btn btn-primary">Cambiar información</a>
                    </form>
                  </div>    
                </div>   
              </div>
            </div>
            
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/mi-cuenta.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
  </body>
</html>