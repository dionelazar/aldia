<?php 
    $title = 'Aldia | Configuración';
    require_once '../header.cliente.php';
?>
    <section class="aldia-section">
        <div class="container">
            <div class="row">
                <?php if ($user->tipo_usuario == 4):
                    require __DIR__.'/form-empresa.php';
                endif ?>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-body">
                            <h4 class="m-t-0 m-b-30">Información de contacto</h4>

                            <form action="">
                                <div class="form-group">
                                    <label for="">Nombre</label>
                                    <input type="text" class="form-control" id="contactoNombre">
                                </div>

                                <div class="form-group">
                                    <label for="">Apellido</label>
                                    <input type="text" class="form-control" id="contactoApellido">
                                </div>

                                <div class="form-group">
                                    <label for="">E-mail</label>
                                    <input type="email" class="form-control" id="contactoEmail">
                                </div>

                                <div class="form-group">
                                    <label for="">Teléfono</label>
                                    <input type="number" class="form-control" id="contactoTelefono">
                                </div>
                                <input type="button" class="btn btn-default" onclick="updDatosUsuario()" value="Cambiar">
                            </form>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <form>
                                <h4 class="m-t-0 m-b-30">Dirección de recepción de pedidos</h4>

                                <div class="form-group">
                                    <label for="">Calle</label>
                                    <input type="text" class="form-control" id="calle" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Entre calles</label>
                                    <input type="text" class="form-control" id="entre_calles" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Altura</label>
                                    <input type="number" class="form-control" id="altura" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Piso</label>
                                    <input type="number" class="form-control" id="piso" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Oficina / Departamento</label>
                                    <input type="text" class="form-control" id="oficina" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Código postal</label>
                                    <input type="number" class="form-control" id="cod_postal" value="">
                                </div>

                                <div class="form-group">
                                    <label for="">Provincia</label>
                                    <select name="provincias" id="provincias" class="form-control"></select>
                                </div>

                                <div class="form-group">
                                    <label for="">Municipio</label>
                                    <select name="municipios" id="municipios" class="form-control"></select>
                                </div>
                                
                                <input type="button" class="btn btn-default" onclick="insDireccion()" value="Cambiar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <a href="index"><img class="center-block" src="img/logo-white.svg" alt="Aldia"></a>
                    </div>

                    <!-- <ul class="social-contact">
                        <li>
                            <img src="img/facebook.svg" alt="facebook">
                        </li>
                        <li>
                            <img src="img/twitter.svg" alt="twitter">
                        </li>
                    </ul> -->

                    <ul class="footer-links">
                        <li>
                            <a href="mispedidos">Mis pedidos</a>
                        </li><!-- 
                        <li>
                            <a href="#0">Invitados</a>
                        </li> -->
                        <li>
                            <a href="configuracion">Configuración</a>
                        </li>
                        <li>
                            <a href="#0">Ayuda</a>
                        </li>
                        <li>
                            <a href="#0">Términos y condiciones</a>
                        </li>
                    </ul>
                </div>    
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/configuracion.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/configuracion.js"></script>
</body>
</html>
