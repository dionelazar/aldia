<?php 
/**
* 
*/
require_once 'Controller.php';
require_once __DIR__.'/../class.menues.php';

class MenuController extends Controllers
{
	
	public function getMenu()
	{
		$id_menu = $_GET['id_menu'];
		$id_catering = $_GET['id_catering'];
		
		$menu = new Menues();
		$menu = Menues::getMenu($id_menu, $id_catering);
		return $menu;
	}
}
?>