<?php 
/**
* 
*/
require_once 'Controller.php';
require_once __DIR__.'/../class.helpers.php';
require_once __DIR__.'/../class.catering.php';
require_once __DIR__.'/../class.menues.php';
require_once __DIR__.'/../class.platos.php';

class ClienteController extends Controllers
{
	function getDatosUser()
	{
		$datos = Cookies::getDatosUser();
		return $datos;
	}

	function getDatosCatering()
	{
		$catering = new Catering();
		$datosUser = Cookies::getDatosUser();
		$miCatering = $catering->getDatosCatering($datosUser->id_catering);
		
		return $miCatering;
	}

	function getPlatos()
	{
		$platos = new Platos();
		return $platos->getAllPlatosActivos();
	}

	function getMenues()
	{
		$menues = new Menues();
		return $menues->getMenuesVigentes2();
	}
} 
?>