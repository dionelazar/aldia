<?php
  $nombreEmpresa = 'Nombre';
  $titulo = 'Aldia | '.$nombreEmpresa;
  require_once '../header.catering.php';
?>

      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">

            <!-- Comienza .breadcrumb -->
            <ul class="breadcrumb">
              <li>
                <a href="misclientes">clientes</a>
              </li>
              <li><a href="#0" class="active">Nombre de la empresa</a>
              </li>
            </ul> <!-- finaliza .breadcrumb -->

            <!-- Comienza panel de clientes -->
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row company-header">
                  <div class="col-md-3">
                      <div class="img-wrapper">
                        <img class="img-responsive" src="http://placehold.it/350x350" alt="Nombre de catering">
                      </div>  
                  </div>
                  
                  <div class="col-md-9">
                    <h3 class="company-name font-montserrat">Nombre de la empresa</h3>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <h4 class="data-company-heading font-montserrat">Datos de la empresa</h4>
                  </div>
                  <div class="col-md-6">
                    <h4 class="data-company-heading font-montserrat">Dirección de recepción de pedidos</h4>
                  </div>
                </div>
                <hr class="data-company-divider">
                <div class="row">
                  <div class="col-md-6 data-company-container">
                    <p><strong>Razón social:</strong> razonsocial</p>
                    <p><strong>CUIT:</strong> 00000000000</p>
                    <p><strong>Domicilio fiscal:</strong> feik estrit 538</p>
                    <p><strong>Condiciones frente al IVA:</strong> Responsable Inscripto</p>
                  </div>

                  <div class="col-md-6 data-company-container">
                    <p><strong>Calle:</strong> feik estrit 420</p>
                    <p><strong>Entre calles:</strong> malibu estrit y rumi estrit</p>
                    <p><strong>Altura:</strong> 400</p>
                    <p><strong>Piso:</strong> 1</p>
                    <p><strong>Oficina:</strong> 2</p>
                    <p><strong>Cod. postal:</strong> 4200</p>
                    <p><strong>Ciudad:</strong> CABA</p>
                    <p><strong>Barrio:</strong> Montserrat</p>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <h4 class="data-company-heading font-montserrat">Datos de contacto</h4>
                  </div>
                </div>
                <hr class="data-company-divider">
                <div class="row">
                  <div class="col-md-6">
                    <p><strong>Nombre de contacto:</strong> Ricardo</p>
                    <p><strong>Apellido:</strong> Iorio</p>
                    <p><strong>E-mail:</strong> hola@empresa.com</p>
                    <p><strong>Teléfono:</strong> 1569592800</p>
                  </div>
                </div>
              </div>
            </div> <!-- Termina panel de clientes -->
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->
    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>

    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/empresa-perfil.js" type="text/javascript"></script>

    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>
  </body>
</html>