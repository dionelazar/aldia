<?php 
/**
* 
*/
require_once 'class.encrypt.php';
require_once 'class.cookies.php';
require_once 'class.error.php';

class Helpers
{
	function insertarError($errorConNumero, $queryEjecutada, $params)
	{
		if(is_array($params))
	    	$paramsString = implode(',', $params);
	    else
	    	$paramsString = $params;
	    
	    Errores::insErrorDeQuery($errorConNumero, $queryEjecutada, $paramsString);
	}

	public function jsonEncodeError($cod_mensaje, $mensaje)
	{
		$data = new Helpers();
		$data->cod_mensaje = $cod_mensaje;
		$data->mensaje     = $mensaje;
		echo json_encode($data);
	}

	public function returnError($cod_mensaje, $mensaje)
	{
		$data = new Helpers();
		$data->cod_mensaje = $cod_mensaje;
		$data->mensaje     = $mensaje;
		return $data;
	}

	public function getDia($fecha)
	{

	}

	public function getNumDia($fecha)
	{
		
	}
}
?>