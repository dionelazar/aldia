<?php 
  $titulo = 'Aldia | Reportes';
  require_once '../header.catering.php';
?>

      
      <!-- Comienza PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">

        <!-- Comienza PAGE CONTENT -->
        <div class="content">
          <div class="nav-tabs-header nav-tabs-default">
            <ul class="nav nav-tabs nav-tabs-default">
              <li class="active">
                <a data-toggle="tab" href="#menu"><span>Menú</span></a>
              </li>
              <li >
                <a data-toggle="tab" href="#pedidos"><span>Pedidos</span></a>
              </li>
              <li>
                <a data-toggle="tab" href="#platos"><span>Platos</span></a>
              </li>
              <li>
                <a data-toggle="tab" href="#clientes"><span>Clientes</span></a>
              </li>
            </ul>
          </div>
          <!-- Comienza CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="menu">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="panel">
                          <div class="panel-body">
                            <div class="menu-report-container">
                              <div class="icon-container pull-left">
                                <img src="img/menu-icon-green.svg" alt="">
                              </div>
                              <div class="data-container pull-left">
                                <p class="title-report"><span class="bold">20</span> Menús vigentes</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="panel">
                          <div class="panel-body">
                            <div class="menu-report-container">
                              <div class="icon-container pull-left">
                                <img src="img/menu-icon-red.svg" alt="">
                              </div>
                              <div class="data-container pull-left">
                                <p class="title-report"><span class="bold">5</span> Menús finalizados</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="panel">
                          <div class="panel-body">
                            <div class="menu-report-container">
                              <div class="icon-container pull-left">
                                <img src="img/menu-icon-grey.svg" alt="">
                              </div>
                              <div class="data-container pull-left">
                                <p class="title-report"><span class="bold">25</span> Menús publicados</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="pedidos">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="panel">
                          <div class="panel-heading">
                            <div class="panel-title"><i class="icon-pin"></i> Pedidos</div>
                          </div>

                          <div class="panel-body">
                            <div class="row">
                              <div class="col-md-12">
                                <ul class="nav nav-pills pull-right xs-pull-reset">
                                  <li class="active"><a data-toggle="tab" href="#diarios">Diarios</a></li>
                                  <li><a data-toggle="tab" href="#semanales">Semanales</a></li>
                                  <li><a data-toggle="tab" href="#mensuales">Mensuales</a></li>
                                </ul>    
                              </div>
                            </div>      
                            
                            <div class="tab-content m-t-20">
                              <div id="diarios" class="tab-pane fade in active">
                                <canvas id="pedidosDiarios"></canvas>
                              </div>

                              <div id="semanales" class="tab-pane fade">
                                <canvas id="pedidosSemanales"></canvas>
                              </div>

                              <div id="mensuales" class="tab-pane fade">
                                <canvas id="pedidosMensuales"></canvas>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="panel">
                          <div class="panel-body">

                            <form class="buscador-panel" id="buscarPedido" action="">
                              <input type="text" id="search-pedido" class="form-control m-t-0 m-b-0" placeholder="Buscar pedido">  
                              <button><i class="icon" data-icon="&#xe041;"></i></button>
                            </form> 


                            <table class="table table-hover" id="tablePedidos">
                              <thead>
                                <tr>
                                  <th>Menú</th>
                                  <th class="text-center">Pedidos <i class="icon-information" data-toggle="tooltip" data-original-title="Cantidad de pedidos recibidos"></i></th>
                                  <th class="text-right">Monto</th>
                                </tr>
                              </thead>

                              <tbody>
                                <tr>
                                  <td data-label="Menú" class="v-align-middle semi-bold">
                                    <p>Milanesa con ensalada mixta</p>
                                  </td>
                                  <td data-label="Pedidos" class="v-align-middle text-center">
                                    <p>85</p>
                                  </td>
                                  <td data-label="Monto" class="v-align-middle text-right">
                                    <p class="text-complete">$7225</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td data-label="Menú"class="v-align-middle semi-bold">
                                    <p>Milanesa con ensalada mixta</p>
                                  </td>
                                  <td data-label="Pedidos" class="v-align-middle text-center">
                                    <p>85</p>
                                  </td>
                                  <td data-label="Monto" class="v-align-middle text-right">
                                    <p class="text-complete">$7225</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td data-label="Menú"class="v-align-middle semi-bold">
                                    <p>Milanesa con ensalada mixta</p>
                                  </td>
                                  <td data-label="Pedidos" class="v-align-middle text-center">
                                    <p>85</p>
                                  </td>
                                  <td data-label="Monto" class="v-align-middle text-right">
                                    <p class="text-complete">$7225</p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>   
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="platos">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel">
                          <div class="panel-heading">
                            <div class="panel-title"><i class="icon-cutlery"></i> Ranking de platos</div>
                          </div>
                          <div class="panel-body">
                            <canvas style="height:500px!important;" id="rankingPlatos"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="clientes">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel">
                          <div class="panel-heading">
                            <div class="panel-title"><i class="icon-user-group"></i> Pedidos por Clientes</div>
                          </div>
                          <div class="panel-body">
                            <canvas style="height:500px!important;" id="pedidosClientes"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div> <!-- Termina CONTAINER FLUID -->
        </div> <!-- Termina PAGE CONTENT -->
      </div> <!-- Termina PAGE CONTENT WRAPPER -->
    </div> <!-- Termina PAGE CONTAINER -->

    
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    
    <script src="pages/js/pages.min.js"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <script src="assets/js/chart.min.js" type="text/javascript"></script>
    <script src="assets/js/reportes.js" type="text/javascript"></script>
    <script src="scripts/header.catering.js" type="text/javascript"></script>


  </body>
</html>